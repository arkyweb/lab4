<?php

define('THEME_PATH', base_path() . drupal_get_path('theme', 'arky8'));

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Template\Attribute;
use Drupal\file\Plugin\Core\Entity\FileInterface;
use Drupal\Component\Utility\Html;
use \Drupal\block_content\BlockContentInterface;
use \Drupal\taxonomy\Entity\Term;


function arky8_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  // Block suggestions for custom block bundles.
  if (isset($variables['elements']['content']['#block_content']) && $variables['elements']['content']['#block_content'] instanceof BlockContentInterface) {
    array_splice($suggestions, 1, 0, 'block__bundle__' . $variables['elements']['content']['#block_content']->bundle());
  }
}

function arky8_preprocess_page(&$variables) {

  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render
    // elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }

  $variables['base_path'] = base_path();
  $variables['logo'] = THEME_PATH . '/logo.png';

  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $variables['node'] = $node; 
  }

  $variables['first_attributes'] = array(
    'id' => array ('first'),
    'class' => array ('first')
  );
  $variables['second_attributes'] = array(
    'id' => array ('second'),
    'class' => array ('second')
  );

  //declarar atributos
  $variables['first_attributes'] = new Attribute($variables['first_attributes']);
  $variables['second_attributes'] = new Attribute($variables['second_attributes']);

}

function arky8_preprocess_html(&$variables) {
  // Get page node.
  $variables['path_info']['args'] = FALSE;
  $path = \Drupal::request()->getPathInfo();
  $path_args = explode('/', $path);
  if (count($path_args) >= 3) {
    $variables['path_info']['args'] = Html::cleanCssIdentifier(ltrim($path, '/'));
  }
  // Get current path.
  $current_path = \Drupal::service('path.current')->getPath();
  $variables['current_path'] = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);
  // Get currently active user and his roles.
  $account = \Drupal::currentUser();
  $roles = $account->getRoles();
  /* The getRoles() method will return us the machine names, so there is no need to process roles names additionally. However, I suggest prefixing the names with "role-", so it's more obvious. */
  foreach ($roles as $role) {
    $variables['attributes']['class'][] = 'role-' . $role;
  }
  // Get language
  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $variables['language'] = $language;


  $path = \Drupal::service('path.current')->getPath();
  $path_args = explode('/', $path);
  if (isset($path_args[1]) && isset($path_args[2]) && ($path_args[1] == 'node') && (is_numeric($path_args[2]))) {
    $variables['attributes']['class'][] = 'node-' . $path_args[2];
  }
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $variables['node'] = $node;
  }

  if (\Drupal::routeMatch()->getRouteName() == 'entity.taxonomy_term.canonical') {
    $variables['term_id'] = \Drupal::routeMatch()->getRawParameter('taxonomy_term');
  } 
}

function arky8_preprocess(&$variables, $hook) {
  $variables['#cache']['contexts'][] = 'url';
  try {
    $variables['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
  }
  catch (Exception $e) {
    $variables['is_front'] = FALSE;
  }

}

function arky8_preprocess_page_title(&$variables) { 
  $variables['url'] = $_SERVER['REQUEST_URI'];
  $variables['#cache']['contexts'][] = 'url.path';
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $variables['node'] = $node;
  }
  
}

function arky8_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  // Add content type suggestions.
  if ($node = \Drupal::request()->attributes->get('node')) {
    array_splice($suggestions, 1, 0, 'page__node__' . $node->getType());
  } 

  //401, 403, 404 solution
  /*
  $http_error_suggestions = [
     'system.401' => 'page__401',
     'system.403' => 'page__403',
     'system.404' => 'page__404',
  ];
  $node = \Drupal::request()->attributes->get('node');
  if ($node) {
     if ($node->id() == 7 ) {  // this is the node id linked to the 404 page
        $suggestions[] = $http_error_suggestions['system.403'];
     }
     if ($node->id() == 6 ) {  // this is the node id linked to the 404 page
        $suggestions[] = $http_error_suggestions['system.404'];
     }
  }
*/
}
function arky8_theme_suggestions_form_alter(array &$suggestions, array $variables) {
    $suggestions[] = 'form__' . $variables['element']['#form_id'];

    $form_id = $variables['element']['#form_id'];
    if (in_array($form_id, ['user_login_form','user_register_form','user_pass']))  {
      $suggestions[] = 'form__' . $form_id;
    }
}