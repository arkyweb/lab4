var swiper = new Swiper('.swipper-container-pagination', { 
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
          },
        loop: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
});
var swiper = new Swiper('.swipper-container-navigation', {
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
          },
        loop: true,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        }, 
});