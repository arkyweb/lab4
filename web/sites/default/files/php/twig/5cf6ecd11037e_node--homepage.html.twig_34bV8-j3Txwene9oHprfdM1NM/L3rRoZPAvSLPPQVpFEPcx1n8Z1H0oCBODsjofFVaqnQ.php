<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node--homepage.html.twig */
class __TwigTemplate_98e168edeb7c6b702c2c37cee9a6888d96a73851314c42b7ac10af43babcc322 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 5, "for" => 66];
        $filters = ["escape" => 7, "length" => 25, "image_style" => 28, "first" => 66, "t" => 88];
        $functions = ["file_url" => 28];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape', 'length', 'image_style', 'first', 't'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
";
        // line 5
        if ((($context["view_mode"] ?? null) == "teaser")) {
            echo " ";
            // line 6
            echo "
<article";
            // line 7
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-teaser"], "method")), "html", null, true);
            echo "> 

";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 10
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

    <h3>";
            // line 12
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TITULO"] ?? null)), "html", null, true);
            echo "</h3>

</article>

";
        } elseif ((        // line 16
($context["view_mode"] ?? null) == "about")) {
            echo " ";
            // line 17
            echo "
<div";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-about"], "method")), "html", null, true);
            echo "> 

";
            // line 20
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 21
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

";
            // line 23
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_hp_about_txt", []), "value", [])) {
                // line 24
                echo "
";
                // line 25
                if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_hp_about_bg", []), 0, [], "array"), "value", [])) > 0)) {
                    echo " ";
                    echo " 
<style>
    .about {
        background-image: url('";
                    // line 28
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_hp_about_bg", []), "entity", []), "fileuri", [])), "thumbnail")]), "html", null, true);
                    echo "');
    }
    .about.visible {
        background-image: url('";
                    // line 31
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_hp_about_bg", []), "entity", []), "fileuri", []))]), "html", null, true);
                    echo "');
    }
</style>
<section class=\"about bglazy\"> 
";
                } else {
                    // line 36
                    echo "<section class=\"about\">
";
                }
                // line 38
                echo "    <div class=\"contenedor about--contenedor\">
        <article class=\"about--block\"> 
                <div class=\"about--text\">
                    ";
                // line 41
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_hp_about_txt", [])), "html", null, true);
                echo " ";
                // line 42
                echo "                </div>
                <div class=\"about--link\">
                    ";
                // line 44
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_hp_about_link", [])), "html", null, true);
                echo "
                </div>
        </article>
    </div> 
</section>

";
            }
            // line 50
            echo "  

</div> 

";
        } elseif ((        // line 54
($context["view_mode"] ?? null) == "carousel")) {
            echo " ";
            // line 55
            echo "
<div";
            // line 56
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-carousel"], "method")), "html", null, true);
            echo "> 

";
            // line 58
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 59
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

";
            // line 61
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_carousel", []), "value", [])) > 0)) {
                echo " 

        <div class=\"swiper-container swipper-container-pagination carousel-container\"> 
            ";
                // line 64
                if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_carousel", []), 0, [], "array")) {
                    // line 65
                    echo "                <div class=\"swiper-wrapper\">
                    ";
                    // line 66
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_carousel", []));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                        if ((twig_first($this->env, $context["key"]) != "#")) {
                            // line 67
                            echo "                        <div class=\"swiper-slide slide-0";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                            echo "\">";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                            echo "</div>
                    ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 69
                    echo "                </div>
                <div class=\"swiper-pagination\"></div>
            ";
                }
                // line 71
                echo " 
        </div>

";
            }
            // line 74
            echo "  

</div> 

";
        } elseif ((        // line 78
($context["view_mode"] ?? null) == "experiences")) {
            echo " ";
            // line 79
            echo "
<div";
            // line 80
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-experiences"], "method"), "setAttribute", [0 => "id", 1 => "schedule"], "method")), "html", null, true);
            echo "> 

";
            // line 82
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 83
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

    <div class=\"experiences--cover\">
        <div class=\"contenedor\">
            <div class=\"experiences--cover-title\">
                <h5 class=\"block--subtitle\">";
            // line 88
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Schedule"));
            echo "</h5>
                <h4 class=\"block--title\">";
            // line 89
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Experiences"));
            echo "</h4>
            </div>
            <div class=\"experiences--cover-text\">
                ";
            // line 92
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_hp_experiences_intro", [])), "html", null, true);
            echo " ";
            // line 93
            echo "            </div>
        </div>
    </div>

    ";
            // line 97
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_hp_experiences", []), 0, [], "array")) {
                // line 98
                echo "        <div class=\"experiences-container\">
            ";
                // line 99
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_hp_experiences", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 100
                        echo "            <div class=\"experiences-item experiences-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
            ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 102
                echo "        </div>
    ";
            }
            // line 103
            echo " 

    <div class=\"experiences--link\">
        ";
            // line 106
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_hp_experiences_link", [])), "html", null, true);
            echo "
    </div>

    <div class=\"cta cta--ambar\">
            <div class=\"contenedor\">
                <h4 class=\"block--subtitle\">";
            // line 111
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_hp_experiences_footer", []), "value", [])), "html", null, true);
            echo "</h4>
                <p><a class=\"btn btn--red\" href=\"#tickets\">";
            // line 112
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Buy your tickets here"));
            echo "</a></p>
            </div>
    </div>

</div>

";
        } elseif ((        // line 118
($context["view_mode"] ?? null) == "new_tracks")) {
            echo " ";
            // line 119
            echo "
<div";
            // line 120
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-new-tracks"], "method"), "setAttribute", [0 => "id", 1 => "tracks"], "method")), "html", null, true);
            echo "> 

";
            // line 122
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 123
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

    <div class=\"experiences--cover\">
        <div class=\"contenedor\">
            <div class=\"experiences--cover-title\"> 
                <h4 class=\"block--title\">";
            // line 128
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("A track for every one"));
            echo "</h4>
            </div>
            <div class=\"experiences--cover-text\">
                ";
            // line 131
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_hp_tracks_intro", [])), "html", null, true);
            echo " ";
            // line 132
            echo "            </div>
        </div>
    </div>

    ";
            // line 136
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_hp_tracks", []), 0, [], "array")) {
                // line 137
                echo "        <div class=\"experiences-container new-tracks-fix\">
            ";
                // line 138
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_hp_tracks", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 139
                        echo "            <div class=\"experiences-item experiences-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
            ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 141
                echo "        </div>

        <div class=\"cta cta--ambar\">
            <div class=\"contenedor\">
                <h4 class=\"block--subtitle\">";
                // line 145
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_hp_track_footer", []), "value", [])), "html", null, true);
                echo "</h4>
                <p><a class=\"btn btn--red\" href=\"#tickets\">";
                // line 146
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Buy your tickets here"));
                echo "</a></p>
            </div>
        </div>
    ";
            }
            // line 149
            echo "  

</div>

";
        } elseif ((        // line 153
($context["view_mode"] ?? null) == "why_attend")) {
            echo " ";
            // line 154
            echo "
<div";
            // line 155
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-experiences"], "method"), "setAttribute", [0 => "id", 1 => "attend"], "method")), "html", null, true);
            echo "> 

";
            // line 157
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 158
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

    <div class=\"experiences--cover\">
        <div class=\"contenedor\">
            <div class=\"experiences--cover-title\"> 
                <h4 class=\"block--title\">";
            // line 163
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Why Attend?"));
            echo "</h4>
            </div>
            <div class=\"experiences--cover-text\">
                ";
            // line 166
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_why_attend_intro", [])), "html", null, true);
            echo " ";
            // line 167
            echo "            </div>
        </div>
    </div>

    ";
            // line 171
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_why_attend", []), 0, [], "array")) {
                // line 172
                echo "        <div class=\"experiences-container why-attend-fix\">
            ";
                // line 173
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_why_attend", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 174
                        echo "            <div class=\"experiences-item experiences-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
            ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 176
                echo "        </div>

        <div class=\"cta cta--ambar\">
            <div class=\"contenedor\">
                <h4 class=\"block--subtitle\">";
                // line 180
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_why_attend_footer", []), "value", [])), "html", null, true);
                echo "</h4>
                <p><a class=\"btn btn--red\" href=\"#tickets\">";
                // line 181
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Buy your tickets here"));
                echo "</a></p>
            </div>
        </div>
    ";
            }
            // line 184
            echo "  

</div> 

";
        } elseif ((        // line 188
($context["view_mode"] ?? null) == "modal")) {
            echo " ";
            // line 189
            echo "
<div";
            // line 190
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-modal"], "method")), "html", null, true);
            echo "> 

";
            // line 192
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 193
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

    ";
            // line 195
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_hp_experiences", []), 0, [], "array")) {
                echo " 
        ";
                // line 196
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_hp_experiences", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 197
                        echo "        <div class=\"modal-item modal-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
        ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 198
                echo " 
    ";
            }
            // line 199
            echo " 

    ";
            // line 201
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_why_attend", []), 0, [], "array")) {
                echo " 
        ";
                // line 202
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_why_attend", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 203
                        echo "        <div class=\"modal-item modal-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
        ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 204
                echo " 
    ";
            }
            // line 205
            echo " 

    ";
            // line 207
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_hp_tracks", []), 0, [], "array")) {
                echo " 
        ";
                // line 208
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_hp_tracks", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 209
                        echo "        <div class=\"modal-item modal-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
        ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 210
                echo " 
    ";
            }
            // line 211
            echo " 

</div> 

";
        } elseif ((        // line 215
($context["view_mode"] ?? null) == "tracks")) {
            echo " ";
            // line 216
            echo "
<div";
            // line 217
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-tracks"], "method")), "html", null, true);
            echo "> 

";
            // line 219
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 220
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

    <div class=\"contenedor\"> 

    ";
            // line 224
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_hp_track_bottom", []), "value", [])) > 0)) {
                echo " 

        <div class=\"div-tracks-container\">

            <h4 class=\"block--title\">";
                // line 228
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Our Values"));
                echo "</h4>

            <div class=\"tracks-container\"> 
                ";
                // line 231
                if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_hp_track_bottom", []), 0, [], "array")) {
                    // line 232
                    echo "                    <div class=\"tracks-wrapper\">
                        ";
                    // line 233
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_hp_track_bottom", []));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                        if ((twig_first($this->env, $context["key"]) != "#")) {
                            // line 234
                            echo "                            <div class=\"tracks-item tracks-item-0";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                            echo "\">";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                            echo "</div>
                        ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 236
                    echo "                    </div> 
                ";
                }
                // line 237
                echo " 
            </div>

        </div>

    ";
            }
            // line 242
            echo "  

    </div>

</div> 

";
        } elseif ((        // line 248
($context["view_mode"] ?? null) == "joining")) {
            echo " ";
            // line 249
            echo "
<div";
            // line 250
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-joining"], "method")), "html", null, true);
            echo "> 

";
            // line 252
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 253
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

";
            // line 255
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_joining", []), "value", [])) > 0)) {
                echo " 

        <div class=\"swiper-container swipper-container-navigation joining-container\">
            <h4 class=\"block--title\">";
                // line 258
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Who is Joining?"));
                echo "</h4>
            ";
                // line 259
                if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_joining", []), 0, [], "array")) {
                    // line 260
                    echo "                <div class=\"swiper-wrapper\">
                    ";
                    // line 261
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_joining", []));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                        if ((twig_first($this->env, $context["key"]) != "#")) {
                            // line 262
                            echo "                        <div class=\"swiper-slide slide-0";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                            echo "\">";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                            echo "</div>
                    ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 264
                    echo "                </div>
                <div class=\"swiper-button-next\"></div>
                <div class=\"swiper-button-prev\"></div>
            ";
                }
                // line 267
                echo " 
        </div>

";
            }
            // line 270
            echo "  

</div> 

";
        } else {
            // line 274
            echo " ";
            // line 275
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node--homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  828 => 275,  826 => 274,  819 => 270,  813 => 267,  807 => 264,  792 => 262,  781 => 261,  778 => 260,  776 => 259,  772 => 258,  766 => 255,  761 => 253,  757 => 252,  752 => 250,  749 => 249,  746 => 248,  738 => 242,  730 => 237,  726 => 236,  711 => 234,  700 => 233,  697 => 232,  695 => 231,  689 => 228,  682 => 224,  675 => 220,  671 => 219,  666 => 217,  663 => 216,  660 => 215,  654 => 211,  650 => 210,  635 => 209,  624 => 208,  620 => 207,  616 => 205,  612 => 204,  597 => 203,  586 => 202,  582 => 201,  578 => 199,  574 => 198,  559 => 197,  548 => 196,  544 => 195,  539 => 193,  535 => 192,  530 => 190,  527 => 189,  524 => 188,  518 => 184,  511 => 181,  507 => 180,  501 => 176,  486 => 174,  475 => 173,  472 => 172,  470 => 171,  464 => 167,  461 => 166,  455 => 163,  447 => 158,  443 => 157,  438 => 155,  435 => 154,  432 => 153,  426 => 149,  419 => 146,  415 => 145,  409 => 141,  394 => 139,  383 => 138,  380 => 137,  378 => 136,  372 => 132,  369 => 131,  363 => 128,  355 => 123,  351 => 122,  346 => 120,  343 => 119,  340 => 118,  331 => 112,  327 => 111,  319 => 106,  314 => 103,  310 => 102,  295 => 100,  284 => 99,  281 => 98,  279 => 97,  273 => 93,  270 => 92,  264 => 89,  260 => 88,  252 => 83,  248 => 82,  243 => 80,  240 => 79,  237 => 78,  231 => 74,  225 => 71,  220 => 69,  205 => 67,  194 => 66,  191 => 65,  189 => 64,  183 => 61,  178 => 59,  174 => 58,  169 => 56,  166 => 55,  163 => 54,  157 => 50,  147 => 44,  143 => 42,  140 => 41,  135 => 38,  131 => 36,  123 => 31,  117 => 28,  110 => 25,  107 => 24,  105 => 23,  100 => 21,  96 => 20,  91 => 18,  88 => 17,  85 => 16,  78 => 12,  73 => 10,  69 => 9,  64 => 7,  61 => 6,  58 => 5,  55 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# EMPIEZA EL NODO DE PAGINA                #}
{# ---------------------------------------- #}

{% if view_mode == 'teaser' %} {# Product full page (teaser). #}

<article{{ attributes.addClass('div-teaser') }}> 

{{ title_prefix }} 
{{ title_suffix }}

    <h3>{{ TITULO }}</h3>

</article>

{% elseif view_mode == 'about' %} {# Product full page (about). #}

<div{{ attributes.addClass('div-about') }}> 

{{ title_prefix }} 
{{ title_suffix }}

{% if node.field_hp_about_txt.value %}

{% if node.field_hp_about_bg[0].value|length > 0 %} {# Campo About Bg #} 
<style>
    .about {
        background-image: url('{{ file_url(node.field_hp_about_bg.entity.fileuri | image_style('thumbnail') ) }}');
    }
    .about.visible {
        background-image: url('{{ file_url(node.field_hp_about_bg.entity.fileuri) }}');
    }
</style>
<section class=\"about bglazy\"> 
{% else %}
<section class=\"about\">
{% endif %}
    <div class=\"contenedor about--contenedor\">
        <article class=\"about--block\"> 
                <div class=\"about--text\">
                    {{ content.field_hp_about_txt }} {# Campo About Text #}
                </div>
                <div class=\"about--link\">
                    {{ content.field_hp_about_link }}
                </div>
        </article>
    </div> 
</section>

{% endif %}  

</div> 

{% elseif view_mode == 'carousel' %} {# Product full page (joining). #}

<div{{ attributes.addClass('div-carousel') }}> 

{{ title_prefix }} 
{{ title_suffix }}

{% if node.field_carousel.value|length > 0 %} 

        <div class=\"swiper-container swipper-container-pagination carousel-container\"> 
            {% if content.field_carousel[0] %}
                <div class=\"swiper-wrapper\">
                    {% for key, item in content.field_carousel if key|first != '#' %}
                        <div class=\"swiper-slide slide-0{{ loop.index }}\">{{ item }}</div>
                    {% endfor %}
                </div>
                <div class=\"swiper-pagination\"></div>
            {% endif %} 
        </div>

{% endif %}  

</div> 

{% elseif view_mode == 'experiences' %} {# Product full page (experiences). #}

<div{{ attributes.addClass('div-experiences').setAttribute('id', 'schedule') }}> 

{{ title_prefix }} 
{{ title_suffix }}

    <div class=\"experiences--cover\">
        <div class=\"contenedor\">
            <div class=\"experiences--cover-title\">
                <h5 class=\"block--subtitle\">{{ 'Schedule'|t }}</h5>
                <h4 class=\"block--title\">{{ 'Experiences'|t }}</h4>
            </div>
            <div class=\"experiences--cover-text\">
                {{ content.field_hp_experiences_intro }} {# Campo Experiences Text #}
            </div>
        </div>
    </div>

    {% if content.field_hp_experiences[0] %}
        <div class=\"experiences-container\">
            {% for key, item in content.field_hp_experiences if key|first != '#' %}
            <div class=\"experiences-item experiences-item-0{{ loop.index }}\">{{ item }}</div>
            {% endfor %}
        </div>
    {% endif %} 

    <div class=\"experiences--link\">
        {{ content.field_hp_experiences_link }}
    </div>

    <div class=\"cta cta--ambar\">
            <div class=\"contenedor\">
                <h4 class=\"block--subtitle\">{{ node.field_hp_experiences_footer.value }}</h4>
                <p><a class=\"btn btn--red\" href=\"#tickets\">{{ 'Buy your tickets here'|t }}</a></p>
            </div>
    </div>

</div>

{% elseif view_mode == 'new_tracks' %} {# Product full page (new tracks). #}

<div{{ attributes.addClass('div-new-tracks').setAttribute('id', 'tracks') }}> 

{{ title_prefix }} 
{{ title_suffix }}

    <div class=\"experiences--cover\">
        <div class=\"contenedor\">
            <div class=\"experiences--cover-title\"> 
                <h4 class=\"block--title\">{{ 'A track for every one'|t }}</h4>
            </div>
            <div class=\"experiences--cover-text\">
                {{ content.field_hp_tracks_intro }} {# Campo Experiences Text #}
            </div>
        </div>
    </div>

    {% if content.field_hp_tracks[0] %}
        <div class=\"experiences-container new-tracks-fix\">
            {% for key, item in content.field_hp_tracks if key|first != '#' %}
            <div class=\"experiences-item experiences-item-0{{ loop.index }}\">{{ item }}</div>
            {% endfor %}
        </div>

        <div class=\"cta cta--ambar\">
            <div class=\"contenedor\">
                <h4 class=\"block--subtitle\">{{ node.field_hp_track_footer.value }}</h4>
                <p><a class=\"btn btn--red\" href=\"#tickets\">{{ 'Buy your tickets here'|t }}</a></p>
            </div>
        </div>
    {% endif %}  

</div>

{% elseif view_mode == 'why_attend' %} {# Product full page (why attend). #}

<div{{ attributes.addClass('div-experiences').setAttribute('id', 'attend') }}> 

{{ title_prefix }} 
{{ title_suffix }}

    <div class=\"experiences--cover\">
        <div class=\"contenedor\">
            <div class=\"experiences--cover-title\"> 
                <h4 class=\"block--title\">{{ 'Why Attend?'|t }}</h4>
            </div>
            <div class=\"experiences--cover-text\">
                {{ content.field_why_attend_intro }} {# Campo Experiences Text #}
            </div>
        </div>
    </div>

    {% if content.field_why_attend[0] %}
        <div class=\"experiences-container why-attend-fix\">
            {% for key, item in content.field_why_attend if key|first != '#' %}
            <div class=\"experiences-item experiences-item-0{{ loop.index }}\">{{ item }}</div>
            {% endfor %}
        </div>

        <div class=\"cta cta--ambar\">
            <div class=\"contenedor\">
                <h4 class=\"block--subtitle\">{{ node.field_why_attend_footer.value }}</h4>
                <p><a class=\"btn btn--red\" href=\"#tickets\">{{ 'Buy your tickets here'|t }}</a></p>
            </div>
        </div>
    {% endif %}  

</div> 

{% elseif view_mode == 'modal' %} {# Product full page (modal-experiences). #}

<div{{ attributes.addClass('div-modal') }}> 

{{ title_prefix }} 
{{ title_suffix }}

    {% if content.field_hp_experiences[0] %} 
        {% for key, item in content.field_hp_experiences if key|first != '#' %}
        <div class=\"modal-item modal-item-0{{ loop.index }}\">{{ item }}</div>
        {% endfor %} 
    {% endif %} 

    {% if content.field_why_attend[0] %} 
        {% for key, item in content.field_why_attend if key|first != '#' %}
        <div class=\"modal-item modal-item-0{{ loop.index }}\">{{ item }}</div>
        {% endfor %} 
    {% endif %} 

    {% if content.field_hp_tracks[0] %} 
        {% for key, item in content.field_hp_tracks if key|first != '#' %}
        <div class=\"modal-item modal-item-0{{ loop.index }}\">{{ item }}</div>
        {% endfor %} 
    {% endif %} 

</div> 

{% elseif view_mode == 'tracks' %} {# Product full page (tracks). #}

<div{{ attributes.addClass('div-tracks') }}> 

{{ title_prefix }} 
{{ title_suffix }}

    <div class=\"contenedor\"> 

    {% if node.field_hp_track_bottom.value|length > 0 %} 

        <div class=\"div-tracks-container\">

            <h4 class=\"block--title\">{{ 'Our Values'|t }}</h4>

            <div class=\"tracks-container\"> 
                {% if content.field_hp_track_bottom[0] %}
                    <div class=\"tracks-wrapper\">
                        {% for key, item in content.field_hp_track_bottom if key|first != '#' %}
                            <div class=\"tracks-item tracks-item-0{{ loop.index }}\">{{ item }}</div>
                        {% endfor %}
                    </div> 
                {% endif %} 
            </div>

        </div>

    {% endif %}  

    </div>

</div> 

{% elseif view_mode == 'joining' %} {# Product full page (joining). #}

<div{{ attributes.addClass('div-joining') }}> 

{{ title_prefix }} 
{{ title_suffix }}

{% if node.field_joining.value|length > 0 %} 

        <div class=\"swiper-container swipper-container-navigation joining-container\">
            <h4 class=\"block--title\">{{ \"Who is Joining?\"|t }}</h4>
            {% if content.field_joining[0] %}
                <div class=\"swiper-wrapper\">
                    {% for key, item in content.field_joining if key|first != '#' %}
                        <div class=\"swiper-slide slide-0{{ loop.index }}\">{{ item }}</div>
                    {% endfor %}
                </div>
                <div class=\"swiper-button-next\"></div>
                <div class=\"swiper-button-prev\"></div>
            {% endif %} 
        </div>

{% endif %}  

</div> 

{% else %} {# Product full page (default). #}

{% endif %}", "themes/custom/arky8/templates/content/node--homepage.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/content/node--homepage.html.twig");
    }
}
