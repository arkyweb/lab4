<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/menu/menu--main.html.twig */
class __TwigTemplate_341ae8ca60636f5824ea243dcec265c7e4e8373c4422d6fdf065f299c5c4ee7f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["import" => 1, "macro" => 4, "set" => 8, "if" => 19, "for" => 27];
        $filters = ["escape" => 22, "keys" => 29];
        $functions = ["menus_attribute" => 29, "link" => 76];

        try {
            $this->sandbox->checkSecurity(
                ['import', 'macro', 'set', 'if', 'for'],
                ['escape', 'keys'],
                ['menus_attribute', 'link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["menus"] = $this;
        // line 2
        echo "
";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0));
        echo "
";
    }

    // line 4
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            echo "  
  ";
            // line 5
            $context["menus"] = $this;
            // line 6
            echo "
";
            // line 8
            $context["menu_classes"] = [0 => "menu"];
            // line 12
            echo "  ";
            // line 13
            echo "  ";
            // line 14
            $context["submenu_classes"] = [0 => "menu-sub"];
            // line 18
            echo "
  ";
            // line 19
            if (($context["items"] ?? null)) {
                // line 20
                echo "
    ";
                // line 21
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 22
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["menu_classes"] ?? null)], "method")), "html", null, true);
                    echo ">
    ";
                } else {
                    // line 24
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "removeClass", [0 => ($context["menu_classes"] ?? null)], "method"), "addClass", [0 => ($context["submenu_classes"] ?? null)], "method")), "html", null, true);
                    echo ">
    ";
                }
                // line 26
                echo "
    ";
                // line 27
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 28
                    echo "
      ";
                    // line 29
                    $context["menu_attributes"] = $this->env->getExtension('Drupal\menus_attribute\Template\TwigExtension')->menusAttribute($this->sandbox->ensureToStringAllowed($this->getAttribute(twig_get_array_keys_filter($this->sandbox->ensureToStringAllowed(($context["items"] ?? null))), $this->getAttribute($context["loop"], "index0", []), [], "array")));
                    // line 30
                    echo "      ";
                    // line 31
                    $context["classes"] = [0 => "menu-item", 1 => (($this->getAttribute(                    // line 33
$context["item"], "is_expanded", [])) ? ("menu-item--expanded") : ("")), 2 => (($this->getAttribute(                    // line 34
$context["item"], "is_collapsed", [])) ? ("menu-item--collapsed") : ("")), 3 => (($this->getAttribute(                    // line 35
$context["item"], "in_active_trail", [])) ? ("menu-item--active-trail") : ("")), 4 => (($this->getAttribute($this->getAttribute(                    // line 36
($context["menu_attributes"] ?? null), "item", []), "class", [])) ? ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "class", [])) : (""))];
                    // line 39
                    echo "      ";
                    // line 40
                    $context["link_classes"] = [0 => "menu-link", 1 => (($this->getAttribute(                    // line 42
$context["item"], "is_expanded", [])) ? ("menu-link-expanded") : (""))];
                    // line 45
                    echo "
      ";
                    // line 47
                    $context["classes_sub"] = [0 => "menu-item-sub", 1 => (($this->getAttribute(                    // line 49
$context["item"], "is_expanded", [])) ? ("menu-item--expanded-sub") : ("")), 2 => (($this->getAttribute(                    // line 50
$context["item"], "is_collapsed", [])) ? ("menu-item--collapsed-sub") : ("")), 3 => (($this->getAttribute(                    // line 51
$context["item"], "in_active_trail", [])) ? ("menu-item--active-trail-sub") : ("")), 4 => (($this->getAttribute($this->getAttribute(                    // line 52
($context["menu_attributes"] ?? null), "item", []), "class", [])) ? ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "class", [])) : (""))];
                    // line 55
                    echo "      ";
                    // line 56
                    $context["link_classes_sub"] = [0 => "menu-link-sub"];
                    // line 60
                    echo "
      ";
                    // line 61
                    if ((($context["menu_level"] ?? null) == 0)) {
                        // line 62
                        echo "
      <li data-counter=\"menu-item-0";
                        // line 63
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\" ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
                        echo "

        ";
                        // line 65
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])) {
                            // line 66
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "id", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 68
                        echo "        ";
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])) {
                            // line 69
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "style", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 71
                        echo "      >


        ";
                        // line 74
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 75
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute(                            // line 77
$context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute(                            // line 78
$context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(                            // line 79
$context["item"], "attributes", []), "removeClass", [0 =>                             // line 80
($context["classes"] ?? null)], "method"), "addClass", [0 =>                             // line 81
($context["link_classes"] ?? null)], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(                            // line 82
($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                            echo " 
          ";
                            // line 83
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)));
                            echo "
        ";
                        } else {
                            // line 85
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "removeClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => ($context["link_classes"] ?? null)], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                            echo "
        ";
                        }
                        // line 87
                        echo "
      </li>

      ";
                    } else {
                        // line 91
                        echo "
      <li data-counter=\"menu-item-sub-0";
                        // line 92
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\" ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes_sub"] ?? null)], "method")), "html", null, true);
                        echo "

        ";
                        // line 94
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])) {
                            // line 95
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "id", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 97
                        echo "        ";
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])) {
                            // line 98
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "style", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 100
                        echo "      >
        ";
                        // line 101
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "removeClass", [0 => ($context["classes_sub"] ?? null)], "method"), "addClass", [0 => ($context["link_classes_sub"] ?? null)], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                        echo "

        ";
                        // line 103
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 104
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)));
                            echo "
        ";
                        }
                        // line 106
                        echo "
      </li>

      ";
                    }
                    // line 110
                    echo "
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 112
                echo "
    </ul>
  ";
            }
        } catch (\Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (\Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/menu/menu--main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  299 => 112,  284 => 110,  278 => 106,  272 => 104,  270 => 103,  265 => 101,  262 => 100,  256 => 98,  253 => 97,  247 => 95,  245 => 94,  238 => 92,  235 => 91,  229 => 87,  223 => 85,  218 => 83,  214 => 82,  213 => 81,  212 => 80,  211 => 79,  210 => 78,  209 => 77,  207 => 75,  205 => 74,  200 => 71,  194 => 69,  191 => 68,  185 => 66,  183 => 65,  176 => 63,  173 => 62,  171 => 61,  168 => 60,  166 => 56,  164 => 55,  162 => 52,  161 => 51,  160 => 50,  159 => 49,  158 => 47,  155 => 45,  153 => 42,  152 => 40,  150 => 39,  148 => 36,  147 => 35,  146 => 34,  145 => 33,  144 => 31,  142 => 30,  140 => 29,  137 => 28,  120 => 27,  117 => 26,  111 => 24,  105 => 22,  103 => 21,  100 => 20,  98 => 19,  95 => 18,  93 => 14,  91 => 13,  89 => 12,  87 => 8,  84 => 6,  82 => 5,  66 => 4,  60 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% import _self as menus %}

{{ menus.menu_links(items, attributes, 0) }}
{% macro menu_links(items, attributes, menu_level) %}  
  {% import _self as menus %}

{%
    set menu_classes = [
      'menu',
    ]
  %}
  {# 1. #}
  {%
    set submenu_classes = [
      'menu-sub', 
    ]
%}

  {% if items %}

    {% if menu_level == 0 %}
      <ul{{ attributes.addClass(menu_classes) }}>
    {% else %}
      <ul{{ attributes.removeClass(menu_classes).addClass(submenu_classes) }}>
    {% endif %}

    {% for item in items %}

      {% set menu_attributes = menus_attribute(items|keys[loop.index0]) %}
      {%
        set classes = [
          'menu-item',
          item.is_expanded ? 'menu-item--expanded',
          item.is_collapsed ? 'menu-item--collapsed',
          item.in_active_trail ? 'menu-item--active-trail',
          menu_attributes.item.class ? menu_attributes.item.class
        ]
      %}
      {%
        set link_classes = [
          'menu-link',
          item.is_expanded ? 'menu-link-expanded',
        ]
      %}

      {%
        set classes_sub = [
          'menu-item-sub',
          item.is_expanded ? 'menu-item--expanded-sub',
          item.is_collapsed ? 'menu-item--collapsed-sub',
          item.in_active_trail ? 'menu-item--active-trail-sub',
          menu_attributes.item.class ? menu_attributes.item.class
        ]
      %}
      {%
        set link_classes_sub = [
          'menu-link-sub',
        ]
      %}

      {% if menu_level == 0 %}

      <li data-counter=\"menu-item-0{{ loop.index }}\" {{ item.attributes.addClass(classes) }}

        {% if menu_attributes.item.id %}
          {{ item.attributes.setAttribute('id', menu_attributes.item.id) }}
        {% endif %}
        {% if menu_attributes.item.style %}
          {{ item.attributes.setAttribute('style', menu_attributes.item.style) }}
        {% endif %}
      >


        {% if item.below %}
          {{ 
            link(
              item.title, 
              item.url, 
              item.attributes
                .removeClass(classes)
                .addClass(link_classes), 
              menu_attributes.link) }} 
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% else %}
          {{ link(item.title, item.url, item.attributes.removeClass(classes).addClass(link_classes), menu_attributes.link) }}
        {% endif %}

      </li>

      {% else %}

      <li data-counter=\"menu-item-sub-0{{ loop.index }}\" {{ item.attributes.addClass(classes_sub) }}

        {% if menu_attributes.item.id %}
          {{ item.attributes.setAttribute('id', menu_attributes.item.id) }}
        {% endif %}
        {% if menu_attributes.item.style %}
          {{ item.attributes.setAttribute('style', menu_attributes.item.style) }}
        {% endif %}
      >
        {{ link(item.title, item.url, item.attributes.removeClass(classes_sub).addClass(link_classes_sub), menu_attributes.link) }}

        {% if item.below %}
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% endif %}

      </li>

      {% endif %}

    {% endfor %}

    </ul>
  {% endif %}
{% endmacro %}", "themes/custom/arky8/templates/menu/menu--main.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/menu/menu--main.html.twig");
    }
}
