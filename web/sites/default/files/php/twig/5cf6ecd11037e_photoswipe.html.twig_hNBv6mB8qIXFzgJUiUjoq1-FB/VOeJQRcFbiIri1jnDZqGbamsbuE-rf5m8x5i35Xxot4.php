<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/@includes/photoswipe.html.twig */
class __TwigTemplate_738966a97c5d683da2ebb343d929f3106c1db42937c30dcb4460414fed9eadd0 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1, "for" => 3, "set" => 6];
        $filters = ["first" => 3, "escape" => 14, "t" => 18];
        $functions = ["file_url" => 6, "attach_library" => 26];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for', 'set'],
                ['first', 'escape', 't'],
                ['file_url', 'attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_colorbox", []), 0, [], "array")) {
            // line 2
            echo "<div class=\"photoswipe-gallery\" itemscope itemtype=\"http://schema.org/ImageGallery\">
";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_colorbox", []));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                if ((twig_first($this->env, $context["key"]) != "#")) {
                    echo " 

";
                    // line 6
                    $context["ITEM_URI"] = call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "#item", [], "array"), "entity", []), "fileuri", []))]);
                    // line 7
                    $context["ITEM_ALT"] = $this->getAttribute($this->getAttribute($context["item"], "#item", [], "array"), "alt", []);
                    // line 8
                    $context["ITEM_TITLE"] = $this->getAttribute($this->getAttribute($context["item"], "#item", [], "array"), "title", []);
                    // line 9
                    $context["ITEM_HEIGHT"] = $this->getAttribute($this->getAttribute($context["item"], "#item", [], "array"), "height", []);
                    // line 10
                    $context["ITEM_WIDTH"] = $this->getAttribute($this->getAttribute($context["item"], "#item", [], "array"), "width", []);
                    // line 11
                    echo "
\t<figure itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\"> 
\t\t";
                    // line 13
                    echo " 
\t\t<a href=\"";
                    // line 14
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ITEM_URI"] ?? null)), "html", null, true);
                    echo "\" data-size=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ITEM_WIDTH"] ?? null)), "html", null, true);
                    echo "x";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ITEM_HEIGHT"] ?? null)), "html", null, true);
                    echo "\" itemprop=\"contentUrl\">
\t\t\t<img src=\"";
                    // line 15
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ITEM_URI"] ?? null)), "html", null, true);
                    echo "\" title=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ITEM_TITLE"] ?? null)), "html", null, true);
                    echo "\" alt=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ITEM_ALT"] ?? null)), "html", null, true);
                    echo "\" itemprop=\"thumbnail\" /> 
\t\t</a>
        <figcaption>
            <p>";
                    // line 18
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ITEM_TITLE"] ?? null)), "html", null, true);
                    echo " - ";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Click to the image"));
                    echo "</p>
        </figcaption>
\t</figure>

";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "</div>
";
        }
        // line 25
        echo "
";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("arky8/include_photoswipe"), "html", null, true);
        echo "

";
        // line 29
        echo "<div class=\"pswp\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">

    ";
        // line 33
        echo "    <div class=\"pswp__bg\"></div>

    ";
        // line 36
        echo "    <div class=\"pswp__scroll-wrap\">

        ";
        // line 41
        echo "        <div class=\"pswp__container\">
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
        </div>

        ";
        // line 48
        echo "        <div class=\"pswp__ui pswp__ui--hidden\">

            <div class=\"pswp__top-bar\">

               ";
        // line 53
        echo "
                <div class=\"pswp__counter\"></div>

                <button class=\"pswp__button pswp__button--close\" title=\"Close (Esc)\"></button>

                <button class=\"pswp__button pswp__button--share\" title=\"Share\"></button>

                <button class=\"pswp__button pswp__button--fs\" title=\"Toggle fullscreen\"></button>

                <button class=\"pswp__button pswp__button--zoom\" title=\"Zoom in/out\"></button>

               ";
        // line 66
        echo "                <div class=\"pswp__preloader\">
                    <div class=\"pswp__preloader__icn\">
                      <div class=\"pswp__preloader__cut\">
                        <div class=\"pswp__preloader__donut\"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class=\"pswp__share-modal pswp__share-modal--hidden pswp__single-tap\">
                <div class=\"pswp__share-tooltip\"></div> 
            </div>

            <button class=\"pswp__button pswp__button--arrow--left\" title=\"Previous (arrow left)\">
            </button>

            <button class=\"pswp__button pswp__button--arrow--right\" title=\"Next (arrow right)\">
            </button>

            <div class=\"pswp__caption\">
                <div class=\"pswp__caption__center\"></div>
            </div>

        </div>

    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/@includes/photoswipe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 66,  155 => 53,  149 => 48,  141 => 41,  137 => 36,  133 => 33,  129 => 29,  124 => 26,  121 => 25,  117 => 23,  103 => 18,  93 => 15,  85 => 14,  82 => 13,  78 => 11,  76 => 10,  74 => 9,  72 => 8,  70 => 7,  68 => 6,  60 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if content.field_colorbox[0] %}
<div class=\"photoswipe-gallery\" itemscope itemtype=\"http://schema.org/ImageGallery\">
{% for key, item in content.field_colorbox if key|first != '#' %} 

{# VARIABLES solo para el key #}
{% set ITEM_URI = file_url(item['#item'].entity.fileuri) %}
{% set ITEM_ALT = item['#item'].alt %}
{% set ITEM_TITLE = item['#item'].title %}
{% set ITEM_HEIGHT = item['#item'].height %}
{% set ITEM_WIDTH = item['#item'].width %}

\t<figure itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\"> 
\t\t{# dirige al image-formatter.html.twig {{ item }} #} 
\t\t<a href=\"{{ ITEM_URI }}\" data-size=\"{{ ITEM_WIDTH }}x{{ ITEM_HEIGHT }}\" itemprop=\"contentUrl\">
\t\t\t<img src=\"{{ ITEM_URI }}\" title=\"{{ ITEM_TITLE }}\" alt=\"{{ ITEM_ALT }}\" itemprop=\"thumbnail\" /> 
\t\t</a>
        <figcaption>
            <p>{{ ITEM_TITLE }} - {{ 'Click to the image'|t }}</p>
        </figcaption>
\t</figure>

{% endfor %}
</div>
{% endif %}

{{ attach_library('arky8/include_photoswipe')}}

{# Root element of PhotoSwipe. Must have class pswp. #}
<div class=\"pswp\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">

    {# Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). #}
    <div class=\"pswp__bg\"></div>

    {# Slides wrapper with overflow:hidden.  #}
    <div class=\"pswp__scroll-wrap\">

        {# Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on.  #}
        <div class=\"pswp__container\">
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
            <div class=\"pswp__item\"></div>
        </div>

        {# Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed.  #}
        <div class=\"pswp__ui pswp__ui--hidden\">

            <div class=\"pswp__top-bar\">

               {#  Controls are self-explanatory. Order can be changed.  #}

                <div class=\"pswp__counter\"></div>

                <button class=\"pswp__button pswp__button--close\" title=\"Close (Esc)\"></button>

                <button class=\"pswp__button pswp__button--share\" title=\"Share\"></button>

                <button class=\"pswp__button pswp__button--fs\" title=\"Toggle fullscreen\"></button>

                <button class=\"pswp__button pswp__button--zoom\" title=\"Zoom in/out\"></button>

               {# Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR 
                 element will get class pswp__preloader--active when preloader is running #}
                <div class=\"pswp__preloader\">
                    <div class=\"pswp__preloader__icn\">
                      <div class=\"pswp__preloader__cut\">
                        <div class=\"pswp__preloader__donut\"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class=\"pswp__share-modal pswp__share-modal--hidden pswp__single-tap\">
                <div class=\"pswp__share-tooltip\"></div> 
            </div>

            <button class=\"pswp__button pswp__button--arrow--left\" title=\"Previous (arrow left)\">
            </button>

            <button class=\"pswp__button pswp__button--arrow--right\" title=\"Next (arrow right)\">
            </button>

            <div class=\"pswp__caption\">
                <div class=\"pswp__caption__center\"></div>
            </div>

        </div>

    </div>

</div>", "themes/custom/arky8/templates/@includes/photoswipe.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/@includes/photoswipe.html.twig");
    }
}
