<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/styles/arkyweb.css */
class __TwigTemplate_a5b8895c26e0bdf81a52e3316f4b37299a46e1c82c98a7cb2539c000abe3e9ef extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "body, body * { font-size: 1em; margin: 0; padding: 0; text-decoration: inherit; }
*, *:before, *:after { box-sizing: border-box; }

a { color: inherit; }
ol, ul { list-style: none; }
h1, h2, h3, b, strong, h4, h5, h6 { font-weight: 700; line-height: 1; } 
figure { overflow: hidden; position: relative; }
img, svg { display: block; height: auto; max-width: 200px; width: 100%; }
fieldset { border: 0; }
.visually-hidden,
.hidden,
.sr-only { display: none; }

.wf-active body {
    font-family: Roboto, Arial, sans-serif;
}

:root {
    --espacios: 1rem;
    --lab-gray: #555;
    --lab-ambar: #FBAE00;
    --lab-black: #131415;
    --lab-blue: #182DB3;
    --lab-green: #04D133;
    --lab-yellow: #FDD013;
    --lab-red: #de001a;
}
@media (min-width: 600px) {
:root {
    --espacios: 2rem;
}   
}
@media (min-width: 900px) {
:root {
    --espacios: 3rem;
}   
}
@media (min-width: 1200px) {
:root {
    --espacios: 4.5rem;
}   
}
@media (min-width: 1440px) {
:root {
    --espacios: 6.25rem;
}   
}

body {
    font-family: Helvetica, Arial, sans-serif;
    color: #555;
    font-size: 13px;
}
@media (min-width: 900px) {
body {
    font-size: 16px;
}
}

.contenedor {
    padding-left: var(--espacios);
    padding-right: var(--espacios);
    width: 100%;
} 

.arkyweb {
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    max-width: 100%;
    position: relative;
}
.headerpage,
.footerpage,
.mainpage { 
    flex-basis: 0;
    flex-shrink: 0;
    flex-grow: 0;
}
.mainpage {  
    flex-grow: 1;
}

@media (max-width: 899px) {
.mi-menu--main {
    display: none;
}
}

.modalpage {
    position: relative;
    z-index: 20;
}
.modal {
    display: none;
}
.modal-open {
    overflow-x: hidden;
    overflow-y: hidden;
}";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/styles/arkyweb.css";
    }

    public function getDebugInfo()
    {
        return array (  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("body, body * { font-size: 1em; margin: 0; padding: 0; text-decoration: inherit; }
*, *:before, *:after { box-sizing: border-box; }

a { color: inherit; }
ol, ul { list-style: none; }
h1, h2, h3, b, strong, h4, h5, h6 { font-weight: 700; line-height: 1; } 
figure { overflow: hidden; position: relative; }
img, svg { display: block; height: auto; max-width: 200px; width: 100%; }
fieldset { border: 0; }
.visually-hidden,
.hidden,
.sr-only { display: none; }

.wf-active body {
    font-family: Roboto, Arial, sans-serif;
}

:root {
    --espacios: 1rem;
    --lab-gray: #555;
    --lab-ambar: #FBAE00;
    --lab-black: #131415;
    --lab-blue: #182DB3;
    --lab-green: #04D133;
    --lab-yellow: #FDD013;
    --lab-red: #de001a;
}
@media (min-width: 600px) {
:root {
    --espacios: 2rem;
}   
}
@media (min-width: 900px) {
:root {
    --espacios: 3rem;
}   
}
@media (min-width: 1200px) {
:root {
    --espacios: 4.5rem;
}   
}
@media (min-width: 1440px) {
:root {
    --espacios: 6.25rem;
}   
}

body {
    font-family: Helvetica, Arial, sans-serif;
    color: #555;
    font-size: 13px;
}
@media (min-width: 900px) {
body {
    font-size: 16px;
}
}

.contenedor {
    padding-left: var(--espacios);
    padding-right: var(--espacios);
    width: 100%;
} 

.arkyweb {
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    max-width: 100%;
    position: relative;
}
.headerpage,
.footerpage,
.mainpage { 
    flex-basis: 0;
    flex-shrink: 0;
    flex-grow: 0;
}
.mainpage {  
    flex-grow: 1;
}

@media (max-width: 899px) {
.mi-menu--main {
    display: none;
}
}

.modalpage {
    position: relative;
    z-index: 20;
}
.modal {
    display: none;
}
.modal-open {
    overflow-x: hidden;
    overflow-y: hidden;
}", "themes/custom/arky8/templates/styles/arkyweb.css", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/styles/arkyweb.css");
    }
}
