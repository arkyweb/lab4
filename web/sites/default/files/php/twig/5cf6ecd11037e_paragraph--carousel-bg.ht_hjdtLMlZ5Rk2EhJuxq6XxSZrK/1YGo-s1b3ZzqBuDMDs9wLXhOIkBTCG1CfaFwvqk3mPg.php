<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/paragraph/paragraph--carousel-bg.html.twig */
class __TwigTemplate_1697f8f1358c56f4c7f0b63503b3e933a138fa75a642f732851fe527c16dbd6b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1];
        $filters = ["length" => 1, "escape" => 5, "t" => 40];
        $functions = ["file_url" => 6];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['length', 'escape', 't'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_bg", []), 0, [], "array"), "value", [])) > 0)) {
            echo " ";
            echo " 

<style>

[data-carousel*='";
            // line 5
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_id", []), "value", [])), "html", null, true);
            echo "'].visible {
    background-image: url('";
            // line 6
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_bg", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "');
}

</style>

<article";
            // line 11
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "carousel bglazy"], "method")), "html", null, true);
            echo " data-carousel=\"carousel-";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_id", []), "value", [])), "html", null, true);
            echo "\"> 
";
        } else {
            // line 13
            echo "<article";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "carousel"], "method")), "html", null, true);
            echo ">
";
        }
        // line 15
        echo "
";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

<div class=\"contenedor\">

    ";
        // line 21
        if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_main_logo", []))) {
            echo " 
        <figure class=\"carousel-logo\">
            <img 
                class=\"lazy\" alt=\"\" title=\"\"
                data-src=\"";
            // line 25
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_main_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "\" 
                data-srcset=\"";
            // line 26
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_main_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "\" 
                loading=\"lazy\" />
            <noscript>
              <img alt=\"\" title=\"\" src=\"";
            // line 29
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_main_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "}\">
            </noscript> 
        </figure>
    ";
        }
        // line 33
        echo "
    <div class=\"carousel-text\">
        ";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_pcs_txt", [])), "html", null, true);
        echo "
    </div> 

    ";
        // line 38
        if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_sec_logo", []))) {
            echo " 
        <p class=\"carousel-sub-logo\">
            <span>";
            // line 40
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Powered by:"));
            echo "</span>
            <img 
                class=\"lazy\" alt=\"\" title=\"\"
                data-src=\"";
            // line 43
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_sec_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "\" 
                data-srcset=\"";
            // line 44
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_sec_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "\" 
                loading=\"lazy\" />
            <noscript>
              <img alt=\"\" title=\"\" src=\"";
            // line 47
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_sec_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "}\">
            </noscript> 
        </p>

    ";
        }
        // line 52
        echo "
    ";
        // line 53
        if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_pcs_link", []))) {
            echo " 
    <div class=\"carousel-link\">
        ";
            // line 55
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_pcs_link", [])), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 58
        echo "
</div>

";
        // line 61
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_deco_left", []), 0, [], "array"), "value", [])) > 0)) {
            // line 62
            echo "    <style>
    .deco-left.visible {
        background-image: url('";
            // line 64
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_deco_left", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "')
    }
    </style>
        <div class=\"deco deco-left bglazy\">
        </div>
";
        }
        // line 70
        echo "
";
        // line 71
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_decor_right", []), 0, [], "array"), "value", [])) > 0)) {
            // line 72
            echo "    <style>
    .deco-right.visible {
        background-image: url('";
            // line 74
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_decor_right", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "')
    }
    </style>
        <div class=\"deco deco-right bglazy\">
        </div>
";
        }
        // line 80
        echo "
</article>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/paragraph/paragraph--carousel-bg.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 80,  208 => 74,  204 => 72,  202 => 71,  199 => 70,  190 => 64,  186 => 62,  184 => 61,  179 => 58,  173 => 55,  168 => 53,  165 => 52,  157 => 47,  151 => 44,  147 => 43,  141 => 40,  136 => 38,  130 => 35,  126 => 33,  119 => 29,  113 => 26,  109 => 25,  102 => 21,  95 => 17,  91 => 16,  88 => 15,  82 => 13,  75 => 11,  67 => 6,  63 => 5,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if paragraph.field_pcs_bg[0].value|length > 0 %} {# Campo Carousel Bg #} 

<style>

[data-carousel*='{{ paragraph.field_pcs_id.value }}'].visible {
    background-image: url('{{ file_url(paragraph.field_pcs_bg.entity.fileuri) }}');
}

</style>

<article{{ attributes.addClass('carousel bglazy') }} data-carousel=\"carousel-{{ paragraph.field_pcs_id.value }}\"> 
{% else %}
<article{{ attributes.addClass('carousel') }}>
{% endif %}

{{ title_prefix }} 
{{ title_suffix }}

<div class=\"contenedor\">

    {% if paragraph.field_pcs_main_logo is not empty %} 
        <figure class=\"carousel-logo\">
            <img 
                class=\"lazy\" alt=\"\" title=\"\"
                data-src=\"{{ file_url(paragraph.field_pcs_main_logo.entity.fileuri) }}\" 
                data-srcset=\"{{ file_url(paragraph.field_pcs_main_logo.entity.fileuri) }}\" 
                loading=\"lazy\" />
            <noscript>
              <img alt=\"\" title=\"\" src=\"{{ file_url(paragraph.field_pcs_main_logo.entity.fileuri) }}}\">
            </noscript> 
        </figure>
    {% endif %}

    <div class=\"carousel-text\">
        {{ content.field_pcs_txt }}
    </div> 

    {% if paragraph.field_pcs_sec_logo is not empty %} 
        <p class=\"carousel-sub-logo\">
            <span>{{ 'Powered by:'|t }}</span>
            <img 
                class=\"lazy\" alt=\"\" title=\"\"
                data-src=\"{{ file_url(paragraph.field_pcs_sec_logo.entity.fileuri) }}\" 
                data-srcset=\"{{ file_url(paragraph.field_pcs_sec_logo.entity.fileuri) }}\" 
                loading=\"lazy\" />
            <noscript>
              <img alt=\"\" title=\"\" src=\"{{ file_url(paragraph.field_pcs_sec_logo.entity.fileuri) }}}\">
            </noscript> 
        </p>

    {% endif %}

    {% if paragraph.field_pcs_link is not empty %} 
    <div class=\"carousel-link\">
        {{ content.field_pcs_link }}
    </div>
    {% endif %}

</div>

{% if paragraph.field_deco_left[0].value|length > 0 %}
    <style>
    .deco-left.visible {
        background-image: url('{{ file_url(paragraph.field_deco_left.entity.fileuri) }}')
    }
    </style>
        <div class=\"deco deco-left bglazy\">
        </div>
{% endif %}

{% if paragraph.field_decor_right[0].value|length > 0 %}
    <style>
    .deco-right.visible {
        background-image: url('{{ file_url(paragraph.field_decor_right.entity.fileuri) }}')
    }
    </style>
        <div class=\"deco deco-right bglazy\">
        </div>
{% endif %}

</article>", "themes/custom/arky8/templates/paragraph/paragraph--carousel-bg.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/paragraph/paragraph--carousel-bg.html.twig");
    }
}
