<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/paragraph/paragraph--paragraph.html.twig */
class __TwigTemplate_c5b42e9cf6e8f69c608153680599d3a687d26e4707f891c97435f0420de749e7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 11];
        $filters = ["escape" => 1];
        $functions = ["file_url" => 13];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<article";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "paragraph"], "method")), "html", null, true);
        echo ">

";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

    <div class=\"paragraph--text\">
        <h4>";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pgh_title", []), "value", [])), "html", null, true);
        echo "</h4>
        ";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_pgh_txt", [])), "html", null, true);
        echo "
    </div> 
    
    ";
        // line 11
        if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_pgh_img", []))) {
            echo " 
    <figure class=\"paragraph--image\">
        <img src=\"";
            // line 13
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_pgh_img", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "\" />
    </figure>
    ";
        }
        // line 15
        echo " 

</article>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/paragraph/paragraph--paragraph.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 15,  86 => 13,  81 => 11,  75 => 8,  71 => 7,  65 => 4,  61 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<article{{ attributes.addClass('paragraph') }}>

{{ title_prefix }} 
{{ title_suffix }}

    <div class=\"paragraph--text\">
        <h4>{{ paragraph.field_pgh_title.value }}</h4>
        {{ content.field_pgh_txt }}
    </div> 
    
    {% if paragraph.field_pgh_img is not empty %} 
    <figure class=\"paragraph--image\">
        <img src=\"{{ file_url(paragraph.field_pgh_img.entity.fileuri) }}\" />
    </figure>
    {% endif %} 

</article>", "themes/custom/arky8/templates/paragraph/paragraph--paragraph.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/paragraph/paragraph--paragraph.html.twig");
    }
}
