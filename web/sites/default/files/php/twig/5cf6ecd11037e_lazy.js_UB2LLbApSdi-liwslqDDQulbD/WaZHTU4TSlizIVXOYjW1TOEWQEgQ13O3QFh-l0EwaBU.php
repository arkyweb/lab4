<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/scripts/lazy.js */
class __TwigTemplate_2d82f69b5252c23c9f0fe4b9abc081c0c441b83cf3756a56104308b7d79fcb10 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "document.addEventListener(\"DOMContentLoaded\", function() {
  var lazyImages = [].slice.call(document.querySelectorAll(\"img.lazy\"));;

  if (\"IntersectionObserver\" in window && \"IntersectionObserverEntry\" in window && \"intersectionRatio\" in window.IntersectionObserverEntry.prototype) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.srcset = lazyImage.dataset.srcset;
          //lazyImage.classList.remove(\"lazy\");
          lazyImage.classList.add(\"lazy-load\");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  }
});

document.addEventListener(\"DOMContentLoaded\", function() {
  var lazyBackgrounds = [].slice.call(document.querySelectorAll(\".bglazy\"));

  if (\"IntersectionObserver\" in window) {
    let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          entry.target.classList.add(\"visible\");
          lazyBackgroundObserver.unobserve(entry.target);
        }
      });
    });

    lazyBackgrounds.forEach(function(lazyBackground) {
      lazyBackgroundObserver.observe(lazyBackground);
    });
  }
});

setTimeout(function() {
var f = document.getElementById('frame-gmaps');
f.src = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1576.2320569727333!2d-122.4493426558481!3d37.80259669462671!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808586d510144681%3A0xe960db8a581aacf5!2sPalacio+de+Bellas+Artes!5e0!3m2!1ses!2spe!4v1559504056059!5m2!1ses!2spe'
}, 2000);

window.addEventListener('touchmove', event => {

}, {pasive: true});";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/scripts/lazy.js";
    }

    public function getDebugInfo()
    {
        return array (  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("document.addEventListener(\"DOMContentLoaded\", function() {
  var lazyImages = [].slice.call(document.querySelectorAll(\"img.lazy\"));;

  if (\"IntersectionObserver\" in window && \"IntersectionObserverEntry\" in window && \"intersectionRatio\" in window.IntersectionObserverEntry.prototype) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.srcset = lazyImage.dataset.srcset;
          //lazyImage.classList.remove(\"lazy\");
          lazyImage.classList.add(\"lazy-load\");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  }
});

document.addEventListener(\"DOMContentLoaded\", function() {
  var lazyBackgrounds = [].slice.call(document.querySelectorAll(\".bglazy\"));

  if (\"IntersectionObserver\" in window) {
    let lazyBackgroundObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          entry.target.classList.add(\"visible\");
          lazyBackgroundObserver.unobserve(entry.target);
        }
      });
    });

    lazyBackgrounds.forEach(function(lazyBackground) {
      lazyBackgroundObserver.observe(lazyBackground);
    });
  }
});

setTimeout(function() {
var f = document.getElementById('frame-gmaps');
f.src = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1576.2320569727333!2d-122.4493426558481!3d37.80259669462671!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808586d510144681%3A0xe960db8a581aacf5!2sPalacio+de+Bellas+Artes!5e0!3m2!1ses!2spe!4v1559504056059!5m2!1ses!2spe'
}, 2000);

window.addEventListener('touchmove', event => {

}, {pasive: true});", "themes/custom/arky8/templates/scripts/lazy.js", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/scripts/lazy.js");
    }
}
