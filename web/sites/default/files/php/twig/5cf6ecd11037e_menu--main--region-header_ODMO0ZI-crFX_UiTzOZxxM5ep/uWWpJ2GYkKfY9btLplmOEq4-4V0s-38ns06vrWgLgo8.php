<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/menu/menu--main--region-header.html.twig */
class __TwigTemplate_e10c147be63d421a46cec3163083432162fff10550f80b463c5859b9eef8bc1b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["import" => 1, "macro" => 4, "set" => 8, "if" => 20, "for" => 28];
        $filters = ["escape" => 23, "keys" => 30];
        $functions = ["menus_attribute" => 30, "link" => 77];

        try {
            $this->sandbox->checkSecurity(
                ['import', 'macro', 'set', 'if', 'for'],
                ['escape', 'keys'],
                ['menus_attribute', 'link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["menus"] = $this;
        // line 2
        echo "
";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0));
        echo "
";
    }

    // line 4
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            echo "  
  ";
            // line 5
            $context["menus"] = $this;
            // line 6
            echo "
";
            // line 8
            $context["menu_classes"] = [0 => "menu", 1 => "menu-main mi-menu-main"];
            // line 13
            echo "  ";
            // line 14
            echo "  ";
            // line 15
            $context["submenu_classes"] = [0 => "menu-sub mi-menu-sub dropdown-menu"];
            // line 19
            echo "
  ";
            // line 20
            if (($context["items"] ?? null)) {
                // line 21
                echo "
    ";
                // line 22
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 23
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["menu_classes"] ?? null)], "method")), "html", null, true);
                    echo ">
    ";
                } else {
                    // line 25
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["attributes"] ?? null), "removeClass", [0 => ($context["menu_classes"] ?? null)], "method"), "addClass", [0 => ($context["submenu_classes"] ?? null)], "method")), "html", null, true);
                    echo ">
    ";
                }
                // line 27
                echo "
    ";
                // line 28
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 29
                    echo "
      ";
                    // line 30
                    $context["menu_attributes"] = $this->env->getExtension('Drupal\menus_attribute\Template\TwigExtension')->menusAttribute($this->sandbox->ensureToStringAllowed($this->getAttribute(twig_get_array_keys_filter($this->sandbox->ensureToStringAllowed(($context["items"] ?? null))), $this->getAttribute($context["loop"], "index0", []), [], "array")));
                    // line 31
                    echo "      ";
                    // line 32
                    $context["classes"] = [0 => "menu-item mi-menu-item", 1 => (($this->getAttribute(                    // line 34
$context["item"], "is_expanded", [])) ? ("menu-item--expanded") : ("menu-item--not-expanded")), 2 => (($this->getAttribute(                    // line 35
$context["item"], "is_collapsed", [])) ? ("menu-item--collapsed") : ("")), 3 => (($this->getAttribute(                    // line 36
$context["item"], "in_active_trail", [])) ? ("menu-item--active-trail") : ("")), 4 => (($this->getAttribute($this->getAttribute(                    // line 37
($context["menu_attributes"] ?? null), "item", []), "class", [])) ? ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "class", [])) : (""))];
                    // line 40
                    echo "      ";
                    // line 41
                    $context["link_classes"] = [0 => "menu-link mi-menu-link", 1 => (($this->getAttribute(                    // line 43
$context["item"], "is_expanded", [])) ? ("dropdown mi-dropdown") : ("not-dropdown"))];
                    // line 46
                    echo "
      ";
                    // line 48
                    $context["classes_sub"] = [0 => "menu-item-sub mi-menu-item-sub", 1 => (($this->getAttribute(                    // line 50
$context["item"], "is_expanded", [])) ? ("menu-item--expanded-sub") : ("")), 2 => (($this->getAttribute(                    // line 51
$context["item"], "is_collapsed", [])) ? ("menu-item--collapsed-sub") : ("")), 3 => (($this->getAttribute(                    // line 52
$context["item"], "in_active_trail", [])) ? ("menu-item--active-trail-sub") : ("")), 4 => (($this->getAttribute($this->getAttribute(                    // line 53
($context["menu_attributes"] ?? null), "item", []), "class", [])) ? ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "class", [])) : (""))];
                    // line 56
                    echo "      ";
                    // line 57
                    $context["link_classes_sub"] = [0 => "menu-link-sub mi-menu-link-sub"];
                    // line 61
                    echo "
      ";
                    // line 62
                    if ((($context["menu_level"] ?? null) == 0)) {
                        // line 63
                        echo "
      <li data-counter=\"menu-item-0";
                        // line 64
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\" ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
                        echo "

        ";
                        // line 66
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])) {
                            // line 67
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "id", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 69
                        echo "        ";
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])) {
                            // line 70
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "style", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 72
                        echo "      >


        ";
                        // line 75
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 76
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute(                            // line 78
$context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute(                            // line 79
$context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(                            // line 80
$context["item"], "attributes", []), "removeClass", [0 =>                             // line 81
($context["classes"] ?? null)], "method"), "addClass", [0 =>                             // line 82
($context["link_classes"] ?? null)], "method"), "setAttribute", [0 => "data-toggle", 1 => "dropdown"], "method"), "setAttribute", [0 => "aria-expanded", 1 => "false"], "method"), "setAttribute", [0 => "aria-haspopup", 1 => "true"], "method"), "setAttribute", [0 => "type", 1 => "button"], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(                            // line 87
($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                            echo "
          ";
                            // line 89
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)));
                            echo "
        ";
                        } else {
                            // line 91
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "removeClass", [0 => ($context["classes"] ?? null)], "method"), "addClass", [0 => ($context["link_classes"] ?? null)], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                            echo "
        ";
                        }
                        // line 93
                        echo "
      </li>

      ";
                    } else {
                        // line 97
                        echo "
      <li data-counter=\"menu-item-sub-0";
                        // line 98
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\" ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes_sub"] ?? null)], "method")), "html", null, true);
                        echo "

        ";
                        // line 100
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])) {
                            // line 101
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "id", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "id", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 103
                        echo "        ";
                        if ($this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])) {
                            // line 104
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "setAttribute", [0 => "style", 1 => $this->getAttribute($this->getAttribute(($context["menu_attributes"] ?? null), "item", []), "style", [])], "method")), "html", null, true);
                            echo "
        ";
                        }
                        // line 106
                        echo "      >
        ";
                        // line 107
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "removeClass", [0 => ($context["classes_sub"] ?? null)], "method"), "addClass", [0 => ($context["link_classes_sub"] ?? null)], "method")), $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["menu_attributes"] ?? null), "link", []))), "html", null, true);
                        echo "

        ";
                        // line 109
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 110
                            echo "          ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)));
                            echo "
        ";
                        }
                        // line 112
                        echo "
      </li>

      ";
                    }
                    // line 116
                    echo "
    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 118
                echo "
    </ul>
  ";
            }
        } catch (\Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (\Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/menu/menu--main--region-header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 118,  285 => 116,  279 => 112,  273 => 110,  271 => 109,  266 => 107,  263 => 106,  257 => 104,  254 => 103,  248 => 101,  246 => 100,  239 => 98,  236 => 97,  230 => 93,  224 => 91,  218 => 89,  214 => 87,  213 => 82,  212 => 81,  211 => 80,  210 => 79,  209 => 78,  207 => 76,  205 => 75,  200 => 72,  194 => 70,  191 => 69,  185 => 67,  183 => 66,  176 => 64,  173 => 63,  171 => 62,  168 => 61,  166 => 57,  164 => 56,  162 => 53,  161 => 52,  160 => 51,  159 => 50,  158 => 48,  155 => 46,  153 => 43,  152 => 41,  150 => 40,  148 => 37,  147 => 36,  146 => 35,  145 => 34,  144 => 32,  142 => 31,  140 => 30,  137 => 29,  120 => 28,  117 => 27,  111 => 25,  105 => 23,  103 => 22,  100 => 21,  98 => 20,  95 => 19,  93 => 15,  91 => 14,  89 => 13,  87 => 8,  84 => 6,  82 => 5,  66 => 4,  60 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% import _self as menus %}

{{ menus.menu_links(items, attributes, 0) }}
{% macro menu_links(items, attributes, menu_level) %}  
  {% import _self as menus %}

{%
    set menu_classes = [
      'menu',
      'menu-main mi-menu-main', 
    ]
  %}
  {# 1. #}
  {%
    set submenu_classes = [
      'menu-sub mi-menu-sub dropdown-menu', 
    ]
%}

  {% if items %}

    {% if menu_level == 0 %}
      <ul{{ attributes.addClass(menu_classes) }}>
    {% else %}
      <ul{{ attributes.removeClass(menu_classes).addClass(submenu_classes) }}>
    {% endif %}

    {% for item in items %}

      {% set menu_attributes = menus_attribute(items|keys[loop.index0]) %}
      {%
        set classes = [
          'menu-item mi-menu-item',
          item.is_expanded ? 'menu-item--expanded' : 'menu-item--not-expanded',
          item.is_collapsed ? 'menu-item--collapsed',
          item.in_active_trail ? 'menu-item--active-trail',
          menu_attributes.item.class ? menu_attributes.item.class
        ]
      %}
      {%
        set link_classes = [
          'menu-link mi-menu-link',
          item.is_expanded ? 'dropdown mi-dropdown' : 'not-dropdown',
        ]
      %}

      {%
        set classes_sub = [
          'menu-item-sub mi-menu-item-sub',
          item.is_expanded ? 'menu-item--expanded-sub',
          item.is_collapsed ? 'menu-item--collapsed-sub',
          item.in_active_trail ? 'menu-item--active-trail-sub',
          menu_attributes.item.class ? menu_attributes.item.class
        ]
      %}
      {%
        set link_classes_sub = [
          'menu-link-sub mi-menu-link-sub',
        ]
      %}

      {% if menu_level == 0 %}

      <li data-counter=\"menu-item-0{{ loop.index }}\" {{ item.attributes.addClass(classes) }}

        {% if menu_attributes.item.id %}
          {{ item.attributes.setAttribute('id', menu_attributes.item.id) }}
        {% endif %}
        {% if menu_attributes.item.style %}
          {{ item.attributes.setAttribute('style', menu_attributes.item.style) }}
        {% endif %}
      >


        {% if item.below %}
          {{ 
            link(
              item.title, 
              item.url, 
              item.attributes
                .removeClass(classes)
                .addClass(link_classes)
                .setAttribute('data-toggle', 'dropdown')
                .setAttribute('aria-expanded', 'false')
                .setAttribute('aria-haspopup', 'true')
                .setAttribute('type', 'button'), 
              menu_attributes.link) }}
          {# <span class=\"dropdown-item dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\" aria-haspopup=\"true\" type=\"button\"></span> #}
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% else %}
          {{ link(item.title, item.url, item.attributes.removeClass(classes).addClass(link_classes), menu_attributes.link) }}
        {% endif %}

      </li>

      {% else %}

      <li data-counter=\"menu-item-sub-0{{ loop.index }}\" {{ item.attributes.addClass(classes_sub) }}

        {% if menu_attributes.item.id %}
          {{ item.attributes.setAttribute('id', menu_attributes.item.id) }}
        {% endif %}
        {% if menu_attributes.item.style %}
          {{ item.attributes.setAttribute('style', menu_attributes.item.style) }}
        {% endif %}
      >
        {{ link(item.title, item.url, item.attributes.removeClass(classes_sub).addClass(link_classes_sub), menu_attributes.link) }}

        {% if item.below %}
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% endif %}

      </li>

      {% endif %}

    {% endfor %}

    </ul>
  {% endif %}
{% endmacro %}", "themes/custom/arky8/templates/menu/menu--main--region-header.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/menu/menu--main--region-header.html.twig");
    }
}
