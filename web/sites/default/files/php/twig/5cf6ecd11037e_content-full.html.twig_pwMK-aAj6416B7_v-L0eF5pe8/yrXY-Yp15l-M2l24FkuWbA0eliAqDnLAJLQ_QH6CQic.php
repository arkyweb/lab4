<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @arky8/-regions/content-full.html.twig */
class __TwigTemplate_2bd3e2c0967ac49be7ab025e01aa1baa330176ebbeef398ef5deee8c82f3a092 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 3];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo "<main id=\"mainpage\" class=\"mainpage\" data-spy=\"scroll\" data-target=\".menu--main\" data-offset=\"0\">
    ";
        // line 3
        if ($this->getAttribute(($context["page"] ?? null), "pban", [])) {
            // line 4
            echo "    <section class=\"mainpage-banner\">
        ";
            // line 5
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pban", [])), "html", null, true);
            echo "
    </section>
    ";
        }
        // line 8
        echo "
    ";
        // line 9
        if (($this->getAttribute(($context["page"] ?? null), "tabs", []) && ($context["logged_in"] ?? null))) {
            // line 10
            echo "    <section class=\"only-tabs\">
        ";
            // line 11
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "tabs", [])), "html", null, true);
            echo "
    </section>
    ";
        }
        // line 14
        echo "
    <section class=\"mainpage-content\"> 
        ";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo " 
    </section>

    ";
        // line 19
        if ($this->getAttribute(($context["page"] ?? null), "pc", [])) {
            // line 20
            echo "    <section class=\"mainpage-postcontent\">
        ";
            // line 21
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pc", [])), "html", null, true);
            echo "
    </section>
    ";
        }
        // line 24
        echo "
    ";
        // line 25
        if ($this->getAttribute(($context["page"] ?? null), "forms", [])) {
            // line 26
            echo "    <section id=\"labform\" class=\"mainpage-formcontent line-top\">
        <div class=\"contenedor\">
            ";
            // line 28
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "forms", [])), "html", null, true);
            echo "
        </div>
    </section>
    ";
        }
        // line 32
        echo "
    ";
        // line 33
        if ($this->getAttribute(($context["page"] ?? null), "pfoo", [])) {
            // line 34
            echo "    <section class=\"mainpage-prefooter\">
        ";
            // line 35
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pfoo", [])), "html", null, true);
            echo "
    </section>
    ";
        }
        // line 38
        echo "</main>";
    }

    public function getTemplateName()
    {
        return "@arky8/-regions/content-full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 38,  128 => 35,  125 => 34,  123 => 33,  120 => 32,  113 => 28,  109 => 26,  107 => 25,  104 => 24,  98 => 21,  95 => 20,  93 => 19,  87 => 16,  83 => 14,  77 => 11,  74 => 10,  72 => 9,  69 => 8,  63 => 5,  60 => 4,  58 => 3,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# MIANPAGE: Content + PostContent #}
<main id=\"mainpage\" class=\"mainpage\" data-spy=\"scroll\" data-target=\".menu--main\" data-offset=\"0\">
    {% if page.pban %}
    <section class=\"mainpage-banner\">
        {{ page.pban }}
    </section>
    {% endif %}

    {% if page.tabs and logged_in %}
    <section class=\"only-tabs\">
        {{ page.tabs }}
    </section>
    {% endif %}

    <section class=\"mainpage-content\"> 
        {{ page.content }} 
    </section>

    {% if page.pc %}
    <section class=\"mainpage-postcontent\">
        {{ page.pc }}
    </section>
    {% endif %}

    {% if page.forms %}
    <section id=\"labform\" class=\"mainpage-formcontent line-top\">
        <div class=\"contenedor\">
            {{ page.forms }}
        </div>
    </section>
    {% endif %}

    {% if page.pfoo %}
    <section class=\"mainpage-prefooter\">
        {{ page.pfoo }}
    </section>
    {% endif %}
</main>", "@arky8/-regions/content-full.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/-regions/content-full.html.twig");
    }
}
