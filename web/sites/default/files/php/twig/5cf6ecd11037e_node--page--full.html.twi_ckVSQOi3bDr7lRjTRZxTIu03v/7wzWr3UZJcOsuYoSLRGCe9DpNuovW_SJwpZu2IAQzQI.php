<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node--page--full.html.twig */
class __TwigTemplate_14ddb9b4e6e257bb2ba895bfe2fc8bd32fcc3a00b4dfdf8192c0b69bf23caea3 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 5];
        $filters = ["escape" => 1];
        $functions = ["file_url" => 6];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "aw"], "method")), "html", null, true);
        echo "> 
";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

";
        // line 5
        if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_image", []), "value", [])) {
            // line 6
            echo "<section class=\"awhite awbg\" style=\"background-image: url(";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_image", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo ")\"> 
";
        } else {
            // line 8
            echo "<section class=\"awith\">
";
        }
        // line 9
        echo " 

<div class=\"contenedor\">

    <article class=\"aw-only\">
 
        <div class=\"aw-only-text\">  

            <header class=\"aw-head\">
                <h1 id=\"title\" class=\"title\">";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
        echo "</h1>
            </header>

            ";
        // line 21
        if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_intro", []), "value", [])) {
            // line 22
            echo "            <article class=\"aw-intro\"> 
                ";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_intro", [])), "html", null, true);
            echo " 
            </article>
            ";
        }
        // line 25
        echo " 

            ";
        // line 27
        if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "body", []), "value", [])) {
            // line 28
            echo "            <article class=\"aw-body\"> 
                ";
            // line 29
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
            echo " 
            </article>
            ";
        }
        // line 31
        echo " 
     
        </div>

    </article>

</div>

</section>

";
        // line 41
        if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_body_secondary", []), "value", [])) {
            // line 42
            echo "<section class=\"awsec\">
<div class=\"contenedor\">

    <article class=\"body-secondary-field line-top\">

        ";
            // line 47
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_body_secondary_title", []), "value", [])) {
                // line 48
                echo "        <h4 class=\"block--minititle\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_body_secondary_title", []), "value", [])), "html", null, true);
                echo "</h4>
        ";
            }
            // line 49
            echo " 
 
        <div class=\"body-secondary aw-body\"> 
            ";
            // line 52
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_body_secondary", [])), "html", null, true);
            echo " 
        </div> 

    </article>

</div>
</section>
";
        }
        // line 59
        echo " 

</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node--page--full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 59,  158 => 52,  153 => 49,  147 => 48,  145 => 47,  138 => 42,  136 => 41,  124 => 31,  118 => 29,  115 => 28,  113 => 27,  109 => 25,  103 => 23,  100 => 22,  98 => 21,  92 => 18,  81 => 9,  77 => 8,  71 => 6,  69 => 5,  64 => 3,  60 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<div{{ attributes.addClass('aw')}}> 
{{ title_prefix }} 
{{ title_suffix }}

{% if node.field_image.value %}
<section class=\"awhite awbg\" style=\"background-image: url({{ file_url(node.field_image.entity.fileuri) }})\"> 
{% else %}
<section class=\"awith\">
{% endif %} 

<div class=\"contenedor\">

    <article class=\"aw-only\">
 
        <div class=\"aw-only-text\">  

            <header class=\"aw-head\">
                <h1 id=\"title\" class=\"title\">{{ node.label }}</h1>
            </header>

            {% if node.field_intro.value %}
            <article class=\"aw-intro\"> 
                {{ content.field_intro }} 
            </article>
            {% endif %} 

            {% if node.body.value %}
            <article class=\"aw-body\"> 
                {{ content.body }} 
            </article>
            {% endif %} 
     
        </div>

    </article>

</div>

</section>

{% if node.field_body_secondary.value %}
<section class=\"awsec\">
<div class=\"contenedor\">

    <article class=\"body-secondary-field line-top\">

        {% if node.field_body_secondary_title.value %}
        <h4 class=\"block--minititle\">{{ node.field_body_secondary_title.value }}</h4>
        {% endif %} 
 
        <div class=\"body-secondary aw-body\"> 
            {{ content.field_body_secondary }} 
        </div> 

    </article>

</div>
</section>
{% endif %} 

</div>", "themes/custom/arky8/templates/content/node--page--full.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/content/node--page--full.html.twig");
    }
}
