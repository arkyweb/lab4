<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/layout/html.html.twig */
class __TwigTemplate_2596146a82025aa89073d965d46497a476c583f0495e934eb9f3bb8c8025b106 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "if" => 31, "include" => 34];
        $filters = ["clean_class" => 4, "escape" => 20, "raw" => 23, "safe_join" => 24, "t" => 59];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'include'],
                ['clean_class', 'escape', 'raw', 'safe_join', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["body_classes"] = [0 => "site", 1 => (( !        // line 4
($context["root_path"] ?? null)) ? ("home") : (("inside-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["root_path"] ?? null)))))), 2 => ((        // line 5
($context["language"] ?? null)) ? (("lang-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["language"] ?? null))))) : ("")), 3 => (( !        // line 6
($context["logged_in"] ?? null)) ? ("off") : ("in")), 4 => (( !        // line 7
($context["node_type"] ?? null)) ? ("config") : (("node-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["node_type"] ?? null)))))), 5 => ((        // line 8
($context["db_offline"] ?? null)) ? ("offline") : ("")), 6 => ((        // line 9
($context["term_id"] ?? null)) ? (("tx-" . $this->sandbox->ensureToStringAllowed(($context["term_id"] ?? null)))) : ("")), 7 => (($this->getAttribute(        // line 10
($context["path_info"] ?? null), "args", [])) ? (("path-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["path_info"] ?? null), "args", [])))) : ("")), 8 => ((        // line 11
($context["current_path"] ?? null)) ? (("current-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["current_path"] ?? null))))) : ("")), 9 => ((( !$this->getAttribute(        // line 12
($context["page"] ?? null), "sidebar_first", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("cols-0") : ("")), 10 => ((($this->getAttribute(        // line 13
($context["page"] ?? null), "sidebar_first", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("cols-1") : ("")), 11 => ((($this->getAttribute(        // line 14
($context["page"] ?? null), "sidebar_second", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_first", []))) ? ("cols-2") : ("")), 12 => ((($this->getAttribute(        // line 15
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("cols-x") : (""))];
        // line 18
        echo "
<!DOCTYPE html>
<html ";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["html_attributes"] ?? null)), "html", null, true);
        echo ">

<head>
  <head-placeholder token=\"";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
  <title>";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->safeJoin($this->env, $this->sandbox->ensureToStringAllowed(($context["head_title"] ?? null)), " | "));
        echo "</title>
  <meta property=\"fb:app_id\" content=\"\"> 

</head>

<body id=\"site\" ";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["body_classes"] ?? null)], "method")), "html", null, true);
        echo ">

";
        // line 31
        if (($context["logged_in"] ?? null)) {
            // line 32
            echo "
  <style media=\"all\"> 
    ";
            // line 34
            $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = null;
            try {
                $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 =                 $this->loadTemplate((($context["directory"] ?? null) . "/templates/styles/arkyweb.css"), "themes/custom/arky8/templates/layout/html.html.twig", 34);
            } catch (LoaderError $e) {
                // ignore missing template
            }
            if ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) {
                $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4->display($context);
            }
            // line 35
            echo "  </style>  

";
        } else {
            // line 38
            echo "
  <style media=\"all\">
    ";
            // line 40
            $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = null;
            try {
                $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 =                 $this->loadTemplate((($context["directory"] ?? null) . "/templates/styles/loader.css"), "themes/custom/arky8/templates/layout/html.html.twig", 40);
            } catch (LoaderError $e) {
                // ignore missing template
            }
            if ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) {
                $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144->display($context);
            }
            // line 41
            echo "    ";
            $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = null;
            try {
                $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b =                 $this->loadTemplate((($context["directory"] ?? null) . "/templates/styles/arkyweb.css"), "themes/custom/arky8/templates/layout/html.html.twig", 41);
            } catch (LoaderError $e) {
                // ignore missing template
            }
            if ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) {
                $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b->display($context);
            }
            // line 42
            echo "  </style> 
  ";
            // line 43
            $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = null;
            try {
                $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 =                 $this->loadTemplate((($context["directory"] ?? null) . "/templates/@includes/load.html.twig"), "themes/custom/arky8/templates/layout/html.html.twig", 43);
            } catch (LoaderError $e) {
                // ignore missing template
            }
            if ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) {
                $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002->display($context);
            }
            // line 44
            echo "  <script>
    ";
            // line 45
            $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = null;
            try {
                $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 =                 $this->loadTemplate((($context["directory"] ?? null) . "/templates/scripts/load.js"), "themes/custom/arky8/templates/layout/html.html.twig", 45);
            } catch (LoaderError $e) {
                // ignore missing template
            }
            if ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) {
                $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4->display($context);
            }
            // line 46
            echo "  </script>

";
        }
        // line 49
        echo "
  ";
        // line 50
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_top"] ?? null)), "html", null, true);
        echo "
  ";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page"] ?? null)), "html", null, true);
        echo " 
  ";
        // line 52
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_bottom"] ?? null)), "html", null, true);
        echo "

";
        // line 54
        if (($context["logged_in"] ?? null)) {
            // line 55
            echo "
";
        } else {
            // line 57
            echo "
<a class=\"messenger\" href=\"http://m.me/Lab4Summit\" target=\"_blank\">
";
            // line 59
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Do you need some help?"));
            echo "
</a>

";
        }
        // line 63
        echo "
  <css-placeholder token=\"";
        // line 64
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">

  <script>
    ";
        // line 67
        $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = null;
        try {
            $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 =             $this->loadTemplate((($context["directory"] ?? null) . "/templates/scripts/font.js"), "themes/custom/arky8/templates/layout/html.html.twig", 67);
        } catch (LoaderError $e) {
            // ignore missing template
        }
        if ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) {
            $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666->display($context);
        }
        // line 68
        echo "    ";
        $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e = null;
        try {
            $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e =             $this->loadTemplate((($context["directory"] ?? null) . "/templates/scripts/lazy.js"), "themes/custom/arky8/templates/layout/html.html.twig", 68);
        } catch (LoaderError $e) {
            // ignore missing template
        }
        if ($__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e) {
            $__internal_01c35b74bd85735098add188b3f8372ba465b232ab8298cb582c60f493d3c22e->display($context);
        }
        // line 69
        echo "  </script> 

  <js-placeholder token=\"";
        // line 71
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">
  <js-bottom-placeholder token=\"";
        // line 72
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null)));
        echo "\">

  <script id=\"mcjs\">
    !function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,\"script\",\"https://chimpstatic.com/mcjs-connected/js/users/339c653b00489a96616fb7226/156f111cad52ff99dcad1fb39.js\");
  </script>

</body>

</html>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/layout/html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 72,  240 => 71,  236 => 69,  225 => 68,  215 => 67,  209 => 64,  206 => 63,  199 => 59,  195 => 57,  191 => 55,  189 => 54,  184 => 52,  180 => 51,  176 => 50,  173 => 49,  168 => 46,  158 => 45,  155 => 44,  145 => 43,  142 => 42,  131 => 41,  121 => 40,  117 => 38,  112 => 35,  102 => 34,  98 => 32,  96 => 31,  91 => 29,  83 => 24,  79 => 23,  73 => 20,  69 => 18,  67 => 15,  66 => 14,  65 => 13,  64 => 12,  63 => 11,  62 => 10,  61 => 9,  60 => 8,  59 => 7,  58 => 6,  57 => 5,  56 => 4,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{%
  set body_classes = [
    'site',
    not root_path ? 'home' : 'inside-' ~ root_path|clean_class,
    language ? 'lang-' ~ language|clean_class,
    not logged_in ? 'off' : 'in',
    not node_type ? 'config' : 'node-' ~ node_type|clean_class,
    db_offline ? 'offline',
    term_id ? 'tx-' ~ term_id, 
    path_info.args ? 'path-' ~ path_info.args,
    current_path ? 'current-' ~ current_path|clean_class,
    not page.sidebar_first and not page.sidebar_second ? 'cols-0',
    page.sidebar_first and not page.sidebar_second ? 'cols-1',
    page.sidebar_second and not page.sidebar_first ? 'cols-2',
    page.sidebar_first and page.sidebar_second ? 'cols-x'
  ]
%}

<!DOCTYPE html>
<html {{ html_attributes }}>

<head>
  <head-placeholder token=\"{{ placeholder_token|raw }}\">
  <title>{{ head_title|safe_join(' | ') }}</title>
  <meta property=\"fb:app_id\" content=\"\"> 

</head>

<body id=\"site\" {{ attributes.addClass(body_classes) }}>

{% if logged_in %}

  <style media=\"all\"> 
    {% include directory ~ '/templates/styles/arkyweb.css' ignore missing %}
  </style>  

{% else %}

  <style media=\"all\">
    {% include directory ~ '/templates/styles/loader.css' ignore missing %}
    {% include directory ~ '/templates/styles/arkyweb.css' ignore missing %}
  </style> 
  {% include directory ~ '/templates/@includes/load.html.twig' ignore missing %}
  <script>
    {% include directory ~ '/templates/scripts/load.js' ignore missing %}
  </script>

{% endif %}

  {{ page_top }}
  {{ page }} 
  {{ page_bottom }}

{% if logged_in %}

{% else %}

<a class=\"messenger\" href=\"http://m.me/Lab4Summit\" target=\"_blank\">
{{ 'Do you need some help?'|t }}
</a>

{% endif %}

  <css-placeholder token=\"{{ placeholder_token|raw }}\">

  <script>
    {% include directory ~ '/templates/scripts/font.js' ignore missing %}
    {% include directory ~ '/templates/scripts/lazy.js' ignore missing %}
  </script> 

  <js-placeholder token=\"{{ placeholder_token|raw }}\">
  <js-bottom-placeholder token=\"{{ placeholder_token|raw }}\">

  <script id=\"mcjs\">
    !function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,\"script\",\"https://chimpstatic.com/mcjs-connected/js/users/339c653b00489a96616fb7226/156f111cad52ff99dcad1fb39.js\");
  </script>

</body>

</html>", "themes/custom/arky8/templates/layout/html.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/layout/html.html.twig");
    }
}
