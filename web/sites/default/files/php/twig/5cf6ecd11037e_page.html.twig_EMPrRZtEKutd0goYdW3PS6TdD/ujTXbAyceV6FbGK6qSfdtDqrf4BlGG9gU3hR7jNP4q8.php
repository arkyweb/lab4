<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/layout/page.html.twig */
class __TwigTemplate_1c5cb6c97050ee884a02ee1c734edb51af0b5c3071fdcec84f049752fb93f250 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 6];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("arky8/styles"), "html", null, true);
        echo "
";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("arky8/styles_page"), "html", null, true);
        echo "

<section id=\"arkyweb\" class=\"arkyweb\">

\t";
        // line 6
        $this->loadTemplate("@arky8/-regions/header.html.twig", "themes/custom/arky8/templates/layout/page.html.twig", 6)->display($context);
        // line 7
        echo "\t";
        $this->loadTemplate("@arky8/-regions/content-full.html.twig", "themes/custom/arky8/templates/layout/page.html.twig", 7)->display($context);
        // line 8
        echo "    ";
        $this->loadTemplate("@arky8/-regions/footer.html.twig", "themes/custom/arky8/templates/layout/page.html.twig", 8)->display($context);
        // line 9
        echo "
</section> ";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 9,  71 => 8,  68 => 7,  66 => 6,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{{ attach_library('arky8/styles') }}
{{ attach_library('arky8/styles_page') }}

<section id=\"arkyweb\" class=\"arkyweb\">

\t{% include '@arky8/-regions/header.html.twig' %}
\t{% include '@arky8/-regions/content-full.html.twig' %}
    {% include '@arky8/-regions/footer.html.twig' %}

</section> {# Acaba SECTION #}", "themes/custom/arky8/templates/layout/page.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/layout/page.html.twig");
    }
}
