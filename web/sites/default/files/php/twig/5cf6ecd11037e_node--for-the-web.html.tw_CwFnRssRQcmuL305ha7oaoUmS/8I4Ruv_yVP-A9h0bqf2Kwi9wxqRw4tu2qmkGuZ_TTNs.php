<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node--for-the-web.html.twig */
class __TwigTemplate_e2aa040974da3ecc1ae08c8f82cbf23fccf9acb3aad2e94c53f6517e6621d8cd extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 5, "for" => 123];
        $filters = ["escape" => 7, "image_style" => 26, "length" => 47, "first" => 123];
        $functions = ["file_url" => 26];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape', 'image_style', 'length', 'first'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
";
        // line 5
        if ((($context["view_mode"] ?? null) == "teaser")) {
            echo " ";
            // line 6
            echo "
<article";
            // line 7
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-teaser"], "method")), "html", null, true);
            echo "> 

";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 10
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

    <h3>";
            // line 12
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["TITULO"] ?? null)), "html", null, true);
            echo "</h3>

</article>

";
        } elseif ((        // line 16
($context["view_mode"] ?? null) == "prefooter")) {
            echo " ";
            // line 17
            echo "
<div";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-prefooter"], "method")), "html", null, true);
            echo "> 

";
            // line 20
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 21
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<style>

.location--bg.bglazy {
  background-image: url('";
            // line 26
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_location_bg", []), "entity", []), "fileuri", [])), "thumbnail")]), "html", null, true);
            echo "');
}
.location--bg.bglazy.visible {
  background-image: url('";
            // line 29
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_location_bg", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "');
}

</style>

<section class=\"location\"> 
    <div class=\"contenedor location--contenedor\">
        <article class=\"location--block\">
            ";
            // line 37
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_location", []), "value", [])) {
                // line 38
                echo "                <div class=\"location--text\">
                    ";
                // line 39
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_ftw_location", [])), "html", null, true);
                echo " ";
                // line 40
                echo "                </div>
            ";
            }
            // line 41
            echo "  
            <figure class=\"location--map\">
                ";
            // line 43
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_ftw_location_iframe", [])), "html", null, true);
            echo "
            </figure> 
        </article>
    </div>
    ";
            // line 47
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_location_bg", []), 0, [], "array"), "value", [])) > 0)) {
                echo " ";
                echo " 
    <div class=\"location--bg bglazy\"></div>
    ";
            }
            // line 50
            echo "</section>

</div> 

";
        } elseif ((        // line 54
($context["view_mode"] ?? null) == "copyright")) {
            echo " ";
            // line 55
            echo "
<div";
            // line 56
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-copyright"], "method")), "html", null, true);
            echo "> 

";
            // line 58
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 59
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<section class=\"copyright\"> 
    <div class=\"copyright--contenedor\">
        
            ";
            // line 64
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright", []), "value", [])) {
                // line 65
                echo "                <div class=\"copyright--text\">
                    ";
                // line 66
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_ftw_foo_copyright", [])), "html", null, true);
                echo " ";
                // line 67
                echo "                </div>
            ";
            }
            // line 68
            echo " 
            ";
            // line 69
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright_img", []), 0, [], "array"), "value", [])) > 0)) {
                echo " ";
                // line 70
                echo "            <figure class=\"copyright--image\">
                <div>
                    <img 
                        class=\"lazy\" alt=\"Lab4\" title=\"Lab4\"
                        data-src=\"";
                // line 74
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright_img", []), 0, [], "array"), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" 
                        data-srcset=\"";
                // line 75
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright_img", []), 0, [], "array"), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" 
                        loading=\"lazy\" />
                    <noscript>
                      <img alt=\"Lab4\" title=\"Lab4\" src=\"";
                // line 78
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright_img", []), 0, [], "array"), "entity", []), "fileuri", []))]), "html", null, true);
                echo "}\">
                    </noscript>
                </div>
                <div>
                    <img 
                        class=\"lazy\" alt=\"The Pacific Alliance\" title=\"The Pacific Alliance\"
                        data-src=\"";
                // line 84
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright_img", []), 1, [], "array"), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" 
                        data-srcset=\"";
                // line 85
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright_img", []), 1, [], "array"), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" 
                        loading=\"lazy\" />
                    <noscript>
                      <img alt=\"Lab4\" title=\"Lab4\" src=\"";
                // line 88
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_copyright_img", []), 1, [], "array"), "entity", []), "fileuri", []))]), "html", null, true);
                echo "}\">
                    </noscript> 
                </div>
            </figure>
            ";
            }
            // line 93
            echo "    </div>
</section>

</div> 

";
        } elseif ((        // line 98
($context["view_mode"] ?? null) == "data")) {
            echo " ";
            // line 99
            echo "
<div";
            // line 100
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-data"], "method")), "html", null, true);
            echo "> 

";
            // line 102
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 103
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<section class=\"data\"> 
    <div class=\"data--contenedor\">
        
            ";
            // line 108
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_data_logo", []), 0, [], "array"), "value", [])) > 0)) {
                echo " ";
                // line 109
                echo "            <figure class=\"data--image\">
                <img 
                    class=\"lazy\" alt=\"\" title=\"\"
                    data-src=\"";
                // line 112
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_data_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" 
                    data-srcset=\"";
                // line 113
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_data_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" 
                    loading=\"lazy\" />
                <noscript>
                    <img alt=\"\" title=\"\" src=\"";
                // line 116
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_ftw_foo_data_logo", []), "entity", []), "fileuri", []))]), "html", null, true);
                echo "}\">
                </noscript> 
            </figure>
            ";
            }
            // line 120
            echo "
            ";
            // line 121
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_ftw_data_social_link", []), 0, [], "array")) {
                // line 122
                echo "                <div class=\"data-links\">
                    ";
                // line 123
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_ftw_data_social_link", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 124
                        echo "                    <div class=\"data-link data-link-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 126
                echo "                </div>
            ";
            }
            // line 128
            echo "
    </div>
</section>

</div> 

";
        } else {
            // line 134
            echo " ";
            // line 135
            echo "
<section";
            // line 136
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "aw"], "method")), "html", null, true);
            echo "> 
";
            // line 137
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 138
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<div class=\"contenedor\">

    <header class=\"aw-head\">
        <h1 id=\"title\" class=\"title\">";
            // line 143
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
            echo "</h1>
    </header>

    ";
            // line 146
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_intro", []), "value", [])) {
                // line 147
                echo "    <article class=\"aw-intro\"> 
        ";
                // line 148
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_intro", [])), "html", null, true);
                echo " 
    </article>
    ";
            }
            // line 150
            echo " 

    ";
            // line 152
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "body", []), "value", [])) {
                // line 153
                echo "    <article class=\"aw-body\"> 
        ";
                // line 154
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
                echo " 
    </article>
    ";
            }
            // line 156
            echo " 

</div>

</section> ";
            // line 161
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node--for-the-web.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  406 => 161,  400 => 156,  394 => 154,  391 => 153,  389 => 152,  385 => 150,  379 => 148,  376 => 147,  374 => 146,  368 => 143,  360 => 138,  356 => 137,  352 => 136,  349 => 135,  347 => 134,  338 => 128,  334 => 126,  319 => 124,  308 => 123,  305 => 122,  303 => 121,  300 => 120,  293 => 116,  287 => 113,  283 => 112,  278 => 109,  275 => 108,  267 => 103,  263 => 102,  258 => 100,  255 => 99,  252 => 98,  245 => 93,  237 => 88,  231 => 85,  227 => 84,  218 => 78,  212 => 75,  208 => 74,  202 => 70,  199 => 69,  196 => 68,  192 => 67,  189 => 66,  186 => 65,  184 => 64,  176 => 59,  172 => 58,  167 => 56,  164 => 55,  161 => 54,  155 => 50,  148 => 47,  141 => 43,  137 => 41,  133 => 40,  130 => 39,  127 => 38,  125 => 37,  114 => 29,  108 => 26,  100 => 21,  96 => 20,  91 => 18,  88 => 17,  85 => 16,  78 => 12,  73 => 10,  69 => 9,  64 => 7,  61 => 6,  58 => 5,  55 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# EMPIEZA EL NODO DE PAGINA                #}
{# ---------------------------------------- #}

{% if view_mode == 'teaser' %} {# Product full page (teaser). #}

<article{{ attributes.addClass('div-teaser') }}> 

{{ title_prefix }} 
{{ title_suffix }}

    <h3>{{ TITULO }}</h3>

</article>

{% elseif view_mode == 'prefooter' %} {# Product full page (prefooter). #}

<div{{ attributes.addClass('div-prefooter') }}> 

{{ title_prefix }} 
{{ title_suffix }}

<style>

.location--bg.bglazy {
  background-image: url('{{ file_url(node.field_ftw_location_bg.entity.fileuri | image_style('thumbnail')) }}');
}
.location--bg.bglazy.visible {
  background-image: url('{{ file_url(node.field_ftw_location_bg.entity.fileuri) }}');
}

</style>

<section class=\"location\"> 
    <div class=\"contenedor location--contenedor\">
        <article class=\"location--block\">
            {% if node.field_ftw_location.value %}
                <div class=\"location--text\">
                    {{ content.field_ftw_location }} {# Campo Location #}
                </div>
            {% endif %}  
            <figure class=\"location--map\">
                {{ content.field_ftw_location_iframe }}
            </figure> 
        </article>
    </div>
    {% if node.field_ftw_location_bg[0].value|length > 0 %} {# Campo Location Bg #} 
    <div class=\"location--bg bglazy\"></div>
    {% endif %}
</section>

</div> 

{% elseif view_mode == 'copyright' %} {# Product full page (copyright). #}

<div{{ attributes.addClass('div-copyright') }}> 

{{ title_prefix }} 
{{ title_suffix }}

<section class=\"copyright\"> 
    <div class=\"copyright--contenedor\">
        
            {% if node.field_ftw_foo_copyright.value %}
                <div class=\"copyright--text\">
                    {{ content.field_ftw_foo_copyright }} {# Campo Copyright #}
                </div>
            {% endif %} 
            {% if node.field_ftw_foo_copyright_img[0].value|length > 0 %} {# Campo Copyright Img #}
            <figure class=\"copyright--image\">
                <div>
                    <img 
                        class=\"lazy\" alt=\"Lab4\" title=\"Lab4\"
                        data-src=\"{{ file_url(node.field_ftw_foo_copyright_img[0].entity.fileuri) }}\" 
                        data-srcset=\"{{ file_url(node.field_ftw_foo_copyright_img[0].entity.fileuri) }}\" 
                        loading=\"lazy\" />
                    <noscript>
                      <img alt=\"Lab4\" title=\"Lab4\" src=\"{{ file_url(node.field_ftw_foo_copyright_img[0].entity.fileuri) }}}\">
                    </noscript>
                </div>
                <div>
                    <img 
                        class=\"lazy\" alt=\"The Pacific Alliance\" title=\"The Pacific Alliance\"
                        data-src=\"{{ file_url(node.field_ftw_foo_copyright_img[1].entity.fileuri) }}\" 
                        data-srcset=\"{{ file_url(node.field_ftw_foo_copyright_img[1].entity.fileuri) }}\" 
                        loading=\"lazy\" />
                    <noscript>
                      <img alt=\"Lab4\" title=\"Lab4\" src=\"{{ file_url(node.field_ftw_foo_copyright_img[1].entity.fileuri) }}}\">
                    </noscript> 
                </div>
            </figure>
            {% endif %}
    </div>
</section>

</div> 

{% elseif view_mode == 'data' %} {# Product full page (data). #}

<div{{ attributes.addClass('div-data') }}> 

{{ title_prefix }} 
{{ title_suffix }}

<section class=\"data\"> 
    <div class=\"data--contenedor\">
        
            {% if node.field_ftw_foo_data_logo[0].value|length > 0 %} {# Campo Data Logo Img #}
            <figure class=\"data--image\">
                <img 
                    class=\"lazy\" alt=\"\" title=\"\"
                    data-src=\"{{ file_url(node.field_ftw_foo_data_logo.entity.fileuri) }}\" 
                    data-srcset=\"{{ file_url(node.field_ftw_foo_data_logo.entity.fileuri) }}\" 
                    loading=\"lazy\" />
                <noscript>
                    <img alt=\"\" title=\"\" src=\"{{ file_url(node.field_ftw_foo_data_logo.entity.fileuri) }}}\">
                </noscript> 
            </figure>
            {% endif %}

            {% if content.field_ftw_data_social_link[0] %}
                <div class=\"data-links\">
                    {% for key, item in content.field_ftw_data_social_link if key|first != '#' %}
                    <div class=\"data-link data-link-item-0{{ loop.index }}\">{{ item }}</div>
                    {% endfor %}
                </div>
            {% endif %}

    </div>
</section>

</div> 

{% else %} {# Product full page (default). #}

<section{{ attributes.addClass('aw') }}> 
{{ title_prefix }} 
{{ title_suffix }}

<div class=\"contenedor\">

    <header class=\"aw-head\">
        <h1 id=\"title\" class=\"title\">{{ node.label }}</h1>
    </header>

    {% if node.field_intro.value %}
    <article class=\"aw-intro\"> 
        {{ content.field_intro }} 
    </article>
    {% endif %} 

    {% if node.body.value %}
    <article class=\"aw-body\"> 
        {{ content.body }} 
    </article>
    {% endif %} 

</div>

</section> {# CULMINA EL DIV DEL NODE #}

{% endif %}", "themes/custom/arky8/templates/content/node--for-the-web.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/content/node--for-the-web.html.twig");
    }
}
