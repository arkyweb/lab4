<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @arky8/-regions/footer.html.twig */
class __TwigTemplate_c963eaf0aa02c1821a7d85b209689fa183c90d4b2bf2dd2ec1558304fe94be4c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (($this->getAttribute(($context["page"] ?? null), "foo_top", []) || $this->getAttribute(($context["page"] ?? null), "foo_bottom", []))) {
            // line 2
            echo "<footer class=\"footerpage\">
    <div class=\"contenedor footerpage-contenedor\"> 
        <div class=\"foo--top\">
            ";
            // line 5
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "foo_top", [])), "html", null, true);
            echo "
        </div>
        <div class=\"foo--bottom\">
            ";
            // line 8
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "foo_bottom", [])), "html", null, true);
            echo "
        </div>
    </div> 
</footer>
";
        }
    }

    public function getTemplateName()
    {
        return "@arky8/-regions/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 8,  62 => 5,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if page.foo_top or page.foo_bottom %}
<footer class=\"footerpage\">
    <div class=\"contenedor footerpage-contenedor\"> 
        <div class=\"foo--top\">
            {{ page.foo_top }}
        </div>
        <div class=\"foo--bottom\">
            {{ page.foo_bottom }}
        </div>
    </div> 
</footer>
{% endif %}", "@arky8/-regions/footer.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/-regions/footer.html.twig");
    }
}
