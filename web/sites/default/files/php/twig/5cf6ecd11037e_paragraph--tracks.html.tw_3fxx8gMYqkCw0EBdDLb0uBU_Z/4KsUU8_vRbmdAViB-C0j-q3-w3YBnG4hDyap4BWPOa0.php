<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/paragraph/paragraph--tracks.html.twig */
class __TwigTemplate_46bb070df4f252d95fe0c2726c4faf262bcc4b8cfc36dadad07f22819320576a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 6];
        $filters = ["escape" => 1];
        $functions = ["file_url" => 10];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<article";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "tracks"], "method")), "html", null, true);
        echo ">

";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo " 
";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
    
    ";
        // line 6
        if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_tk_icono", []))) {
            echo " 
    <figure class=\"tracks--icon\"> 
        <img 
            class=\"lazy\" alt=\"";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_txt", []), "value", [])), "html", null, true);
            echo "\" title=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_txt", []), "value", [])), "html", null, true);
            echo "\"
            data-src=\"";
            // line 10
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_icono", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "\" 
            data-srcset=\"";
            // line 11
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_icono", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "\" 
            loading=\"lazy\" />
        <noscript>
          <img alt=\"";
            // line 14
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_txt", []), "value", [])), "html", null, true);
            echo "\" title=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_txt", []), "value", [])), "html", null, true);
            echo "\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_icono", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "}\">
        </noscript>
    </figure>
    ";
        }
        // line 17
        echo " 



    ";
        // line 21
        if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_tk_txt", []))) {
            echo " 
    <p class=\"tracks--text\">
        ";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_tk_txt", []), "value", [])), "html", null, true);
            echo "
    </p>
    ";
        }
        // line 26
        echo "
</article>";
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/paragraph/paragraph--tracks.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 26,  114 => 23,  109 => 21,  103 => 17,  92 => 14,  86 => 11,  82 => 10,  76 => 9,  70 => 6,  65 => 4,  61 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<article{{ attributes.addClass('tracks') }}>

{{ title_prefix }} 
{{ title_suffix }}
    
    {% if paragraph.field_tk_icono is not empty %} 
    <figure class=\"tracks--icon\"> 
        <img 
            class=\"lazy\" alt=\"{{ paragraph.field_tk_txt.value }}\" title=\"{{ paragraph.field_tk_txt.value }}\"
            data-src=\"{{ file_url(paragraph.field_tk_icono.entity.fileuri) }}\" 
            data-srcset=\"{{ file_url(paragraph.field_tk_icono.entity.fileuri) }}\" 
            loading=\"lazy\" />
        <noscript>
          <img alt=\"{{ paragraph.field_tk_txt.value }}\" title=\"{{ paragraph.field_tk_txt.value }}\" src=\"{{ file_url(paragraph.field_tk_icono.entity.fileuri) }}}\">
        </noscript>
    </figure>
    {% endif %} 



    {% if paragraph.field_tk_txt is not empty %} 
    <p class=\"tracks--text\">
        {{ paragraph.field_tk_txt.value }}
    </p>
    {% endif %}

</article>", "themes/custom/arky8/templates/paragraph/paragraph--tracks.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/paragraph/paragraph--tracks.html.twig");
    }
}
