<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node--experiences.html.twig */
class __TwigTemplate_2ae241fe7bfd2b5de6f893088a794863bd64ca8171e31c59282a5550b0c055ce extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 5];
        $filters = ["escape" => 7, "image_style" => 15, "t" => 25];
        $functions = ["file_url" => 15];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape', 'image_style', 't'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
";
        // line 5
        if ((($context["view_mode"] ?? null) == "teaser")) {
            echo " ";
            // line 6
            echo "
<article";
            // line 7
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-teaser"], "method")), "html", null, true);
            echo "> 

";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 10
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<style>

/*.bgs-";
            // line 14
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo " {
  background-image: url('";
            // line 15
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", [])), "thumbnail")]), "html", null, true);
            echo "');
}*/
.bgs-";
            // line 17
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo ".visible {
  background-image: url('";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "');
}

</style>

    <a href=\"#\" data-toggle=\"modal\" data-target=\"#";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "\" class=\"bglazy experiences bgs-";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "\">
        <h3>";
            // line 24
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
            echo "</h3>
        <span>";
            // line 25
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("See more"));
            echo "</span>
    </a>

</article>

";
        } elseif ((        // line 30
($context["view_mode"] ?? null) == "modal")) {
            echo " ";
            // line 31
            echo "
<article";
            // line 32
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-modal modal fade"], "method")), "html", null, true);
            echo " id=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "-modal\" aria-hidden=\"true\"> 

";
            // line 34
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 35
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<style>

/*.mheader-";
            // line 39
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo " {
  background-image: url('";
            // line 40
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", [])), "thumbnail")]), "html", null, true);
            echo "');
}*/
.mheader-";
            // line 42
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo ".visible {
  background-image: url('";
            // line 43
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "');
}

</style>

  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header mheader-";
            // line 50
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo " bglazy\">
        <h5 class=\"modal-title\">";
            // line 51
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
            echo "</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
        ";
            // line 57
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
            echo "
      </div>
    </div>
  </div>

</article>

";
        } else {
            // line 64
            echo " ";
            // line 65
            echo "
<section";
            // line 66
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "aw"], "method")), "html", null, true);
            echo "> 
";
            // line 67
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 68
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<div class=\"contenedor\">

    <header class=\"aw-head\">
        <h1 id=\"title\" class=\"title\">";
            // line 73
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
            echo "</h1>
    </header>

    ";
            // line 76
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_intro", []), "value", [])) {
                // line 77
                echo "    <article class=\"aw-intro\"> 
        ";
                // line 78
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_intro", [])), "html", null, true);
                echo " 
    </article>
    ";
            }
            // line 80
            echo " 

    ";
            // line 82
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "body", []), "value", [])) {
                // line 83
                echo "    <article class=\"aw-body\"> 
        ";
                // line 84
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
                echo " 
    </article>
    ";
            }
            // line 86
            echo " 

</div>

</section> ";
            // line 91
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node--experiences.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 91,  245 => 86,  239 => 84,  236 => 83,  234 => 82,  230 => 80,  224 => 78,  221 => 77,  219 => 76,  213 => 73,  205 => 68,  201 => 67,  197 => 66,  194 => 65,  192 => 64,  181 => 57,  172 => 51,  168 => 50,  158 => 43,  154 => 42,  149 => 40,  145 => 39,  138 => 35,  134 => 34,  125 => 32,  122 => 31,  119 => 30,  111 => 25,  107 => 24,  101 => 23,  93 => 18,  89 => 17,  84 => 15,  80 => 14,  73 => 10,  69 => 9,  64 => 7,  61 => 6,  58 => 5,  55 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# EMPIEZA EL NODO DE PAGINA                #}
{# ---------------------------------------- #}

{% if view_mode == 'teaser' %} {# Product full page (teaser). #}

<article{{ attributes.addClass('div-teaser') }}> 

{{ title_prefix }} 
{{ title_suffix }}

<style>

/*.bgs-{{ node.field_id.value }} {
  background-image: url('{{ file_url(node.field_background.entity.fileuri | image_style('thumbnail')) }}');
}*/
.bgs-{{ node.field_id.value }}.visible {
  background-image: url('{{ file_url(node.field_background.entity.fileuri) }}');
}

</style>

    <a href=\"#\" data-toggle=\"modal\" data-target=\"#{{ node.field_id.value }}\" class=\"bglazy experiences bgs-{{ node.field_id.value }}\">
        <h3>{{ node.label }}</h3>
        <span>{{ 'See more'|t }}</span>
    </a>

</article>

{% elseif view_mode == 'modal' %} {# Product full page (joining). #}

<article{{ attributes.addClass('div-modal modal fade') }} id=\"{{ node.field_id.value }}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"{{  node.field_id.value }}-modal\" aria-hidden=\"true\"> 

{{ title_prefix }} 
{{ title_suffix }}

<style>

/*.mheader-{{ node.field_id.value }} {
  background-image: url('{{ file_url(node.field_background.entity.fileuri | image_style('thumbnail')) }}');
}*/
.mheader-{{ node.field_id.value }}.visible {
  background-image: url('{{ file_url(node.field_background.entity.fileuri) }}');
}

</style>

  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header mheader-{{ node.field_id.value }} bglazy\">
        <h5 class=\"modal-title\">{{ node.label }}</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
        {{ content.body }}
      </div>
    </div>
  </div>

</article>

{% else %} {# Product full page (default). #}

<section{{ attributes.addClass('aw') }}> 
{{ title_prefix }} 
{{ title_suffix }}

<div class=\"contenedor\">

    <header class=\"aw-head\">
        <h1 id=\"title\" class=\"title\">{{ node.label }}</h1>
    </header>

    {% if node.field_intro.value %}
    <article class=\"aw-intro\"> 
        {{ content.field_intro }} 
    </article>
    {% endif %} 

    {% if node.body.value %}
    <article class=\"aw-body\"> 
        {{ content.body }} 
    </article>
    {% endif %} 

</div>

</section> {# CULMINA EL DIV DEL NODE #}

{% endif %}", "themes/custom/arky8/templates/content/node--experiences.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/content/node--experiences.html.twig");
    }
}
