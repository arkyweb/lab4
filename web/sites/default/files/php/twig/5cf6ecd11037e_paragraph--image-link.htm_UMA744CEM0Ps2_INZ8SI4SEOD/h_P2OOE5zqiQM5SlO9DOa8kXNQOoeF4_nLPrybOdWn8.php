<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/paragraph/paragraph--image-link.html.twig */
class __TwigTemplate_a18ec28eaa5914e7c152d2a8726f26323a7bae5c15b998c68b219f0870963ce1 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1];
        $filters = ["escape" => 3];
        $functions = ["file_url" => 14];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if ((($context["view_mode"] ?? null) == "teaser")) {
            echo " 

<article";
            // line 3
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "logos"], "method")), "html", null, true);
            echo ">

";
            // line 5
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 6
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "
    
    ";
            // line 8
            if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_il_img", []))) {
                echo " 
    <a 
        href=\"";
                // line 10
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_link", []), 0, []), "url", [])), "html", null, true);
                echo "\" 
        target=\"";
                // line 11
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_link", []), 0, []), "url", []), "external", [])) ? ("_blank") : ("_self")));
                echo "\" 
        rel=\"";
                // line 12
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_link", []), 0, []), "url", []), "external", [])) ? ("noopener") : ("")));
                echo "\" 
        class=\"logos--link\" alt=\"";
                // line 13
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\" title=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\">
        <img src=\"";
                // line 14
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_img", []), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" alt=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\" title=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\" />
    </a>
    ";
            }
            // line 16
            echo " 

</article>

";
        } else {
            // line 21
            echo "
<article";
            // line 22
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "data-link"], "method")), "html", null, true);
            echo ">

";
            // line 24
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 25
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "
    
    ";
            // line 27
            if ( !twig_test_empty($this->getAttribute(($context["paragraph"] ?? null), "field_il_img", []))) {
                echo " 
    <a 
        href=\"";
                // line 29
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_link", []), 0, []), "url", [])), "html", null, true);
                echo "\" 
        target=\"";
                // line 30
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_link", []), 0, []), "url", []), "external", [])) ? ("_blank") : ("_self")));
                echo "\" 
        rel=\"";
                // line 31
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar((($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_link", []), 0, []), "url", []), "external", [])) ? ("noopener") : ("")));
                echo "\" 
        class=\"dlink\" alt=\"";
                // line 32
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\" title=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\">
        <img src=\"";
                // line 33
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_img", []), "entity", []), "fileuri", []))]), "html", null, true);
                echo "\" alt=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\" title=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["paragraph"] ?? null), "field_il_title", []), "value", [])), "html", null, true);
                echo "\" />
    </a>
    ";
            }
            // line 35
            echo " 

</article>

";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/paragraph/paragraph--image-link.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 35,  154 => 33,  148 => 32,  144 => 31,  140 => 30,  136 => 29,  131 => 27,  126 => 25,  122 => 24,  117 => 22,  114 => 21,  107 => 16,  97 => 14,  91 => 13,  87 => 12,  83 => 11,  79 => 10,  74 => 8,  69 => 6,  65 => 5,  60 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if view_mode == 'teaser' %} 

<article{{ attributes.addClass('logos') }}>

{{ title_prefix }} 
{{ title_suffix }}
    
    {% if paragraph.field_il_img is not empty %} 
    <a 
        href=\"{{ paragraph.field_il_link.0.url }}\" 
        target=\"{{ paragraph.field_il_link.0.url.external ? '_blank' : '_self' }}\" 
        rel=\"{{ paragraph.field_il_link.0.url.external ? 'noopener' : '' }}\" 
        class=\"logos--link\" alt=\"{{ paragraph.field_il_title.value }}\" title=\"{{ paragraph.field_il_title.value }}\">
        <img src=\"{{ file_url(paragraph.field_il_img.entity.fileuri) }}\" alt=\"{{ paragraph.field_il_title.value }}\" title=\"{{ paragraph.field_il_title.value }}\" />
    </a>
    {% endif %} 

</article>

{% else %}

<article{{ attributes.addClass('data-link') }}>

{{ title_prefix }} 
{{ title_suffix }}
    
    {% if paragraph.field_il_img is not empty %} 
    <a 
        href=\"{{ paragraph.field_il_link.0.url }}\" 
        target=\"{{ paragraph.field_il_link.0.url.external ? '_blank' : '_self' }}\" 
        rel=\"{{ paragraph.field_il_link.0.url.external ? 'noopener' : '' }}\" 
        class=\"dlink\" alt=\"{{ paragraph.field_il_title.value }}\" title=\"{{ paragraph.field_il_title.value }}\">
        <img src=\"{{ file_url(paragraph.field_il_img.entity.fileuri) }}\" alt=\"{{ paragraph.field_il_title.value }}\" title=\"{{ paragraph.field_il_title.value }}\" />
    </a>
    {% endif %} 

</article>

{% endif %}", "themes/custom/arky8/templates/paragraph/paragraph--image-link.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/paragraph/paragraph--image-link.html.twig");
    }
}
