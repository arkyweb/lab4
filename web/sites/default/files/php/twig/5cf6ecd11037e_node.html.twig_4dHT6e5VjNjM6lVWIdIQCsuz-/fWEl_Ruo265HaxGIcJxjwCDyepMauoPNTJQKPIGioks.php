<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/arky8/templates/content/node.html.twig */
class __TwigTemplate_66d93c43ea37d52d826b6059901037035434ebf364e089315be4e5dacea64e92 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 5, "for" => 41, "include" => 96];
        $filters = ["escape" => 7, "image_style" => 15, "t" => 25, "first" => 41, "length" => 88];
        $functions = ["file_url" => 15];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for', 'include'],
                ['escape', 'image_style', 't', 'first', 'length'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 4
        echo "
";
        // line 5
        if ((($context["view_mode"] ?? null) == "teaser")) {
            echo " ";
            // line 6
            echo "
<article";
            // line 7
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-teaser"], "method")), "html", null, true);
            echo "> 

";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 10
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<style>

/*.bgs-";
            // line 14
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo " {
  background-image: url('";
            // line 15
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", [])), "thumbnail")]), "html", null, true);
            echo "');
}*/
.bgs-";
            // line 17
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo ".visible {
  background-image: url('";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "');
}

</style>

    <a href=\"#\" data-toggle=\"modal\" data-target=\"#";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "\" class=\"experiences bglazy bgs-";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "\">
        <h3>";
            // line 24
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
            echo "</h3>
        <span>";
            // line 25
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("See more"));
            echo "</span>
    </a>

</article>

";
        } elseif ((        // line 30
($context["view_mode"] ?? null) == "paragraph")) {
            echo " ";
            // line 31
            echo "
    ";
            // line 32
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []), 0, [], "array")) {
                // line 33
                echo "
    <article";
                // line 34
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-paragraph line-top"], "method")), "html", null, true);
                echo "> 

    <div class=\"contenedor\">

        <h4 class=\"block--minititle\">";
                // line 38
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_paragraph_title", []), "value", [])), "html", null, true);
                echo "</h4>

            <div class=\"paragraph-container\">
                ";
                // line 41
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_paragraphs", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 42
                        echo "                <div class=\"paragraph-item paragraph-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
                ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 44
                echo "            </div>

    </div>

    </article>

    ";
            }
            // line 50
            echo " 

";
        } elseif ((        // line 52
($context["view_mode"] ?? null) == "modal")) {
            echo " ";
            // line 53
            echo "
<article";
            // line 54
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-modal modal fade"], "method")), "html", null, true);
            echo " id=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo "-modal\" aria-hidden=\"true\"> 

";
            // line 56
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 57
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<style>

/*.mheader-";
            // line 61
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo " {
  background-image: url('";
            // line 62
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", [])), "thumbnail")]), "html", null, true);
            echo "');
}*/
.mheader-";
            // line 64
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo ".visible {
  background-image: url('";
            // line 65
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_background", []), "entity", []), "fileuri", []))]), "html", null, true);
            echo "');
}

</style>

  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header mheader-";
            // line 72
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_id", []), "value", [])), "html", null, true);
            echo " bglazy\">
        <h5 class=\"modal-title\">";
            // line 73
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
            echo "</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button> 
      </div>
      <div class=\"modal-body\">
        ";
            // line 79
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_intro", [])), "html", null, true);
            echo "
      </div>
    </div>
  </div>

</article>

";
        } elseif ((        // line 86
($context["view_mode"] ?? null) == "colorbox")) {
            echo " ";
            // line 87
            echo "
    ";
            // line 88
            if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_colorbox", []), "value", [])) > 0)) {
                // line 89
                echo "
        <article";
                // line 90
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-colorbox line-top"], "method")), "html", null, true);
                echo "> 

        <div class=\"contenedor\">

            <h4 class=\"block--minititle\">";
                // line 94
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_colorbox_title", []), "value", [])), "html", null, true);
                echo "</h4>
            
            ";
                // line 96
                $this->loadTemplate((($context["directory"] ?? null) . "/templates/@includes/photoswipe.html.twig"), "themes/custom/arky8/templates/content/node.html.twig", 96)->display($context);
                echo " ";
                // line 97
                echo "
        </div>

        </article>
    ";
            }
            // line 102
            echo "
";
        } elseif ((        // line 103
($context["view_mode"] ?? null) == "logoslink")) {
            echo " ";
            // line 104
            echo "
    ";
            // line 105
            if ($this->getAttribute($this->getAttribute(($context["content"] ?? null), "field_logos_link", []), 0, [], "array")) {
                // line 106
                echo "
    <article";
                // line 107
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "div-logoslink line-top"], "method")), "html", null, true);
                echo "> 

    <div class=\"contenedor\">

        <h4 class=\"block--minititle\">";
                // line 111
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_logos_", []), "value", [])), "html", null, true);
                echo "</h4>

            <div class=\"logoslink-container\">
                ";
                // line 114
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["content"] ?? null), "field_logos_link", []));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                    if ((twig_first($this->env, $context["key"]) != "#")) {
                        // line 115
                        echo "                <div class=\"logoslink-item logoslink-item-0";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                        echo "\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["item"]), "html", null, true);
                        echo "</div>
                ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 117
                echo "            </div>

    </div>

    </article>

    ";
            }
            // line 123
            echo " 

";
        } else {
            // line 125
            echo " ";
            // line 126
            echo "
<section";
            // line 127
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "aw"], "method")), "html", null, true);
            echo "> 
";
            // line 128
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo " 
";
            // line 129
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "

<div class=\"contenedor\">

    <article class=\"aw-only\">
 
    <div class=\"aw-only-text\">  

        <header class=\"aw-head\">
            <h1 id=\"title\" class=\"title\">";
            // line 138
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["node"] ?? null), "label", [])), "html", null, true);
            echo "</h1>
        </header>

        ";
            // line 141
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_intro", []), "value", [])) {
                // line 142
                echo "        <article class=\"aw-intro\"> 
            ";
                // line 143
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "field_intro", [])), "html", null, true);
                echo " 
        </article>
        ";
            }
            // line 145
            echo " 

        ";
            // line 147
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "body", []), "value", [])) {
                // line 148
                echo "        <article class=\"aw-body\"> 
            ";
                // line 149
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "body", [])), "html", null, true);
                echo " 
        </article>
        ";
            }
            // line 151
            echo " 
 
    </div>

</div>

</section> ";
            // line 158
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/arky8/templates/content/node.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  419 => 158,  411 => 151,  405 => 149,  402 => 148,  400 => 147,  396 => 145,  390 => 143,  387 => 142,  385 => 141,  379 => 138,  367 => 129,  363 => 128,  359 => 127,  356 => 126,  354 => 125,  349 => 123,  340 => 117,  325 => 115,  314 => 114,  308 => 111,  301 => 107,  298 => 106,  296 => 105,  293 => 104,  290 => 103,  287 => 102,  280 => 97,  277 => 96,  272 => 94,  265 => 90,  262 => 89,  260 => 88,  257 => 87,  254 => 86,  244 => 79,  235 => 73,  231 => 72,  221 => 65,  217 => 64,  212 => 62,  208 => 61,  201 => 57,  197 => 56,  188 => 54,  185 => 53,  182 => 52,  178 => 50,  169 => 44,  154 => 42,  143 => 41,  137 => 38,  130 => 34,  127 => 33,  125 => 32,  122 => 31,  119 => 30,  111 => 25,  107 => 24,  101 => 23,  93 => 18,  89 => 17,  84 => 15,  80 => 14,  73 => 10,  69 => 9,  64 => 7,  61 => 6,  58 => 5,  55 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ---------------------------------------- #}
{# EMPIEZA EL NODO DE PAGINA                #}
{# ---------------------------------------- #}

{% if view_mode == 'teaser' %} {# Product full page (teaser). #}

<article{{ attributes.addClass('div-teaser') }}> 

{{ title_prefix }} 
{{ title_suffix }}

<style>

/*.bgs-{{ node.field_id.value }} {
  background-image: url('{{ file_url(node.field_background.entity.fileuri | image_style('thumbnail')) }}');
}*/
.bgs-{{ node.field_id.value }}.visible {
  background-image: url('{{ file_url(node.field_background.entity.fileuri) }}');
}

</style>

    <a href=\"#\" data-toggle=\"modal\" data-target=\"#{{ node.field_id.value }}\" class=\"experiences bglazy bgs-{{ node.field_id.value }}\">
        <h3>{{ node.label }}</h3>
        <span>{{ 'See more'|t }}</span>
    </a>

</article>

{% elseif view_mode == 'paragraph' %} {# Product full page (paragraph). #}

    {% if content.field_paragraphs[0] %}

    <article{{ attributes.addClass('div-paragraph line-top') }}> 

    <div class=\"contenedor\">

        <h4 class=\"block--minititle\">{{ node.field_paragraph_title.value }}</h4>

            <div class=\"paragraph-container\">
                {% for key, item in content.field_paragraphs if key|first != '#' %}
                <div class=\"paragraph-item paragraph-item-0{{ loop.index }}\">{{ item }}</div>
                {% endfor %}
            </div>

    </div>

    </article>

    {% endif %} 

{% elseif view_mode == 'modal' %} {# Product full page (joining). #}

<article{{ attributes.addClass('div-modal modal fade') }} id=\"{{ node.field_id.value }}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"{{ node.field_id.value }}-modal\" aria-hidden=\"true\"> 

{{ title_prefix }} 
{{ title_suffix }}

<style>

/*.mheader-{{ node.field_id.value }} {
  background-image: url('{{ file_url(node.field_background.entity.fileuri | image_style('thumbnail')) }}');
}*/
.mheader-{{ node.field_id.value }}.visible {
  background-image: url('{{ file_url(node.field_background.entity.fileuri) }}');
}

</style>

  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header mheader-{{ node.field_id.value }} bglazy\">
        <h5 class=\"modal-title\">{{ node.label }}</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button> 
      </div>
      <div class=\"modal-body\">
        {{ content.field_intro }}
      </div>
    </div>
  </div>

</article>

{% elseif view_mode == 'colorbox' %} {# Product full page (colorbox). #}

    {% if node.field_colorbox.value|length > 0 %}

        <article{{ attributes.addClass('div-colorbox line-top') }}> 

        <div class=\"contenedor\">

            <h4 class=\"block--minititle\">{{ node.field_colorbox_title.value }}</h4>
            
            {% include directory ~ '/templates/@includes/photoswipe.html.twig' %} {# includes photoswiper #}

        </div>

        </article>
    {% endif %}

{% elseif view_mode == 'logoslink' %} {# Product full page (logoslink). #}

    {% if content.field_logos_link[0] %}

    <article{{ attributes.addClass('div-logoslink line-top') }}> 

    <div class=\"contenedor\">

        <h4 class=\"block--minititle\">{{ node.field_logos_.value }}</h4>

            <div class=\"logoslink-container\">
                {% for key, item in content.field_logos_link if key|first != '#' %}
                <div class=\"logoslink-item logoslink-item-0{{ loop.index }}\">{{ item }}</div>
                {% endfor %}
            </div>

    </div>

    </article>

    {% endif %} 

{% else %} {# Product full page (default). #}

<section{{ attributes.addClass('aw') }}> 
{{ title_prefix }} 
{{ title_suffix }}

<div class=\"contenedor\">

    <article class=\"aw-only\">
 
    <div class=\"aw-only-text\">  

        <header class=\"aw-head\">
            <h1 id=\"title\" class=\"title\">{{ node.label }}</h1>
        </header>

        {% if node.field_intro.value %}
        <article class=\"aw-intro\"> 
            {{ content.field_intro }} 
        </article>
        {% endif %} 

        {% if node.body.value %}
        <article class=\"aw-body\"> 
            {{ content.body }} 
        </article>
        {% endif %} 
 
    </div>

</div>

</section> {# CULMINA EL DIV DEL NODE #}

{% endif %}", "themes/custom/arky8/templates/content/node.html.twig", "/home4/arkyweb/paginas/pro/lab4/web/themes/custom/arky8/templates/content/node.html.twig");
    }
}
