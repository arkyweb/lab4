var initPhotoSwipeFromDOM = function(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements 
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes 
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML; 
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            } 

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) { 
                continue; 
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');  
            if(pair.length < 2) {
                continue;
            }           
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect(); 

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used 
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};

// execute above function
initPhotoSwipeFromDOM('.photoswipe-gallery');;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings, storage) {
  var currentUserID = parseInt(drupalSettings.user.uid, 10);

  var secondsIn30Days = 2592000;
  var thirtyDaysAgo = Math.round(new Date().getTime() / 1000) - secondsIn30Days;

  var embeddedLastReadTimestamps = false;
  if (drupalSettings.history && drupalSettings.history.lastReadTimestamps) {
    embeddedLastReadTimestamps = drupalSettings.history.lastReadTimestamps;
  }

  Drupal.history = {
    fetchTimestamps: function fetchTimestamps(nodeIDs, callback) {
      if (embeddedLastReadTimestamps) {
        callback();
        return;
      }

      $.ajax({
        url: Drupal.url('history/get_node_read_timestamps'),
        type: 'POST',
        data: { 'node_ids[]': nodeIDs },
        dataType: 'json',
        success: function success(results) {
          Object.keys(results || {}).forEach(function (nodeID) {
            storage.setItem('Drupal.history.' + currentUserID + '.' + nodeID, results[nodeID]);
          });
          callback();
        }
      });
    },
    getLastRead: function getLastRead(nodeID) {
      if (embeddedLastReadTimestamps && embeddedLastReadTimestamps[nodeID]) {
        return parseInt(embeddedLastReadTimestamps[nodeID], 10);
      }
      return parseInt(storage.getItem('Drupal.history.' + currentUserID + '.' + nodeID) || 0, 10);
    },
    markAsRead: function markAsRead(nodeID) {
      $.ajax({
        url: Drupal.url('history/' + nodeID + '/read'),
        type: 'POST',
        dataType: 'json',
        success: function success(timestamp) {
          if (embeddedLastReadTimestamps && embeddedLastReadTimestamps[nodeID]) {
            return;
          }

          storage.setItem('Drupal.history.' + currentUserID + '.' + nodeID, timestamp);
        }
      });
    },
    needsServerCheck: function needsServerCheck(nodeID, contentTimestamp) {
      if (contentTimestamp < thirtyDaysAgo) {
        return false;
      }

      if (embeddedLastReadTimestamps && embeddedLastReadTimestamps[nodeID]) {
        return contentTimestamp > parseInt(embeddedLastReadTimestamps[nodeID], 10);
      }

      var minLastReadTimestamp = parseInt(storage.getItem('Drupal.history.' + currentUserID + '.' + nodeID) || 0, 10);
      return contentTimestamp > minLastReadTimestamp;
    }
  };
})(jQuery, Drupal, drupalSettings, window.localStorage);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (window, Drupal, drupalSettings) {
  window.addEventListener('load', function () {
    if (drupalSettings.history && drupalSettings.history.nodesToMarkAsRead) {
      Object.keys(drupalSettings.history.nodesToMarkAsRead).forEach(Drupal.history.markAsRead);
    }
  });
})(window, Drupal, drupalSettings);;
!function(t,e){if("function"==typeof define&&define.amd)define([],e);else if("object"==typeof module&&module.exports)module.exports=e();else{var i=e();t.Affix=i.Affix,t.Alert=i.Alert,t.Button=i.Button,t.Carousel=i.Carousel,t.Collapse=i.Collapse,t.Dropdown=i.Dropdown,t.Modal=i.Modal,t.Popover=i.Popover,t.ScrollSpy=i.ScrollSpy,t.Tab=i.Tab,t.Tooltip=i.Tooltip}}(this,function(){"use strict";var t="undefined"!=typeof global?global:this||window,e=document,i=e.documentElement,n="body",o=t.BSN={},a=o.supports=[],l="target",r="animation",c="getAttribute",s="setAttribute",d="parentNode",f="length",u="Transition",h="Webkit",p="style",g="push",m="active",v="left",b="top",y=!("opacity"in i[p]),w="onmouseleave"in e?["mouseenter","mouseleave"]:["mouseover","mouseout"],T=/\b(top|bottom|left|right)+/,x=0,k="navbar-fixed-top",C=h+u in i[p]||u.toLowerCase()in i[p],A=h+u in i[p]?h.toLowerCase()+u+"End":u.toLowerCase()+"end",I=h+"Duration"in i[p]?h.toLowerCase()+u+"Duration":u.toLowerCase()+"Duration",L={start:"touchstart",end:"touchend",move:"touchmove"},B=function(t){t.focus?t.focus():t.setActive()},H=function(t,e){t.classList.add(e)},E=function(t,e){t.classList.remove(e)},N=function(t,e){return t.classList.contains(e)},M=function(t){for(var e=[],i=0,n=t[f];i<n;i++)e[g](t[i]);return e},D=function(t,e){return M(t[y?"querySelectorAll":"getElementsByClassName"](y?"."+e.replace(/\s(?=[a-z])/g,"."):e))},S=function(t,i){var n=i||e;return"object"==typeof t?t:n.querySelector(t)},R=function(t,i){var n=i.charAt(0),o=i.substr(1);if("."===n){for(;t&&t!==e;t=t[d])if(null!==S(i,t[d])&&N(t,o))return t}else if("#"===n)for(;t&&t!==e;t=t[d])if(t.id===o)return t;return!1},W=function(t,e,i){t.addEventListener(e,i,!1)},P=function(t,e,i){t.removeEventListener(e,i,!1)},z=function(t,e,i){W(t,e,function n(o){i(o),P(t,e,n)})},O=function(e){var i=C?t.getComputedStyle(e)[I]:0;return i=parseFloat(i),i="number"!=typeof i||isNaN(i)?0:1e3*i},X=function(t,e){var i=0;O(t)?z(t,A,function(t){!i&&e(t),i=1}):setTimeout(function(){!i&&e(),i=1},17)},j=function(t,e,i){var n=new CustomEvent(t+".bs."+e);n.relatedTarget=i,this.dispatchEvent(n)},q=function(){return{y:t.pageYOffset||i.scrollTop,x:t.pageXOffset||i.scrollLeft}},U=function(t,o,a,l){var r,c,s,d,f={w:o.offsetWidth,h:o.offsetHeight},u=i.clientWidth||e[n].clientWidth,h=i.clientHeight||e[n].clientHeight,g=t.getBoundingClientRect(),m=l===e[n]?q():{x:l.offsetLeft+l.scrollLeft,y:l.offsetTop+l.scrollTop},y={w:g.right-g[v],h:g.bottom-g[b]},w=S('[class*="arrow"]',o),x=g[b]+y.h/2-f.h/2<0,k=g[v]+y.w/2-f.w/2<0,C=g[v]+f.w/2+y.w/2>=u,A=g[b]+f.h/2+y.h/2>=h,I=g[b]-f.h<0,L=g[v]-f.w<0,B=g[b]+f.h+y.h>=h,H=g[v]+f.w+y.w>=u;a=(a===v||"right"===a)&&L&&H?b:a,a=a===b&&I?"bottom":a,a="bottom"===a&&B?b:a,a=a===v&&L?"right":a,a="right"===a&&H?v:a,a===v||"right"===a?(c=a===v?g[v]+m.x-f.w:g[v]+m.x+y.w,x?(r=g[b]+m.y,s=y.h/2):A?(r=g[b]+m.y-f.h+y.h,s=f.h-y.h/2):r=g[b]+m.y-f.h/2+y.h/2):a!==b&&"bottom"!==a||(r=a===b?g[b]+m.y-f.h:g[b]+m.y+y.h,k?(c=0,d=g[v]+y.w/2):C?(c=u-1.01*f.w,d=f.w-(u-g[v])+y.w/2):c=g[v]+m.x-f.w/2+y.w/2),o[p][b]=r+"px",o[p][v]=c+"px",s&&(w[p][b]=s+"px"),d&&(w[p][v]=d+"px"),-1===o.className.indexOf(a)&&(o.className=o.className.replace(T,a))};o.version="2.0.26";var F=function(o,a){o=S(o),a=a||{};var r=o[c]("data-target"),s=o[c]("data-offset-top"),d=o[c]("data-offset-bottom"),f="affix",u="function";if(this[l]=a[l]?S(a[l]):S(r)||null,this.offsetTop=a.offsetTop?a.offsetTop:parseInt(s)||0,this.offsetBottom=a.offsetBottom?a.offsetBottom:parseInt(d)||0,this[l]||this.offsetTop||this.offsetBottom){var h,p,g,m,v,w,T=this,x=!1,k=!1,C=function(){return Math.max(e[n].scrollHeight,e[n].offsetHeight,i.clientHeight,i.scrollHeight,i.offsetHeight)},A=function(){return null!==T[l]?T[l].getBoundingClientRect()[b]+m:T.offsetTop?parseInt(typeof T.offsetTop===u?T.offsetTop():T.offsetTop||0):void 0},I=function(){if(T.offsetBottom)return g-o.offsetHeight-parseInt(typeof T.offsetBottom===u?T.offsetBottom():T.offsetBottom||0)},L=function(){g=C(),m=parseInt(q().y,0),h=A(),p=I(),v=parseInt(h)-m<0&&m>parseInt(h),w=parseInt(p)-m<0&&m>parseInt(p)},B=function(){x||N(o,f)||(j.call(o,f,f),j.call(o,"affix-top",f),H(o,f),x=!0,j.call(o,"affixed",f),j.call(o,"affixed-top",f))},M=function(){x&&N(o,f)&&(E(o,f),x=!1)},D=function(){k||N(o,"affix-bottom")||(j.call(o,f,f),j.call(o,"affix-bottom",f),H(o,"affix-bottom"),k=!0,j.call(o,"affixed",f),j.call(o,"affixed-bottom",f))},R=function(){k&&N(o,"affix-bottom")&&(E(o,"affix-bottom"),k=!1)},P=function(){w?(v&&M(),D()):(R(),v?B():M())};this.update=function(){L(),P()},"Affix"in o||(W(t,"scroll",T.update),!y&&W(t,"resize",T.update)),o.Affix=T,T.update()}};a[g](["Affix",F,'[data-spy="affix"]']);var Y=function(t){t=S(t);var e=this,i=R(t,".alert"),n=function(){N(i,"fade")?X(i,a):a()},o=function(n){i=R(n[l],".alert"),(t=S('[data-dismiss="alert"]',i))&&i&&(t===n[l]||t.contains(n[l]))&&e.close()},a=function(){j.call(i,"closed","alert"),P(t,"click",o),i[d].removeChild(i)};this.close=function(){i&&t&&N(i,"in")&&(j.call(i,"close","alert"),E(i,"in"),i&&n())},"Alert"in t||W(t,"click",o),t.Alert=e};a[g](["Alert",Y,'[data-dismiss="alert"]']);var G=function(t,i){t=S(t),i=i||null;var n=!1,o="checked",a=function(t){32===(t.which||t.keyCode)&&t[l]===e.activeElement&&u(t)},r=function(t){32===(t.which||t.keyCode)&&t.preventDefault()},u=function(e){var i="LABEL"===e[l].tagName?e[l]:"LABEL"===e[l][d].tagName?e[l][d]:null;if(i){var a=e[l],r=D(a[d],"btn"),u=i.getElementsByTagName("INPUT")[0];if(u){if("checkbox"===u.type&&(u[o]?(E(i,m),u[c](o),u.removeAttribute(o),u[o]=!1):(H(i,m),u[c](o),u[s](o,o),u[o]=!0),n||(n=!0,j.call(u,"change","button"),j.call(t,"change","button"))),"radio"===u.type&&!n&&!u[o]){H(i,m),u[s](o,o),u[o]=!0,j.call(u,"change","button"),j.call(t,"change","button"),n=!0;for(var h=0,p=r[f];h<p;h++){var g=r[h],v=g.getElementsByTagName("INPUT")[0];g!==i&&N(g,m)&&(E(g,m),v.removeAttribute(o),v[o]=!1,j.call(v,"change","button"))}}setTimeout(function(){n=!1},50)}}};if(N(t,"btn"))null!==i&&("reset"!==i?function(){i&&"reset"!==i&&("loading"===i&&(H(t,"disabled"),t[s]("disabled","disabled"),t[s]("data-original-text",t.innerHTML.trim())),t.innerHTML=t[c]("data-"+i+"-text"))}():function(){t[c]("data-original-text")&&((N(t,"disabled")||"disabled"===t[c]("disabled"))&&(E(t,"disabled"),t.removeAttribute("disabled")),t.innerHTML=t[c]("data-original-text"))}());else{"Button"in t||(W(t,"click",u),S("[tabindex]",t)&&W(t,"keyup",a),W(t,"keydown",r));for(var h=D(t,"btn"),p=h[f],g=0;g<p;g++)!N(h[g],m)&&S("input",h[g])[c](o)&&H(h[g],m);t.Button=this}};a[g](["Button",G,'[data-toggle="buttons"]']);var J=function(e,n){e=S(e),n=n||{};var o=e[c]("data-interval"),a=n.interval,r="false"===o?0:parseInt(o),s="hover"===e[c]("data-pause")||!1,d="true"===e[c]("data-keyboard")||!1;this.keyboard=!0===n.keyboard||d,this.pause=!("hover"!==n.pause&&!s)&&"hover",this.interval="number"==typeof a?a:!1===a||0===r||!1===r?0:isNaN(r)?5e3:r;var u=this,h=e.index=0,p=e.timer=0,g=!1,y=!1,T=null,x=null,k=null,A=D(e,"item"),I=A[f],B=this.direction=v,M=D(e,"carousel-control"),R=M[0],z=M[1],O=S(".carousel-indicators",e),q=O&&O.getElementsByTagName("LI")||[];if(!(I<2)){var U=function(){!1===u.interval||N(e,"paused")||(H(e,"paused"),!g&&(clearInterval(p),p=null))},F=function(){!1!==u.interval&&N(e,"paused")&&(E(e,"paused"),!g&&(clearInterval(p),p=null),!g&&u.cycle())},Y=function(t){if(t.preventDefault(),!g){var e=t[l];if(!e||N(e,m)||!e[c]("data-slide-to"))return!1;h=parseInt(e[c]("data-slide-to"),10),u.slideTo(h)}},G=function(t){if(t.preventDefault(),!g){var e=t.currentTarget||t.srcElement;e===z?h++:e===R&&h--,u.slideTo(h)}},J=function(t){if(!g){switch(t.which){case 39:h++;break;case 37:h--;break;default:return}u.slideTo(h)}},K=function(t){t(e,L.move,V),t(e,L.end,Z)},Q=function(t){y||(T=parseInt(t.touches[0].pageX),e.contains(t[l])&&(y=!0,K(W)))},V=function(t){return y?(x=parseInt(t.touches[0].pageX),"touchmove"===t.type&&t.touches[f]>1?(t.preventDefault(),!1):void 0):void t.preventDefault()},Z=function(t){if(y&&!g&&(k=x||parseInt(t.touches[0].pageX),y)){if((!e.contains(t[l])||!e.contains(t.relatedTarget))&&Math.abs(T-k)<75)return!1;x<T?h++:x>T&&h--,y=!1,u.slideTo(h),K(P)}},$=function(){var n=e.getBoundingClientRect(),o=t.innerHeight||i.clientHeight;return n[b]<=o&&n.bottom>=0},_=function(t){for(var e=0,i=q[f];e<i;e++)E(q[e],m);q[t]&&H(q[t],m)};this.cycle=function(){p&&(clearInterval(p),p=null),p=setInterval(function(){$()&&(h++,u.slideTo(h))},this.interval)},this.slideTo=function(t){if(!g){var i,n=this.getActiveIndex();n!==t&&(n<t||0===n&&t===I-1?B=u.direction=v:(n>t||n===I-1&&0===t)&&(B=u.direction="right"),t<0?t=I-1:t>=I&&(t=0),h=t,i=B===v?"next":"prev",j.call(e,"slide","carousel",A[t]),g=!0,clearInterval(p),p=null,_(t),C&&N(e,"slide")?(H(A[t],i),A[t].offsetWidth,H(A[t],B),H(A[n],B),X(A[t],function(o){var a=o&&o[l]!==A[t]?1e3*o.elapsedTime+100:20;g&&setTimeout(function(){g=!1,H(A[t],m),E(A[n],m),E(A[t],i),E(A[t],B),E(A[n],B),j.call(e,"slid","carousel",A[t]),u.interval&&!N(e,"paused")&&u.cycle()},a)})):(H(A[t],m),A[t].offsetWidth,E(A[n],m),setTimeout(function(){g=!1,u.interval&&!N(e,"paused")&&u.cycle(),j.call(e,"slid","carousel",A[t])},100)))}},this.getActiveIndex=function(){return A.indexOf(D(e,"item active")[0])||0},"Carousel"in e||(u.pause&&u.interval&&(W(e,w[0],U),W(e,w[1],F),W(e,L.start,U),W(e,L.end,F)),A[f]>1&&W(e,L.start,Q),z&&W(z,"click",G),R&&W(R,"click",G),O&&W(O,"click",Y),u.keyboard&&W(t,"keydown",J)),u.getActiveIndex()<0&&(A[f]&&H(A[0],m),q[f]&&_(0)),u.interval&&u.cycle(),e.Carousel=u}};a[g](["Carousel",J,'[data-ride="carousel"]']);var K=function(t,e){t=S(t),e=e||{};var i,n,o=null,a=null,l=this,r=t[c]("data-parent"),d=function(t,e){j.call(t,"show","collapse"),t.isAnimating=!0,H(t,"collapsing"),E(t,"collapse"),t[p].height=t.scrollHeight+"px",X(t,function(){t.isAnimating=!1,t[s]("aria-expanded","true"),e[s]("aria-expanded","true"),E(t,"collapsing"),H(t,"collapse"),H(t,"in"),t[p].height="",j.call(t,"shown","collapse")})},f=function(t,e){j.call(t,"hide","collapse"),t.isAnimating=!0,t[p].height=t.scrollHeight+"px",E(t,"collapse"),E(t,"in"),H(t,"collapsing"),t.offsetWidth,t[p].height="0px",X(t,function(){t.isAnimating=!1,t[s]("aria-expanded","false"),e[s]("aria-expanded","false"),E(t,"collapsing"),H(t,"collapse"),t[p].height="",j.call(t,"hidden","collapse")})};this.toggle=function(t){t.preventDefault(),N(a,"in")?l.hide():l.show()},this.hide=function(){a.isAnimating||(f(a,t),H(t,"collapsed"))},this.show=function(){o&&(i=S(".collapse.in",o),n=i&&(S('[data-target="#'+i.id+'"]',o)||S('[href="#'+i.id+'"]',o))),(!a.isAnimating||i&&!i.isAnimating)&&(n&&i!==a&&(f(i,n),H(n,"collapsed")),d(a,t),E(t,"collapsed"))},"Collapse"in t||W(t,"click",l.toggle),a=function(){var e=t.href&&t[c]("href"),i=t[c]("data-target"),n=e||i&&"#"===i.charAt(0)&&i;return n&&S(n)}(),a.isAnimating=!1,o=S(e.parent)||r&&R(t,r),t.Collapse=l};a[g](["Collapse",K,'[data-toggle="collapse"]']);var Q=function(t,i){t=S(t),this.persist=!0===i||"true"===t[c]("data-persist")||!1;var n=this,o=t[d],a="open",r=null,u=S(".dropdown-menu",o),h=function(){for(var t=u.children,e=[],i=0;i<t[f];i++)t[i].children[f]&&"A"===t[i].children[0].tagName&&e[g](t[i]);return e}(),p=function(t){(t.href&&"#"===t.href.slice(-1)||t[d]&&t[d].href&&"#"===t[d].href.slice(-1))&&this.preventDefault()},m=function(){var i=t[a]?W:P;i(e,"click",v),i(e,"keydown",y),i(e,"keyup",w)},v=function(e){var i=e[l],o=i&&("Dropdown"in i||"Dropdown"in i[d]);(i!==u&&!u.contains(i)||!n.persist&&!o)&&(r=i===t||t.contains(i)?t:null,x(),p.call(e,i))},b=function(e){r=t,T(),p.call(e,e[l])},y=function(t){var e=t.which||t.keyCode;38!==e&&40!==e||t.preventDefault()},w=function(i){var o=i.which||i.keyCode,l=e.activeElement,c=h.indexOf(l[d]),s=l===t,p=u.contains(l),g=l[d][d]===u;(g||s)&&(c=s?0:38===o?c>1?c-1:0:40===o&&c<h[f]-1?c+1:c,h[c]&&B(h[c].children[0])),(h[f]&&g||!h[f]&&(p||s)||!p)&&t[a]&&27===o&&(n.toggle(),r=null)},T=function(){j.call(o,"show","dropdown",r),H(o,a),t[s]("aria-expanded",!0),j.call(o,"shown","dropdown",r),t[a]=!0,P(t,"click",b),setTimeout(function(){B(u.getElementsByTagName("INPUT")[0]||t),m()},1)},x=function(){j.call(o,"hide","dropdown",r),E(o,a),t[s]("aria-expanded",!1),j.call(o,"hidden","dropdown",r),t[a]=!1,m(),B(t),setTimeout(function(){W(t,"click",b)},1)};t[a]=!1,this.toggle=function(){N(o,a)&&t[a]?x():T()},"Dropdown"in t||(!1 in u&&u[s]("tabindex","0"),W(t,"click",b)),t.Dropdown=n};a[g](["Dropdown",Q,'[data-toggle="dropdown"]']);var V=function(o,a){o=S(o);var r=o[c]("data-target")||o[c]("href"),u=S(r),h=N(o,"modal")?o:u;if(N(o,"modal")&&(o=null),h){a=a||{},this.keyboard=!1!==a.keyboard&&"false"!==h[c]("data-keyboard"),this.backdrop="static"!==a.backdrop&&"static"!==h[c]("data-backdrop")||"static",this.backdrop=!1!==a.backdrop&&"false"!==h[c]("data-backdrop")&&this.backdrop,this.content=a.content;var g,m,b,y,w=this,T=null,A=D(i,k).concat(D(i,"navbar-fixed-bottom")),I=function(){var e=i.getBoundingClientRect();return t.innerWidth||e.right-Math.abs(e[v])},L=function(){var i,o=e[n].currentStyle||t.getComputedStyle(e[n]),a=parseInt(o.paddingRight,10);if(g&&(e[n][p].paddingRight=a+m+"px",h[p].paddingRight=m+"px",A[f]))for(var l=0;l<A[f];l++)i=(A[l].currentStyle||t.getComputedStyle(A[l])).paddingRight,A[l][p].paddingRight=parseInt(i)+m+"px"},M=function(){if(e[n][p].paddingRight="",h[p].paddingRight="",A[f])for(var t=0;t<A[f];t++)A[t][p].paddingRight=""},R=function(){var t,i=e.createElement("div");return i.className="modal-scrollbar-measure",e[n].appendChild(i),t=i.offsetWidth-i.clientWidth,e[n].removeChild(i),t},z=function(){g=e[n].clientWidth<I(),m=R()},q=function(){x=1;var t=e.createElement("div");null===(b=S(".modal-backdrop"))&&(t[s]("class","modal-backdrop fade"),b=t,e[n].appendChild(b))},U=function(){b=S(".modal-backdrop"),b&&null!==b&&"object"==typeof b&&(x=0,e[n].removeChild(b),b=null),j.call(h,"hidden","modal")},F=function(){N(h,"in")?W(e,"keydown",V):P(e,"keydown",V)},Y=function(){N(h,"in")?W(t,"resize",w.update):P(t,"resize",w.update)},G=function(){N(h,"in")?W(h,"click",Z):P(h,"click",Z)},J=function(){Y(),G(),F(),B(h),j.call(h,"shown","modal",T)},K=function(){h[p].display="",o&&B(o),function(){D(e,"modal in")[0]||(M(),E(e[n],"modal-open"),b&&N(b,"fade")?(E(b,"in"),X(b,U)):U(),Y(),G(),F())}()},Q=function(t){var e=t[l];(e=e.hasAttribute("data-target")||e.hasAttribute("href")?e:e[d])!==o||N(h,"in")||(h.modalTrigger=o,T=o,w.show(),t.preventDefault())},V=function(t){var e=t.which||t.keyCode;w.keyboard&&27==e&&N(h,"in")&&w.hide()},Z=function(t){var e=t[l];N(h,"in")&&("modal"===e[d][c]("data-dismiss")||"modal"===e[c]("data-dismiss")||e===h&&"static"!==w.backdrop)&&(w.hide(),T=null,t.preventDefault())};this.toggle=function(){N(h,"in")?this.hide():this.show()},this.show=function(){j.call(h,"show","modal",T);var t=D(e,"modal in")[0];t&&t!==h&&("modalTrigger"in t&&t.modalTrigger.Modal.hide(),"Modal"in t&&t.Modal.hide()),this.backdrop&&!x&&q(),b&&x&&!N(b,"in")&&(b.offsetWidth,y=O(b),H(b,"in")),setTimeout(function(){h[p].display="block",z(),L(),H(e[n],"modal-open"),H(h,"in"),h[s]("aria-hidden",!1),N(h,"fade")?X(h,J):J()},C&&b?y:0)},this.hide=function(){j.call(h,"hide","modal"),b=S(".modal-backdrop"),y=b&&O(b),E(h,"in"),h[s]("aria-hidden",!0),setTimeout(function(){N(h,"fade")?X(h,K):K()},C&&b?y:0)},this.setContent=function(t){S(".modal-content",h).innerHTML=t},this.update=function(){N(h,"in")&&(z(),L())},!o||"Modal"in o||W(o,"click",Q),w.content&&w.setContent(w.content),o?(o.Modal=w,h.modalTrigger=o):h.Modal=w}};a[g](["Modal",V,'[data-toggle="modal"]']);var Z=function(i,o){i=S(i),o=o||{};var a=i[c]("data-trigger"),d=i[c]("data-animation"),f=i[c]("data-placement"),u=i[c]("data-dismissible"),h=i[c]("data-delay"),g=i[c]("data-container"),m='<button type="button" class="close">×</button>',v=S(o.container),T=S(g),x=R(i,".modal"),C=R(i,"."+k),A=R(i,".navbar-fixed-bottom");this.template=o.template?o.template:null,this.trigger=o.trigger?o.trigger:a||"hover",this[r]=o[r]&&"fade"!==o[r]?o[r]:d||"fade",this.placement=o.placement?o.placement:f||b,this.delay=parseInt(o.delay||h)||200,this.dismissible=!(!o.dismissible&&"true"!==u),this.container=v||(T||(C||(A||(x||e[n]))));var I=this,L=o.title||i[c]("data-title")||null,B=o.content||i[c]("data-content")||null;if(B||this.template){var M=null,D=0,z=this.placement,O=function(t){null!==M&&t[l]===S(".close",M)&&I.hide()},q=function(){I.container.removeChild(M),D=null,M=null},F=function(){if(L=i[c]("data-title"),B=i[c]("data-content"),M=e.createElement("div"),null!==B&&null===I.template){if(M[s]("role","tooltip"),null!==L){var t=e.createElement("h3");t[s]("class","popover-title"),t.innerHTML=I.dismissible?L+m:L,M.appendChild(t)}var n=e.createElement("div"),o=e.createElement("div");n[s]("class","arrow"),o[s]("class","popover-content"),M.appendChild(n),M.appendChild(o),o.innerHTML=I.dismissible&&null===L?B+m:B}else{var a=e.createElement("div");a.innerHTML=I.template,M.innerHTML=a.firstChild.innerHTML}I.container.appendChild(M),M[p].display="block",M[s]("class","popover "+z+" "+I[r])},Y=function(){!N(M,"in")&&H(M,"in")},G=function(){U(i,M,z,I.container)},J=function(n){"click"!=I.trigger&&"focus"!=I.trigger||!I.dismissible&&n(i,"blur",I.hide),I.dismissible&&n(e,"click",O),!y&&n(t,"resize",I.hide)},K=function(){J(W),j.call(i,"shown","popover")},Q=function(){J(P),q(),j.call(i,"hidden","popover")};this.toggle=function(){null===M?I.show():I.hide()},this.show=function(){clearTimeout(D),D=setTimeout(function(){null===M&&(z=I.placement,F(),G(),Y(),j.call(i,"show","popover"),I[r]?X(M,K):K())},20)},this.hide=function(){clearTimeout(D),D=setTimeout(function(){M&&null!==M&&N(M,"in")&&(j.call(i,"hide","popover"),E(M,"in"),I[r]?X(M,Q):Q())},I.delay)},"Popover"in i||("hover"===I.trigger?(W(i,w[0],I.show),I.dismissible||W(i,w[1],I.hide)):"click"!=I.trigger&&"focus"!=I.trigger||W(i,I.trigger,I.toggle)),i.Popover=I}};a[g](["Popover",Z,'[data-toggle="popover"]']);var $=function(e,i){e=S(e);var n=S(e[c]("data-target")),o=e[c]("data-offset");if(i=i||{},i[l]||n){for(var a,r=this,s=i[l]&&S(i[l])||n,u=s&&s.getElementsByTagName("A"),h=parseInt(i.offset||o)||10,p=[],v=[],w=e.offsetHeight<e.scrollHeight?e:t,T=w===t,x=0,k=u[f];x<k;x++){var C=u[x][c]("href"),A=C&&"#"===C.charAt(0)&&"#"!==C.slice(-1)&&S(C);A&&(p[g](u[x]),v[g](A))}var I=function(t){var i=p[t][d],n=v[t],o=R(i,".dropdown"),l=T&&n.getBoundingClientRect(),r=N(i,m)||!1,c=(T?l[b]+a:n.offsetTop)-h,s=T?l.bottom+a-h:v[t+1]?v[t+1].offsetTop-h:e.scrollHeight,f=a>=c&&s>a;if(!r&&f)"LI"!==i.tagName||N(i,m)||(H(i,m),o&&!N(o,m)&&H(o,m),j.call(e,"activate","scrollspy",p[t]));else if(f){if(!f&&!r||r&&f)return}else"LI"===i.tagName&&N(i,m)&&(E(i,m),o&&N(o,m)&&!D(i[d],m).length&&E(o,m))},L=function(){a=T?q().y:e.scrollTop;for(var t=0,i=p[f];t<i;t++)I(t)};this.refresh=function(){L()},"ScrollSpy"in e||(W(w,"scroll",r.refresh),!y&&W(t,"resize",r.refresh)),r.refresh(),e.ScrollSpy=r}};a[g](["ScrollSpy",$,'[data-spy="scroll"]']);var _=function(t,e){t=S(t);var i=t[c]("data-height");e=e||{},this.height=!!C&&(e.height||"true"===i);var n,o,a,l,r,u,h,g=this,b=R(t,".nav"),y=!1,w=b&&S(".dropdown",b),T=function(){y[p].height="",E(y,"collapsing"),b.isAnimating=!1},x=function(){y?u?T():setTimeout(function(){y[p].height=h+"px",y.offsetWidth,X(y,T)},50):b.isAnimating=!1,j.call(n,"shown","tab",o)},k=function(){y&&(a[p].float=v,l[p].float=v,r=a.scrollHeight),H(l,m),j.call(n,"show","tab",o),E(a,m),j.call(o,"hidden","tab",n),y&&(h=l.scrollHeight,u=h===r,H(y,"collapsing"),y[p].height=r+"px",y.offsetHeight,a[p].float="",l[p].float=""),N(l,"fade")?setTimeout(function(){H(l,"in"),X(l,x)},20):x()};if(b){b.isAnimating=!1;var A=function(){var t,e=D(b,m);return 1!==e[f]||N(e[0],"dropdown")?e[f]>1&&(t=e[e[f]-1]):t=e[0],t.getElementsByTagName("A")[0]},I=function(){return S(A()[c]("href"))},L=function(t){t.preventDefault(),n=t.currentTarget||this,!b.isAnimating&&!N(n[d],m)&&g.show()};this.show=function(){n=n||t,l=S(n[c]("href")),o=A(),a=I(),b.isAnimating=!0,E(o[d],m),o[s]("aria-expanded","false"),H(n[d],m),n[s]("aria-expanded","true"),w&&(N(t[d][d],"dropdown-menu")?N(w,m)||H(w,m):N(w,m)&&E(w,m)),j.call(o,"hide","tab",n),N(a,"fade")?(E(a,"in"),X(a,k)):k()},"Tab"in t||W(t,"click",L),g.height&&(y=I()[d]),t.Tab=g}};a[g](["Tab",_,'[data-toggle="tab"]']);var tt=function(i,o){i=S(i),o=o||{};var a=i[c]("data-animation"),l=i[c]("data-placement"),d=i[c]("data-delay"),f=i[c]("data-container"),u=S(o.container),h=S(f),p=R(i,".modal"),g=R(i,"."+k),m=R(i,".navbar-fixed-bottom");this[r]=o[r]&&"fade"!==o[r]?o[r]:a||"fade",this.placement=o.placement?o.placement:l||b,this.delay=parseInt(o.delay||d)||200,this.container=u||(h||(g||(m||(p||e[n]))));var v=this,T=0,x=this.placement,C=null,A=i[c]("title")||i[c]("data-title")||i[c]("data-original-title");if(A&&""!=A){var I=function(){v.container.removeChild(C),C=null,T=null},L=function(){if(!(A=i[c]("title")||i[c]("data-title")||i[c]("data-original-title"))||""==A)return!1;C=e.createElement("div"),C[s]("role","tooltip");var t=e.createElement("div"),n=e.createElement("div");t[s]("class","tooltip-arrow"),n[s]("class","tooltip-inner"),C.appendChild(t),C.appendChild(n),n.innerHTML=A,v.container.appendChild(C),C[s]("class","tooltip "+x+" "+v[r])},B=function(){U(i,C,x,v.container)},M=function(){!N(C,"in")&&H(C,"in")},D=function(){j.call(i,"shown","tooltip"),!y&&W(t,"resize",v.hide)},z=function(){!y&&P(t,"resize",v.hide),I(),j.call(i,"hidden","tooltip")};this.show=function(){clearTimeout(T),T=setTimeout(function(){if(null===C){if(x=v.placement,0==L())return;B(),M(),j.call(i,"show","tooltip"),v[r]?X(C,D):D()}},20)},this.hide=function(){clearTimeout(T),T=setTimeout(function(){C&&N(C,"in")&&(j.call(i,"hide","tooltip"),E(C,"in"),v[r]?X(C,z):z())},v.delay)},this.toggle=function(){C?v.hide():v.show()},"Tooltip"in i||(i[s]("data-original-title",A),i.removeAttribute("title"),W(i,w[0],v.show),W(i,w[1],v.hide)),i.Tooltip=v}};a[g](["Tooltip",tt,'[data-toggle="tooltip"]']);var et=function(t,e){for(var i=0,n=e[f];i<n;i++)new t(e[i])},it=o.initCallback=function(t){t=t||e;for(var i=0,n=a[f];i<n;i++)et(a[i][1],t.querySelectorAll(a[i][2]))};return e[n]?it():W(e,"DOMContentLoaded",function(){it()}),{Affix:F,Alert:Y,Button:G,Carousel:J,Collapse:K,Dropdown:Q,Modal:V,Popover:Z,ScrollSpy:$,Tab:_,Tooltip:tt}});

const dlink = document.querySelectorAll('.dropdown');
for (let i = 0; i < dlink.length; i++) {
    dlink[i].addEventListener("click", function(e) {
        e.preventDefault(); 
    });
};

//Toggle
const trigger = document.getElementById('toggle');
const trigger_point = document.getElementById('site');
const trigger_remove = document.querySelectorAll('.not-dropdown');

trigger.addEventListener('click', function(e) {
  e.preventDefault();
  trigger_point.classList.toggle('site-menu');
}); 
for (let i = 0; i < trigger_remove.length; i++) {
    trigger_remove[i].addEventListener("click", function(e) {
        trigger_point.classList.remove('site-menu');
    });
};


//Smooth

if (document.body.classList.contains('node-page')) {

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});

}; ;
window.matchMedia||(window.matchMedia=function(){"use strict";var e=window.styleMedia||window.media;if(!e){var t=document.createElement("style"),i=document.getElementsByTagName("script")[0],n=null;t.type="text/css";t.id="matchmediajs-test";i.parentNode.insertBefore(t,i);n="getComputedStyle"in window&&window.getComputedStyle(t,null)||t.currentStyle;e={matchMedium:function(e){var i="@media "+e+"{ #matchmediajs-test { width: 1px; } }";if(t.styleSheet){t.styleSheet.cssText=i}else{t.textContent=i}return n.width==="1px"}}}return function(t){return{matches:e.matchMedium(t||"all"),media:t||"all"}}}());
;
(function(){if(window.matchMedia&&window.matchMedia("all").addListener){return false}var e=window.matchMedia,i=e("only all").matches,n=false,t=0,a=[],r=function(i){clearTimeout(t);t=setTimeout(function(){for(var i=0,n=a.length;i<n;i++){var t=a[i].mql,r=a[i].listeners||[],o=e(t.media).matches;if(o!==t.matches){t.matches=o;for(var s=0,l=r.length;s<l;s++){r[s].call(window,t)}}}},30)};window.matchMedia=function(t){var o=e(t),s=[],l=0;o.addListener=function(e){if(!i){return}if(!n){n=true;window.addEventListener("resize",r,true)}if(l===0){l=a.push({mql:o,listeners:s})}s.push(e)};o.removeListener=function(e){for(var i=0,n=s.length;i<n;i++){if(s[i]===e){s.splice(i,1)}}};return o}})();
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  var activeItem = Drupal.url(drupalSettings.path.currentPath);

  $.fn.drupalToolbarMenu = function () {
    var ui = {
      handleOpen: Drupal.t('Extend'),
      handleClose: Drupal.t('Collapse')
    };

    function toggleList($item, switcher) {
      var $toggle = $item.children('.toolbar-box').children('.toolbar-handle');
      switcher = typeof switcher !== 'undefined' ? switcher : !$item.hasClass('open');

      $item.toggleClass('open', switcher);

      $toggle.toggleClass('open', switcher);

      $toggle.find('.action').text(switcher ? ui.handleClose : ui.handleOpen);
    }

    function toggleClickHandler(event) {
      var $toggle = $(event.target);
      var $item = $toggle.closest('li');

      toggleList($item);

      var $openItems = $item.siblings().filter('.open');
      toggleList($openItems, false);
    }

    function linkClickHandler(event) {
      if (!Drupal.toolbar.models.toolbarModel.get('isFixed')) {
        Drupal.toolbar.models.toolbarModel.set('activeTab', null);
      }

      event.stopPropagation();
    }

    function initItems($menu) {
      var options = {
        class: 'toolbar-icon toolbar-handle',
        action: ui.handleOpen,
        text: ''
      };

      $menu.find('li > a').wrap('<div class="toolbar-box">');

      $menu.find('li').each(function (index, element) {
        var $item = $(element);
        if ($item.children('ul.toolbar-menu').length) {
          var $box = $item.children('.toolbar-box');
          options.text = Drupal.t('@label', {
            '@label': $box.find('a').text()
          });
          $item.children('.toolbar-box').append(Drupal.theme('toolbarMenuItemToggle', options));
        }
      });
    }

    function markListLevels($lists, level) {
      level = !level ? 1 : level;
      var $lis = $lists.children('li').addClass('level-' + level);
      $lists = $lis.children('ul');
      if ($lists.length) {
        markListLevels($lists, level + 1);
      }
    }

    function openActiveItem($menu) {
      var pathItem = $menu.find('a[href="' + window.location.pathname + '"]');
      if (pathItem.length && !activeItem) {
        activeItem = window.location.pathname;
      }
      if (activeItem) {
        var $activeItem = $menu.find('a[href="' + activeItem + '"]').addClass('menu-item--active');
        var $activeTrail = $activeItem.parentsUntil('.root', 'li').addClass('menu-item--active-trail');
        toggleList($activeTrail, true);
      }
    }

    return this.each(function (selector) {
      var $menu = $(this).once('toolbar-menu');
      if ($menu.length) {
        $menu.on('click.toolbar', '.toolbar-box', toggleClickHandler).on('click.toolbar', '.toolbar-box a', linkClickHandler);

        $menu.addClass('root');
        initItems($menu);
        markListLevels($menu);

        openActiveItem($menu);
      }
    });
  };

  Drupal.theme.toolbarMenuItemToggle = function (options) {
    return '<button class="' + options.class + '"><span class="action">' + options.action + '</span> <span class="label">' + options.text + '</span></button>';
  };
})(jQuery, Drupal, drupalSettings);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  var options = $.extend({
    breakpoints: {
      'toolbar.narrow': '',
      'toolbar.standard': '',
      'toolbar.wide': ''
    }
  }, drupalSettings.toolbar, {
    strings: {
      horizontal: Drupal.t('Horizontal orientation'),
      vertical: Drupal.t('Vertical orientation')
    }
  });

  Drupal.behaviors.toolbar = {
    attach: function attach(context) {
      if (!window.matchMedia('only screen').matches) {
        return;
      }

      $(context).find('#toolbar-administration').once('toolbar').each(function () {
        var model = new Drupal.toolbar.ToolbarModel({
          locked: JSON.parse(localStorage.getItem('Drupal.toolbar.trayVerticalLocked')),
          activeTab: document.getElementById(JSON.parse(localStorage.getItem('Drupal.toolbar.activeTabID'))),
          height: $('#toolbar-administration').outerHeight()
        });

        Drupal.toolbar.models.toolbarModel = model;

        Object.keys(options.breakpoints).forEach(function (label) {
          var mq = options.breakpoints[label];
          var mql = window.matchMedia(mq);
          Drupal.toolbar.mql[label] = mql;

          mql.addListener(Drupal.toolbar.mediaQueryChangeHandler.bind(null, model, label));

          Drupal.toolbar.mediaQueryChangeHandler.call(null, model, label, mql);
        });

        Drupal.toolbar.views.toolbarVisualView = new Drupal.toolbar.ToolbarVisualView({
          el: this,
          model: model,
          strings: options.strings
        });
        Drupal.toolbar.views.toolbarAuralView = new Drupal.toolbar.ToolbarAuralView({
          el: this,
          model: model,
          strings: options.strings
        });
        Drupal.toolbar.views.bodyVisualView = new Drupal.toolbar.BodyVisualView({
          el: this,
          model: model
        });

        model.trigger('change:isFixed', model, model.get('isFixed'));
        model.trigger('change:activeTray', model, model.get('activeTray'));

        var menuModel = new Drupal.toolbar.MenuModel();
        Drupal.toolbar.models.menuModel = menuModel;
        Drupal.toolbar.views.menuVisualView = new Drupal.toolbar.MenuVisualView({
          el: $(this).find('.toolbar-menu-administration').get(0),
          model: menuModel,
          strings: options.strings
        });

        Drupal.toolbar.setSubtrees.done(function (subtrees) {
          menuModel.set('subtrees', subtrees);
          var theme = drupalSettings.ajaxPageState.theme;
          localStorage.setItem('Drupal.toolbar.subtrees.' + theme, JSON.stringify(subtrees));

          model.set('areSubtreesLoaded', true);
        });

        Drupal.toolbar.views.toolbarVisualView.loadSubtrees();

        $(document).on('drupalViewportOffsetChange.toolbar', function (event, offsets) {
          model.set('offsets', offsets);
        });

        model.on('change:orientation', function (model, orientation) {
          $(document).trigger('drupalToolbarOrientationChange', orientation);
        }).on('change:activeTab', function (model, tab) {
          $(document).trigger('drupalToolbarTabChange', tab);
        }).on('change:activeTray', function (model, tray) {
          $(document).trigger('drupalToolbarTrayChange', tray);
        });

        if (Drupal.toolbar.models.toolbarModel.get('orientation') === 'horizontal' && Drupal.toolbar.models.toolbarModel.get('activeTab') === null) {
          Drupal.toolbar.models.toolbarModel.set({
            activeTab: $('.toolbar-bar .toolbar-tab:not(.home-toolbar-tab) a').get(0)
          });
        }

        $(window).on({
          'dialog:aftercreate': function dialogAftercreate(event, dialog, $element, settings) {
            var $toolbar = $('#toolbar-bar');
            $toolbar.css('margin-top', '0');

            if (settings.drupalOffCanvasPosition === 'top') {
              var height = Drupal.offCanvas.getContainer($element).outerHeight();
              $toolbar.css('margin-top', height + 'px');

              $element.on('dialogContentResize.off-canvas', function () {
                var newHeight = Drupal.offCanvas.getContainer($element).outerHeight();
                $toolbar.css('margin-top', newHeight + 'px');
              });
            }
          },
          'dialog:beforeclose': function dialogBeforeclose() {
            $('#toolbar-bar').css('margin-top', '0');
          }
        });
      });
    }
  };

  Drupal.toolbar = {
    views: {},

    models: {},

    mql: {},

    setSubtrees: new $.Deferred(),

    mediaQueryChangeHandler: function mediaQueryChangeHandler(model, label, mql) {
      switch (label) {
        case 'toolbar.narrow':
          model.set({
            isOriented: mql.matches,
            isTrayToggleVisible: false
          });

          if (!mql.matches || !model.get('orientation')) {
            model.set({ orientation: 'vertical' }, { validate: true });
          }
          break;

        case 'toolbar.standard':
          model.set({
            isFixed: mql.matches
          });
          break;

        case 'toolbar.wide':
          model.set({
            orientation: mql.matches && !model.get('locked') ? 'horizontal' : 'vertical'
          }, { validate: true });

          model.set({
            isTrayToggleVisible: mql.matches
          });
          break;

        default:
          break;
      }
    }
  };

  Drupal.theme.toolbarOrientationToggle = function () {
    return '<div class="toolbar-toggle-orientation"><div class="toolbar-lining">' + '<button class="toolbar-icon" type="button"></button>' + '</div></div>';
  };

  Drupal.AjaxCommands.prototype.setToolbarSubtrees = function (ajax, response, status) {
    Drupal.toolbar.setSubtrees.resolve(response.subtrees);
  };
})(jQuery, Drupal, drupalSettings);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Backbone, Drupal) {
  Drupal.toolbar.MenuModel = Backbone.Model.extend({
    defaults: {
      subtrees: {}
    }
  });
})(Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Backbone, Drupal) {
  Drupal.toolbar.ToolbarModel = Backbone.Model.extend({
    defaults: {
      activeTab: null,

      activeTray: null,

      isOriented: false,

      isFixed: false,

      areSubtreesLoaded: false,

      isViewportOverflowConstrained: false,

      orientation: 'horizontal',

      locked: false,

      isTrayToggleVisible: true,

      height: null,

      offsets: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      }
    },

    validate: function validate(attributes, options) {
      if (attributes.orientation === 'horizontal' && this.get('locked') && !options.override) {
        return Drupal.t('The toolbar cannot be set to a horizontal orientation when it is locked.');
      }
    }
  });
})(Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, Backbone) {
  Drupal.toolbar.BodyVisualView = Backbone.View.extend({
    initialize: function initialize() {
      this.listenTo(this.model, 'change:activeTray ', this.render);
      this.listenTo(this.model, 'change:isFixed change:isViewportOverflowConstrained', this.isToolbarFixed);
    },
    isToolbarFixed: function isToolbarFixed() {
      var isViewportOverflowConstrained = this.model.get('isViewportOverflowConstrained');
      $('body').toggleClass('toolbar-fixed', isViewportOverflowConstrained || this.model.get('isFixed'));
    },
    render: function render() {
      $('body').toggleClass('toolbar-tray-open', !!this.model.get('activeTray'));
    }
  });
})(jQuery, Drupal, Backbone);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Backbone, Drupal) {
  Drupal.toolbar.MenuVisualView = Backbone.View.extend({
    initialize: function initialize() {
      this.listenTo(this.model, 'change:subtrees', this.render);
    },
    render: function render() {
      var _this = this;

      var subtrees = this.model.get('subtrees');

      Object.keys(subtrees || {}).forEach(function (id) {
        _this.$el.find('#toolbar-link-' + id).once('toolbar-subtrees').after(subtrees[id]);
      });

      if ('drupalToolbarMenu' in $.fn) {
        this.$el.children('.toolbar-menu').drupalToolbarMenu();
      }
    }
  });
})(jQuery, Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Backbone, Drupal) {
  Drupal.toolbar.ToolbarAuralView = Backbone.View.extend({
    initialize: function initialize(options) {
      this.strings = options.strings;

      this.listenTo(this.model, 'change:orientation', this.onOrientationChange);
      this.listenTo(this.model, 'change:activeTray', this.onActiveTrayChange);
    },
    onOrientationChange: function onOrientationChange(model, orientation) {
      Drupal.announce(Drupal.t('Tray orientation changed to @orientation.', {
        '@orientation': orientation
      }));
    },
    onActiveTrayChange: function onActiveTrayChange(model, tray) {
      var relevantTray = tray === null ? model.previous('activeTray') : tray;

      if (!relevantTray) {
        return;
      }
      var action = tray === null ? Drupal.t('closed') : Drupal.t('opened');
      var trayNameElement = relevantTray.querySelector('.toolbar-tray-name');
      var text = void 0;
      if (trayNameElement !== null) {
        text = Drupal.t('Tray "@tray" @action.', {
          '@tray': trayNameElement.textContent,
          '@action': action
        });
      } else {
        text = Drupal.t('Tray @action.', { '@action': action });
      }
      Drupal.announce(text);
    }
  });
})(Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings, Backbone) {
  Drupal.toolbar.ToolbarVisualView = Backbone.View.extend({
    events: function events() {
      var touchEndToClick = function touchEndToClick(event) {
        event.preventDefault();
        event.target.click();
      };

      return {
        'click .toolbar-bar .toolbar-tab .trigger': 'onTabClick',
        'click .toolbar-toggle-orientation button': 'onOrientationToggleClick',
        'touchend .toolbar-bar .toolbar-tab .trigger': touchEndToClick,
        'touchend .toolbar-toggle-orientation button': touchEndToClick
      };
    },
    initialize: function initialize(options) {
      this.strings = options.strings;

      this.listenTo(this.model, 'change:activeTab change:orientation change:isOriented change:isTrayToggleVisible', this.render);
      this.listenTo(this.model, 'change:mqMatches', this.onMediaQueryChange);
      this.listenTo(this.model, 'change:offsets', this.adjustPlacement);
      this.listenTo(this.model, 'change:activeTab change:orientation change:isOriented', this.updateToolbarHeight);

      this.$el.find('.toolbar-tray .toolbar-lining').append(Drupal.theme('toolbarOrientationToggle'));

      this.model.trigger('change:activeTab');
    },
    updateToolbarHeight: function updateToolbarHeight() {
      var toolbarTabOuterHeight = $('#toolbar-bar').find('.toolbar-tab').outerHeight() || 0;
      var toolbarTrayHorizontalOuterHeight = $('.is-active.toolbar-tray-horizontal').outerHeight() || 0;
      this.model.set('height', toolbarTabOuterHeight + toolbarTrayHorizontalOuterHeight);

      $('body').css({
        'padding-top': this.model.get('height')
      });

      this.triggerDisplace();
    },
    triggerDisplace: function triggerDisplace() {
      _.defer(function () {
        Drupal.displace(true);
      });
    },
    render: function render() {
      this.updateTabs();
      this.updateTrayOrientation();
      this.updateBarAttributes();

      $('body').removeClass('toolbar-loading');

      if (this.model.changed.orientation === 'vertical' || this.model.changed.activeTab) {
        this.loadSubtrees();
      }

      return this;
    },
    onTabClick: function onTabClick(event) {
      if (event.currentTarget.hasAttribute('data-toolbar-tray')) {
        var activeTab = this.model.get('activeTab');
        var clickedTab = event.currentTarget;

        this.model.set('activeTab', !activeTab || clickedTab !== activeTab ? clickedTab : null);

        event.preventDefault();
        event.stopPropagation();
      }
    },
    onOrientationToggleClick: function onOrientationToggleClick(event) {
      var orientation = this.model.get('orientation');

      var antiOrientation = orientation === 'vertical' ? 'horizontal' : 'vertical';
      var locked = antiOrientation === 'vertical';

      if (locked) {
        localStorage.setItem('Drupal.toolbar.trayVerticalLocked', 'true');
      } else {
        localStorage.removeItem('Drupal.toolbar.trayVerticalLocked');
      }

      this.model.set({
        locked: locked,
        orientation: antiOrientation
      }, {
        validate: true,
        override: true
      });

      event.preventDefault();
      event.stopPropagation();
    },
    updateTabs: function updateTabs() {
      var $tab = $(this.model.get('activeTab'));

      $(this.model.previous('activeTab')).removeClass('is-active').prop('aria-pressed', false);

      $(this.model.previous('activeTray')).removeClass('is-active');

      if ($tab.length > 0) {
        $tab.addClass('is-active').prop('aria-pressed', true);
        var name = $tab.attr('data-toolbar-tray');

        var id = $tab.get(0).id;
        if (id) {
          localStorage.setItem('Drupal.toolbar.activeTabID', JSON.stringify(id));
        }

        var $tray = this.$el.find('[data-toolbar-tray="' + name + '"].toolbar-tray');
        if ($tray.length) {
          $tray.addClass('is-active');
          this.model.set('activeTray', $tray.get(0));
        } else {
          this.model.set('activeTray', null);
        }
      } else {
        this.model.set('activeTray', null);
        localStorage.removeItem('Drupal.toolbar.activeTabID');
      }
    },
    updateBarAttributes: function updateBarAttributes() {
      var isOriented = this.model.get('isOriented');
      if (isOriented) {
        this.$el.find('.toolbar-bar').attr('data-offset-top', '');
      } else {
        this.$el.find('.toolbar-bar').removeAttr('data-offset-top');
      }

      this.$el.toggleClass('toolbar-oriented', isOriented);
    },
    updateTrayOrientation: function updateTrayOrientation() {
      var orientation = this.model.get('orientation');

      var antiOrientation = orientation === 'vertical' ? 'horizontal' : 'vertical';

      $('body').toggleClass('toolbar-vertical', orientation === 'vertical').toggleClass('toolbar-horizontal', orientation === 'horizontal');

      var removeClass = antiOrientation === 'horizontal' ? 'toolbar-tray-horizontal' : 'toolbar-tray-vertical';
      var $trays = this.$el.find('.toolbar-tray').removeClass(removeClass).addClass('toolbar-tray-' + orientation);

      var iconClass = 'toolbar-icon-toggle-' + orientation;
      var iconAntiClass = 'toolbar-icon-toggle-' + antiOrientation;
      var $orientationToggle = this.$el.find('.toolbar-toggle-orientation').toggle(this.model.get('isTrayToggleVisible'));
      $orientationToggle.find('button').val(antiOrientation).attr('title', this.strings[antiOrientation]).text(this.strings[antiOrientation]).removeClass(iconClass).addClass(iconAntiClass);

      var dir = document.documentElement.dir;
      var edge = dir === 'rtl' ? 'right' : 'left';

      $trays.removeAttr('data-offset-left data-offset-right data-offset-top');

      $trays.filter('.toolbar-tray-vertical.is-active').attr('data-offset-' + edge, '');

      $trays.filter('.toolbar-tray-horizontal.is-active').attr('data-offset-top', '');
    },
    adjustPlacement: function adjustPlacement() {
      var $trays = this.$el.find('.toolbar-tray');
      if (!this.model.get('isOriented')) {
        $trays.removeClass('toolbar-tray-horizontal').addClass('toolbar-tray-vertical');
      }
    },
    loadSubtrees: function loadSubtrees() {
      var $activeTab = $(this.model.get('activeTab'));
      var orientation = this.model.get('orientation');

      if (!this.model.get('areSubtreesLoaded') && typeof $activeTab.data('drupal-subtrees') !== 'undefined' && orientation === 'vertical') {
        var subtreesHash = drupalSettings.toolbar.subtreesHash;
        var theme = drupalSettings.ajaxPageState.theme;
        var endpoint = Drupal.url('toolbar/subtrees/' + subtreesHash);
        var cachedSubtreesHash = localStorage.getItem('Drupal.toolbar.subtreesHash.' + theme);
        var cachedSubtrees = JSON.parse(localStorage.getItem('Drupal.toolbar.subtrees.' + theme));
        var isVertical = this.model.get('orientation') === 'vertical';

        if (isVertical && subtreesHash === cachedSubtreesHash && cachedSubtrees) {
          Drupal.toolbar.setSubtrees.resolve(cachedSubtrees);
        } else if (isVertical) {
            localStorage.removeItem('Drupal.toolbar.subtreesHash.' + theme);
            localStorage.removeItem('Drupal.toolbar.subtrees.' + theme);

            Drupal.ajax({ url: endpoint }).execute();

            localStorage.setItem('Drupal.toolbar.subtreesHash.' + theme, subtreesHash);
          }
      }
    }
  });
})(jQuery, Drupal, drupalSettings, Backbone);;
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","../keycode","../position","../safe-active-element","../unique-id","../version","../widget"],a):a(jQuery)}(function(a){return a.widget("ui.menu",{version:"1.12.1",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-caret-1-e"},items:"> *",menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.mouseHandled=!1,this.element.uniqueId().attr({role:this.options.role,tabIndex:0}),this._addClass("ui-menu","ui-widget ui-widget-content"),this._on({"mousedown .ui-menu-item":function(a){a.preventDefault()},"click .ui-menu-item":function(b){var c=a(b.target),d=a(a.ui.safeActiveElement(this.document[0]));!this.mouseHandled&&c.not(".ui-state-disabled").length&&(this.select(b),b.isPropagationStopped()||(this.mouseHandled=!0),c.has(".ui-menu").length?this.expand(b):!this.element.is(":focus")&&d.closest(".ui-menu").length&&(this.element.trigger("focus",[!0]),this.active&&1===this.active.parents(".ui-menu").length&&clearTimeout(this.timer)))},"mouseenter .ui-menu-item":function(b){if(!this.previousFilter){var c=a(b.target).closest(".ui-menu-item"),d=a(b.currentTarget);c[0]===d[0]&&(this._removeClass(d.siblings().children(".ui-state-active"),null,"ui-state-active"),this.focus(b,d))}},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(a,b){var c=this.active||this.element.find(this.options.items).eq(0);b||this.focus(a,c)},blur:function(b){this._delay(function(){var c=!a.contains(this.element[0],a.ui.safeActiveElement(this.document[0]));c&&this.collapseAll(b)})},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(a){this._closeOnDocumentClick(a)&&this.collapseAll(a),this.mouseHandled=!1}})},_destroy:function(){var b=this.element.find(".ui-menu-item").removeAttr("role aria-disabled"),c=b.children(".ui-menu-item-wrapper").removeUniqueId().removeAttr("tabIndex role aria-haspopup");this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeAttr("role aria-labelledby aria-expanded aria-hidden aria-disabled tabIndex").removeUniqueId().show(),c.children().each(function(){var b=a(this);b.data("ui-menu-submenu-caret")&&b.remove()})},_keydown:function(b){var c,d,e,f,g=!0;switch(b.keyCode){case a.ui.keyCode.PAGE_UP:this.previousPage(b);break;case a.ui.keyCode.PAGE_DOWN:this.nextPage(b);break;case a.ui.keyCode.HOME:this._move("first","first",b);break;case a.ui.keyCode.END:this._move("last","last",b);break;case a.ui.keyCode.UP:this.previous(b);break;case a.ui.keyCode.DOWN:this.next(b);break;case a.ui.keyCode.LEFT:this.collapse(b);break;case a.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(b);break;case a.ui.keyCode.ENTER:case a.ui.keyCode.SPACE:this._activate(b);break;case a.ui.keyCode.ESCAPE:this.collapse(b);break;default:g=!1,d=this.previousFilter||"",f=!1,e=b.keyCode>=96&&b.keyCode<=105?(b.keyCode-96).toString():String.fromCharCode(b.keyCode),clearTimeout(this.filterTimer),e===d?f=!0:e=d+e,c=this._filterMenuItems(e),c=f&&c.index(this.active.next())!==-1?this.active.nextAll(".ui-menu-item"):c,c.length||(e=String.fromCharCode(b.keyCode),c=this._filterMenuItems(e)),c.length?(this.focus(b,c),this.previousFilter=e,this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)):delete this.previousFilter}g&&b.preventDefault()},_activate:function(a){this.active&&!this.active.is(".ui-state-disabled")&&(this.active.children("[aria-haspopup='true']").length?this.expand(a):this.select(a))},refresh:function(){var b,c,d,e,f,g=this,h=this.options.icons.submenu,i=this.element.find(this.options.menus);this._toggleClass("ui-menu-icons",null,!!this.element.find(".ui-icon").length),d=i.filter(":not(.ui-menu)").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var b=a(this),c=b.prev(),d=a("<span>").data("ui-menu-submenu-caret",!0);g._addClass(d,"ui-menu-icon","ui-icon "+h),c.attr("aria-haspopup","true").prepend(d),b.attr("aria-labelledby",c.attr("id"))}),this._addClass(d,"ui-menu","ui-widget ui-widget-content ui-front"),b=i.add(this.element),c=b.find(this.options.items),c.not(".ui-menu-item").each(function(){var b=a(this);g._isDivider(b)&&g._addClass(b,"ui-menu-divider","ui-widget-content")}),e=c.not(".ui-menu-item, .ui-menu-divider"),f=e.children().not(".ui-menu").uniqueId().attr({tabIndex:-1,role:this._itemRole()}),this._addClass(e,"ui-menu-item")._addClass(f,"ui-menu-item-wrapper"),c.filter(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!a.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]},_setOption:function(a,b){if("icons"===a){var c=this.element.find(".ui-menu-icon");this._removeClass(c,null,this.options.icons.submenu)._addClass(c,null,b.submenu)}this._super(a,b)},_setOptionDisabled:function(a){this._super(a),this.element.attr("aria-disabled",String(a)),this._toggleClass(null,"ui-state-disabled",!!a)},focus:function(a,b){var c,d,e;this.blur(a,a&&"focus"===a.type),this._scrollIntoView(b),this.active=b.first(),d=this.active.children(".ui-menu-item-wrapper"),this._addClass(d,null,"ui-state-active"),this.options.role&&this.element.attr("aria-activedescendant",d.attr("id")),e=this.active.parent().closest(".ui-menu-item").children(".ui-menu-item-wrapper"),this._addClass(e,null,"ui-state-active"),a&&"keydown"===a.type?this._close():this.timer=this._delay(function(){this._close()},this.delay),c=b.children(".ui-menu"),c.length&&a&&/^mouse/.test(a.type)&&this._startOpening(c),this.activeMenu=b.parent(),this._trigger("focus",a,{item:b})},_scrollIntoView:function(b){var c,d,e,f,g,h;this._hasScroll()&&(c=parseFloat(a.css(this.activeMenu[0],"borderTopWidth"))||0,d=parseFloat(a.css(this.activeMenu[0],"paddingTop"))||0,e=b.offset().top-this.activeMenu.offset().top-c-d,f=this.activeMenu.scrollTop(),g=this.activeMenu.height(),h=b.outerHeight(),e<0?this.activeMenu.scrollTop(f+e):e+h>g&&this.activeMenu.scrollTop(f+e-g+h))},blur:function(a,b){b||clearTimeout(this.timer),this.active&&(this._removeClass(this.active.children(".ui-menu-item-wrapper"),null,"ui-state-active"),this._trigger("blur",a,{item:this.active}),this.active=null)},_startOpening:function(a){clearTimeout(this.timer),"true"===a.attr("aria-hidden")&&(this.timer=this._delay(function(){this._close(),this._open(a)},this.delay))},_open:function(b){var c=a.extend({of:this.active},this.options.position);clearTimeout(this.timer),this.element.find(".ui-menu").not(b.parents(".ui-menu")).hide().attr("aria-hidden","true"),b.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(c)},collapseAll:function(b,c){clearTimeout(this.timer),this.timer=this._delay(function(){var d=c?this.element:a(b&&b.target).closest(this.element.find(".ui-menu"));d.length||(d=this.element),this._close(d),this.blur(b),this._removeClass(d.find(".ui-state-active"),null,"ui-state-active"),this.activeMenu=d},this.delay)},_close:function(a){a||(a=this.active?this.active.parent():this.element),a.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false")},_closeOnDocumentClick:function(b){return!a(b.target).closest(".ui-menu").length},_isDivider:function(a){return!/[^\-\u2014\u2013\s]/.test(a.text())},collapse:function(a){var b=this.active&&this.active.parent().closest(".ui-menu-item",this.element);b&&b.length&&(this._close(),this.focus(a,b))},expand:function(a){var b=this.active&&this.active.children(".ui-menu ").find(this.options.items).first();b&&b.length&&(this._open(b.parent()),this._delay(function(){this.focus(a,b)}))},next:function(a){this._move("next","first",a)},previous:function(a){this._move("prev","last",a)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(a,b,c){var d;this.active&&(d="first"===a||"last"===a?this.active["first"===a?"prevAll":"nextAll"](".ui-menu-item").eq(-1):this.active[a+"All"](".ui-menu-item").eq(0)),d&&d.length&&this.active||(d=this.activeMenu.find(this.options.items)[b]()),this.focus(c,d)},nextPage:function(b){var c,d,e;return this.active?void(this.isLastItem()||(this._hasScroll()?(d=this.active.offset().top,e=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return c=a(this),c.offset().top-d-e<0}),this.focus(b,c)):this.focus(b,this.activeMenu.find(this.options.items)[this.active?"last":"first"]()))):void this.next(b)},previousPage:function(b){var c,d,e;return this.active?void(this.isFirstItem()||(this._hasScroll()?(d=this.active.offset().top,e=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return c=a(this),c.offset().top-d+e>0}),this.focus(b,c)):this.focus(b,this.activeMenu.find(this.options.items).first()))):void this.next(b)},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(b){this.active=this.active||a(b.target).closest(".ui-menu-item");var c={item:this.active};this.active.has(".ui-menu").length||this.collapseAll(b,!0),this._trigger("select",b,c)},_filterMenuItems:function(b){var c=b.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&"),d=new RegExp("^"+c,"i");return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function(){return d.test(a.trim(a(this).children(".ui-menu-item-wrapper").text()))})}})});;
/*! jQuery UI - v1.12.1 - 2017-03-31
* http://jqueryui.com
* Copyright jQuery Foundation and other contributors; Licensed  */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./menu","../keycode","../position","../safe-active-element","../version","../widget"],a):a(jQuery)}(function(a){return a.widget("ui.autocomplete",{version:"1.12.1",defaultElement:"<input>",options:{appendTo:null,autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},requestIndex:0,pending:0,_create:function(){var b,c,d,e=this.element[0].nodeName.toLowerCase(),f="textarea"===e,g="input"===e;this.isMultiLine=f||!g&&this._isContentEditable(this.element),this.valueMethod=this.element[f||g?"val":"text"],this.isNewMenu=!0,this._addClass("ui-autocomplete-input"),this.element.attr("autocomplete","off"),this._on(this.element,{keydown:function(e){if(this.element.prop("readOnly"))return b=!0,d=!0,void(c=!0);b=!1,d=!1,c=!1;var f=a.ui.keyCode;switch(e.keyCode){case f.PAGE_UP:b=!0,this._move("previousPage",e);break;case f.PAGE_DOWN:b=!0,this._move("nextPage",e);break;case f.UP:b=!0,this._keyEvent("previous",e);break;case f.DOWN:b=!0,this._keyEvent("next",e);break;case f.ENTER:this.menu.active&&(b=!0,e.preventDefault(),this.menu.select(e));break;case f.TAB:this.menu.active&&this.menu.select(e);break;case f.ESCAPE:this.menu.element.is(":visible")&&(this.isMultiLine||this._value(this.term),this.close(e),e.preventDefault());break;default:c=!0,this._searchTimeout(e)}},keypress:function(d){if(b)return b=!1,void(this.isMultiLine&&!this.menu.element.is(":visible")||d.preventDefault());if(!c){var e=a.ui.keyCode;switch(d.keyCode){case e.PAGE_UP:this._move("previousPage",d);break;case e.PAGE_DOWN:this._move("nextPage",d);break;case e.UP:this._keyEvent("previous",d);break;case e.DOWN:this._keyEvent("next",d)}}},input:function(a){return d?(d=!1,void a.preventDefault()):void this._searchTimeout(a)},focus:function(){this.selectedItem=null,this.previous=this._value()},blur:function(a){return this.cancelBlur?void delete this.cancelBlur:(clearTimeout(this.searching),this.close(a),void this._change(a))}}),this._initSource(),this.menu=a("<ul>").appendTo(this._appendTo()).menu({role:null}).hide().menu("instance"),this._addClass(this.menu.element,"ui-autocomplete","ui-front"),this._on(this.menu.element,{mousedown:function(b){b.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur,this.element[0]!==a.ui.safeActiveElement(this.document[0])&&this.element.trigger("focus")})},menufocus:function(b,c){var d,e;return this.isNewMenu&&(this.isNewMenu=!1,b.originalEvent&&/^mouse/.test(b.originalEvent.type))?(this.menu.blur(),void this.document.one("mousemove",function(){a(b.target).trigger(b.originalEvent)})):(e=c.item.data("ui-autocomplete-item"),!1!==this._trigger("focus",b,{item:e})&&b.originalEvent&&/^key/.test(b.originalEvent.type)&&this._value(e.value),d=c.item.attr("aria-label")||e.value,void(d&&a.trim(d).length&&(this.liveRegion.children().hide(),a("<div>").text(d).appendTo(this.liveRegion))))},menuselect:function(b,c){var d=c.item.data("ui-autocomplete-item"),e=this.previous;this.element[0]!==a.ui.safeActiveElement(this.document[0])&&(this.element.trigger("focus"),this.previous=e,this._delay(function(){this.previous=e,this.selectedItem=d})),!1!==this._trigger("select",b,{item:d})&&this._value(d.value),this.term=this._value(),this.close(b),this.selectedItem=d}}),this.liveRegion=a("<div>",{role:"status","aria-live":"assertive","aria-relevant":"additions"}).appendTo(this.document[0].body),this._addClass(this.liveRegion,null,"ui-helper-hidden-accessible"),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching),this.element.removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()},_setOption:function(a,b){this._super(a,b),"source"===a&&this._initSource(),"appendTo"===a&&this.menu.element.appendTo(this._appendTo()),"disabled"===a&&b&&this.xhr&&this.xhr.abort()},_isEventTargetInWidget:function(b){var c=this.menu.element[0];return b.target===this.element[0]||b.target===c||a.contains(c,b.target)},_closeOnClickOutside:function(a){this._isEventTargetInWidget(a)||this.close()},_appendTo:function(){var b=this.options.appendTo;return b&&(b=b.jquery||b.nodeType?a(b):this.document.find(b).eq(0)),b&&b[0]||(b=this.element.closest(".ui-front, dialog")),b.length||(b=this.document[0].body),b},_initSource:function(){var b,c,d=this;a.isArray(this.options.source)?(b=this.options.source,this.source=function(c,d){d(a.ui.autocomplete.filter(b,c.term))}):"string"==typeof this.options.source?(c=this.options.source,this.source=function(b,e){d.xhr&&d.xhr.abort(),d.xhr=a.ajax({url:c,data:b,dataType:"json",success:function(a){e(a)},error:function(){e([])}})}):this.source=this.options.source},_searchTimeout:function(a){clearTimeout(this.searching),this.searching=this._delay(function(){var b=this.term===this._value(),c=this.menu.element.is(":visible"),d=a.altKey||a.ctrlKey||a.metaKey||a.shiftKey;b&&(!b||c||d)||(this.selectedItem=null,this.search(null,a))},this.options.delay)},search:function(a,b){return a=null!=a?a:this._value(),this.term=this._value(),a.length<this.options.minLength?this.close(b):this._trigger("search",b)!==!1?this._search(a):void 0},_search:function(a){this.pending++,this._addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:a},this._response())},_response:function(){var b=++this.requestIndex;return a.proxy(function(a){b===this.requestIndex&&this.__response(a),this.pending--,this.pending||this._removeClass("ui-autocomplete-loading")},this)},__response:function(a){a&&(a=this._normalize(a)),this._trigger("response",null,{content:a}),!this.options.disabled&&a&&a.length&&!this.cancelSearch?(this._suggest(a),this._trigger("open")):this._close()},close:function(a){this.cancelSearch=!0,this._close(a)},_close:function(a){this._off(this.document,"mousedown"),this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",a))},_change:function(a){this.previous!==this._value()&&this._trigger("change",a,{item:this.selectedItem})},_normalize:function(b){return b.length&&b[0].label&&b[0].value?b:a.map(b,function(b){return"string"==typeof b?{label:b,value:b}:a.extend({},b,{label:b.label||b.value,value:b.value||b.label})})},_suggest:function(b){var c=this.menu.element.empty();this._renderMenu(c,b),this.isNewMenu=!0,this.menu.refresh(),c.show(),this._resizeMenu(),c.position(a.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next(),this._on(this.document,{mousedown:"_closeOnClickOutside"})},_resizeMenu:function(){var a=this.menu.element;a.outerWidth(Math.max(a.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(b,c){var d=this;a.each(c,function(a,c){d._renderItemData(b,c)})},_renderItemData:function(a,b){return this._renderItem(a,b).data("ui-autocomplete-item",b)},_renderItem:function(b,c){return a("<li>").append(a("<div>").text(c.label)).appendTo(b)},_move:function(a,b){return this.menu.element.is(":visible")?this.menu.isFirstItem()&&/^previous/.test(a)||this.menu.isLastItem()&&/^next/.test(a)?(this.isMultiLine||this._value(this.term),void this.menu.blur()):void this.menu[a](b):void this.search(null,b)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(a,b){this.isMultiLine&&!this.menu.element.is(":visible")||(this._move(a,b),b.preventDefault())},_isContentEditable:function(a){if(!a.length)return!1;var b=a.prop("contentEditable");return"inherit"===b?this._isContentEditable(a.parent()):"true"===b}}),a.extend(a.ui.autocomplete,{escapeRegex:function(a){return a.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(b,c){var d=new RegExp(a.ui.autocomplete.escapeRegex(c),"i");return a.grep(b,function(a){return d.test(a.label||a.value||a)})}}),a.widget("ui.autocomplete",a.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(a){return a+(a>1?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(b){var c;this._superApply(arguments),this.options.disabled||this.cancelSearch||(c=b&&b.length?this.options.messages.results(b.length):this.options.messages.noResults,this.liveRegion.children().hide(),a("<div>").text(c).appendTo(this.liveRegion))}}),a.ui.autocomplete});;
(function ($, Drupal) {
  Drupal.behaviors.adminToolbarSearch = {
    attach: function (context) {
      if (context != document) {
        return;
      }
      var getUrl = window.location;
      var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
      var $self = this;
      this.links = [];
      $('a[data-drupal-link-system-path]').each(function() {

        if (this.href != baseUrl) {
          var label = $self.getItemLabel(this);
          $self.links.push({
            'value': $(this).attr('href'),
            'label': label + ' ' + $(this).attr('href'),
            'labelRaw': label
          });
        }
      });

      $( "#admin-toolbar-search-input").autocomplete({
        minLength: 2,
        source: function(request, response) {
          var data = $self.handleAutocomplete(request.term);
          response(data);
        },
        open: function(){
          var zIndex = $('#toolbar-item-administration-search-tray').css("z-index")+1;
          $(this).autocomplete('widget').css('z-index', zIndex);
          return false;
        },
        select: function( event, ui ) {
          if (ui.item.value) {
            location.href = ui.item.value;
            return false;
          }
        }
      }).data("ui-autocomplete")._renderItem = (function(ul, item) {
        return $("<li>")
          .append('<div>' + item.labelRaw + ' <span class="admin-toolbar-search-url">' + item.value + '</span></div>')
          .appendTo(ul);
      });

      // Focus on search field when tab is clicked, or enter is pressed.
      $(context).find('#toolbar-item-administration-search').once('admin_toolbar_search').each(function () {
        if ($('#toolbar-item-administration-search-tray:visible').length) {
          $('#admin-toolbar-search-input').focus();
        }
        $(this).on('click', function() {
          $self.focusOnSearchElement();
        });
      });
    },
    focusOnSearchElement: function() {
      var waitforVisible = function() {
        if ($('#toolbar-item-administration-search-tray:visible').length) {
          $('#admin-toolbar-search-input').focus();
        } else {
          setTimeout(function() {
            waitforVisible();
          }, 1);
        }
      };
      waitforVisible();
    },
    getItemLabel: function(item) {
      var breadcrumbs = [];
      $(item).parents().each(function() {
        if ($(this).hasClass('menu-item')) {
          var $link = $(this).find('a:first');
          if ($link.length && !$link.hasClass('admin-toolbar-search-ignore')) {
            breadcrumbs.unshift($link.text());
          }
        }
      });

      label = breadcrumbs.join(' > ');

      return label;
    },
    handleAutocomplete: function(term) {
      var $self = this;
      var keywords = term.split(" "); // split search terms into list.

      var suggestions = [];
      $self.links.forEach(function(element) {
        var label = element.label.toLowerCase();

        // Add exact matches.
        if (label.indexOf(term.toLowerCase()) >= 0) {
          suggestions.push(element);
        }
        else {
          // Add suggestions where it matches all search terms.
          var matchCount = 0;
          keywords.forEach(function(keyword) {
            if (label.indexOf(keyword.toLowerCase()) >= 0) {
              matchCount++;
            }
          });
          if (matchCount == keywords.length) {
            suggestions.push(element);
          }
        }
      });
      return suggestions;
    }
  };
})(jQuery, Drupal);
;
/*!
 * hoverIntent v1.8.1 // 2014.08.11 // jQuery v1.9.1+
 * http://briancherne.github.io/jquery-hoverIntent/
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007, 2014 Brian Cherne
 */

/* hoverIntent is similar to jQuery's built-in "hover" method except that
 * instead of firing the handlerIn function immediately, hoverIntent checks
 * to see if the user's mouse has slowed down (beneath the sensitivity
 * threshold) before firing the event. The handlerOut function is only
 * called after a matching handlerIn.
 *
 * // basic usage ... just like .hover()
 * .hoverIntent( handlerIn, handlerOut )
 * .hoverIntent( handlerInOut )
 *
 * // basic usage ... with event delegation!
 * .hoverIntent( handlerIn, handlerOut, selector )
 * .hoverIntent( handlerInOut, selector )
 *
 * // using a basic configuration object
 * .hoverIntent( config )
 *
 * @param  handlerIn   function OR configuration object
 * @param  handlerOut  function OR selector for delegation OR undefined
 * @param  selector    selector OR undefined
 * @author Brian Cherne <brian(at)cherne(dot)net>
 */

;(function(factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (jQuery && !jQuery.fn.hoverIntent) {
    factory(jQuery);
  }
})(function($) {
  'use strict';

  // default configuration values
  var _cfg = {
    interval: 100,
    sensitivity: 6,
    timeout: 0
  };

  // counter used to generate an ID for each instance
  var INSTANCE_COUNT = 0;

  // current X and Y position of mouse, updated during mousemove tracking (shared across instances)
  var cX, cY;

  // saves the current pointer position coordinates based on the given mousemove event
  var track = function(ev) {
    cX = ev.pageX;
    cY = ev.pageY;
  };

  // compares current and previous mouse positions
  var compare = function(ev,$el,s,cfg) {
    // compare mouse positions to see if pointer has slowed enough to trigger `over` function
    if ( Math.sqrt( (s.pX-cX)*(s.pX-cX) + (s.pY-cY)*(s.pY-cY) ) < cfg.sensitivity ) {
      $el.off(s.event,track);
      delete s.timeoutId;
      // set hoverIntent state as active for this element (permits `out` handler to trigger)
      s.isActive = true;
      // overwrite old mouseenter event coordinates with most recent pointer position
      ev.pageX = cX; ev.pageY = cY;
      // clear coordinate data from state object
      delete s.pX; delete s.pY;
      return cfg.over.apply($el[0],[ev]);
    } else {
      // set previous coordinates for next comparison
      s.pX = cX; s.pY = cY;
      // use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
      s.timeoutId = setTimeout( function(){compare(ev, $el, s, cfg);} , cfg.interval );
    }
  };

  // triggers given `out` function at configured `timeout` after a mouseleave and clears state
  var delay = function(ev,$el,s,out) {
    delete $el.data('hoverIntent')[s.id];
    return out.apply($el[0],[ev]);
  };

  $.fn.hoverIntent = function(handlerIn,handlerOut,selector) {
    // instance ID, used as a key to store and retrieve state information on an element
    var instanceId = INSTANCE_COUNT++;

    // extend the default configuration and parse parameters
    var cfg = $.extend({}, _cfg);
    if ( $.isPlainObject(handlerIn) ) {
      cfg = $.extend(cfg, handlerIn);
      if ( !$.isFunction(cfg.out) ) {
        cfg.out = cfg.over;
      }
    } else if ( $.isFunction(handlerOut) ) {
      cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector } );
    } else {
      cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut } );
    }

    // A private function for handling mouse 'hovering'
    var handleHover = function(e) {
      // cloned event to pass to handlers (copy required for event object to be passed in IE)
      var ev = $.extend({},e);

      // the current target of the mouse event, wrapped in a jQuery object
      var $el = $(this);

      // read hoverIntent data from element (or initialize if not present)
      var hoverIntentData = $el.data('hoverIntent');
      if (!hoverIntentData) { $el.data('hoverIntent', (hoverIntentData = {})); }

      // read per-instance state from element (or initialize if not present)
      var state = hoverIntentData[instanceId];
      if (!state) { hoverIntentData[instanceId] = state = { id: instanceId }; }

      // state properties:
      // id = instance ID, used to clean up data
      // timeoutId = timeout ID, reused for tracking mouse position and delaying "out" handler
      // isActive = plugin state, true after `over` is called just until `out` is called
      // pX, pY = previously-measured pointer coordinates, updated at each polling interval
      // event = string representing the namespaced event used for mouse tracking

      // clear any existing timeout
      if (state.timeoutId) { state.timeoutId = clearTimeout(state.timeoutId); }

      // namespaced event used to register and unregister mousemove tracking
      var mousemove = state.event = 'mousemove.hoverIntent.hoverIntent'+instanceId;

      // handle the event, based on its type
      if (e.type === 'mouseenter') {
        // do nothing if already active
        if (state.isActive) { return; }
        // set "previous" X and Y position based on initial entry point
        state.pX = ev.pageX; state.pY = ev.pageY;
        // update "current" X and Y position based on mousemove
        $el.off(mousemove,track).on(mousemove,track);
        // start polling interval (self-calling timeout) to compare mouse coordinates over time
        state.timeoutId = setTimeout( function(){compare(ev,$el,state,cfg);} , cfg.interval );
      } else { // "mouseleave"
        // do nothing if not already active
        if (!state.isActive) { return; }
        // unbind expensive mousemove event
        $el.off(mousemove,track);
        // if hoverIntent state is true, then call the mouseOut function after the specified delay
        state.timeoutId = setTimeout( function(){delay(ev,$el,state,cfg.out);} , cfg.timeout );
      }
    };

    // listen for mouseenter and mouseleave
    return this.on({'mouseenter.hoverIntent':handleHover,'mouseleave.hoverIntent':handleHover}, cfg.selector);
  };
});
;
(function ($, Drupal) {
  Drupal.behaviors.adminToolbar = {
    attach: function (context, settings) {

      $('a.toolbar-icon', context).removeAttr('title');

      $('.toolbar-tray li.menu-item--expanded, .toolbar-tray ul li.menu-item--expanded .menu-item', context).hoverIntent({
        over: function () {
          // At the current depth, we should delete all "hover-intent" classes.
          // Other wise we get unwanted behaviour where menu items are expanded while already in hovering other ones.
          $(this).parent().find('li').removeClass('hover-intent');
          $(this).addClass('hover-intent');
        },
        out: function () {
          $(this).removeClass('hover-intent');
        },
        timeout: 250
      });

      // Make the toolbar menu navigable with keyboard.
      $('ul.toolbar-menu li.menu-item--expanded a', context).on('focusin', function () {
        $('li.menu-item--expanded', context).removeClass('hover-intent');
        $(this).parents('li.menu-item--expanded').addClass('hover-intent');
      });

      $('ul.toolbar-menu li.menu-item a', context).keydown(function (e) {
        if ((e.shiftKey && (e.keyCode || e.which) == 9)) {
          if ($(this).parent('.menu-item').prev().hasClass('menu-item--expanded')) {
            $(this).parent('.menu-item').prev().addClass('hover-intent');
          }
        }
      });

      $('.toolbar-menu:first-child > .menu-item:not(.menu-item--expanded) a, .toolbar-tab > a', context).on('focusin', function () {
        $('.menu-item--expanded').removeClass('hover-intent');
      });

      $('.toolbar-menu:first-child > .menu-item', context).on('hover', function () {
        $(this, 'a').css("background: #fff;");
      });

      $('ul:not(.toolbar-menu)', context).on({
        mousemove: function () {
          $('li.menu-item--expanded').removeClass('hover-intent');
        },
        hover: function () {
          $('li.menu-item--expanded').removeClass('hover-intent');
        }
      });

    }
  };
})(jQuery, Drupal);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  var pathInfo = drupalSettings.path;
  var escapeAdminPath = sessionStorage.getItem('escapeAdminPath');
  var windowLocation = window.location;

  if (!pathInfo.currentPathIsAdmin && !/destination=/.test(windowLocation.search)) {
    sessionStorage.setItem('escapeAdminPath', windowLocation);
  }

  Drupal.behaviors.escapeAdmin = {
    attach: function attach() {
      var $toolbarEscape = $('[data-toolbar-escape-admin]').once('escapeAdmin');
      if ($toolbarEscape.length && pathInfo.currentPathIsAdmin) {
        if (escapeAdminPath !== null) {
          $toolbarEscape.attr('href', escapeAdminPath);
        } else {
          $toolbarEscape.text(Drupal.t('Home'));
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  function TabbingManager() {
    this.stack = [];
  }

  function TabbingContext(options) {
    $.extend(this, {
      level: null,

      $tabbableElements: $(),

      $disabledElements: $(),

      released: false,

      active: false
    }, options);
  }

  $.extend(TabbingManager.prototype, {
    constrain: function constrain(elements) {
      var il = this.stack.length;
      for (var i = 0; i < il; i++) {
        this.stack[i].deactivate();
      }

      var $elements = $(elements).find(':tabbable').addBack(':tabbable');

      var tabbingContext = new TabbingContext({
        level: this.stack.length,
        $tabbableElements: $elements
      });

      this.stack.push(tabbingContext);

      tabbingContext.activate();

      $(document).trigger('drupalTabbingConstrained', tabbingContext);

      return tabbingContext;
    },
    release: function release() {
      var toActivate = this.stack.length - 1;
      while (toActivate >= 0 && this.stack[toActivate].released) {
        toActivate--;
      }

      this.stack.splice(toActivate + 1);

      if (toActivate >= 0) {
        this.stack[toActivate].activate();
      }
    },
    activate: function activate(tabbingContext) {
      var $set = tabbingContext.$tabbableElements;
      var level = tabbingContext.level;

      var $disabledSet = $(':tabbable').not($set);

      tabbingContext.$disabledElements = $disabledSet;

      var il = $disabledSet.length;
      for (var i = 0; i < il; i++) {
        this.recordTabindex($disabledSet.eq(i), level);
      }

      $disabledSet.prop('tabindex', -1).prop('autofocus', false);

      var $hasFocus = $set.filter('[autofocus]').eq(-1);

      if ($hasFocus.length === 0) {
        $hasFocus = $set.eq(0);
      }
      $hasFocus.trigger('focus');
    },
    deactivate: function deactivate(tabbingContext) {
      var $set = tabbingContext.$disabledElements;
      var level = tabbingContext.level;
      var il = $set.length;
      for (var i = 0; i < il; i++) {
        this.restoreTabindex($set.eq(i), level);
      }
    },
    recordTabindex: function recordTabindex($el, level) {
      var tabInfo = $el.data('drupalOriginalTabIndices') || {};
      tabInfo[level] = {
        tabindex: $el[0].getAttribute('tabindex'),
        autofocus: $el[0].hasAttribute('autofocus')
      };
      $el.data('drupalOriginalTabIndices', tabInfo);
    },
    restoreTabindex: function restoreTabindex($el, level) {
      var tabInfo = $el.data('drupalOriginalTabIndices');
      if (tabInfo && tabInfo[level]) {
        var data = tabInfo[level];
        if (data.tabindex) {
          $el[0].setAttribute('tabindex', data.tabindex);
        } else {
            $el[0].removeAttribute('tabindex');
          }
        if (data.autofocus) {
          $el[0].setAttribute('autofocus', 'autofocus');
        }

        if (level === 0) {
          $el.removeData('drupalOriginalTabIndices');
        } else {
          var levelToDelete = level;
          while (tabInfo.hasOwnProperty(levelToDelete)) {
            delete tabInfo[levelToDelete];
            levelToDelete++;
          }
          $el.data('drupalOriginalTabIndices', tabInfo);
        }
      }
    }
  });

  $.extend(TabbingContext.prototype, {
    release: function release() {
      if (!this.released) {
        this.deactivate();
        this.released = true;
        Drupal.tabbingManager.release(this);

        $(document).trigger('drupalTabbingContextReleased', this);
      }
    },
    activate: function activate() {
      if (!this.active && !this.released) {
        this.active = true;
        Drupal.tabbingManager.activate(this);

        $(document).trigger('drupalTabbingContextActivated', this);
      }
    },
    deactivate: function deactivate() {
      if (this.active) {
        this.active = false;
        Drupal.tabbingManager.deactivate(this);

        $(document).trigger('drupalTabbingContextDeactivated', this);
      }
    }
  });

  if (Drupal.tabbingManager) {
    return;
  }

  Drupal.tabbingManager = new TabbingManager();
})(jQuery, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, Backbone) {
  var strings = {
    tabbingReleased: Drupal.t('Tabbing is no longer constrained by the Contextual module.'),
    tabbingConstrained: Drupal.t('Tabbing is constrained to a set of @contextualsCount and the edit mode toggle.'),
    pressEsc: Drupal.t('Press the esc key to exit.')
  };

  function initContextualToolbar(context) {
    if (!Drupal.contextual || !Drupal.contextual.collection) {
      return;
    }

    var contextualToolbar = Drupal.contextualToolbar;
    contextualToolbar.model = new contextualToolbar.StateModel({
      isViewing: localStorage.getItem('Drupal.contextualToolbar.isViewing') !== 'false'
    }, {
      contextualCollection: Drupal.contextual.collection
    });

    var viewOptions = {
      el: $('.toolbar .toolbar-bar .contextual-toolbar-tab'),
      model: contextualToolbar.model,
      strings: strings
    };
    new contextualToolbar.VisualView(viewOptions);
    new contextualToolbar.AuralView(viewOptions);
  }

  Drupal.behaviors.contextualToolbar = {
    attach: function attach(context) {
      if ($('body').once('contextualToolbar-init').length) {
        initContextualToolbar(context);
      }
    }
  };

  Drupal.contextualToolbar = {
    model: null
  };
})(jQuery, Drupal, Backbone);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Drupal, Backbone) {
  Drupal.contextualToolbar.StateModel = Backbone.Model.extend({
    defaults: {
      isViewing: true,

      isVisible: false,

      contextualCount: 0,

      tabbingContext: null
    },

    initialize: function initialize(attrs, options) {
      this.listenTo(options.contextualCollection, 'reset remove add', this.countContextualLinks);
      this.listenTo(options.contextualCollection, 'add', this.lockNewContextualLinks);

      this.listenTo(this, 'change:contextualCount', this.updateVisibility);

      this.listenTo(this, 'change:isViewing', function (model, isViewing) {
        options.contextualCollection.each(function (contextualModel) {
          contextualModel.set('isLocked', !isViewing);
        });
      });
    },
    countContextualLinks: function countContextualLinks(contextualModel, contextualCollection) {
      this.set('contextualCount', contextualCollection.length);
    },
    lockNewContextualLinks: function lockNewContextualLinks(contextualModel, contextualCollection) {
      if (!this.get('isViewing')) {
        contextualModel.set('isLocked', true);
      }
    },
    updateVisibility: function updateVisibility() {
      this.set('isVisible', this.get('contextualCount') > 0);
    }
  });
})(Drupal, Backbone);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, Backbone, _) {
  Drupal.contextualToolbar.AuralView = Backbone.View.extend({
    announcedOnce: false,

    initialize: function initialize(options) {
      this.options = options;

      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'change:isViewing', this.manageTabbing);

      $(document).on('keyup', _.bind(this.onKeypress, this));
      this.manageTabbing();
    },
    render: function render() {
      this.$el.find('button').attr('aria-pressed', !this.model.get('isViewing'));

      return this;
    },
    manageTabbing: function manageTabbing() {
      var tabbingContext = this.model.get('tabbingContext');

      if (tabbingContext) {
        if (tabbingContext.active) {
          Drupal.announce(this.options.strings.tabbingReleased);
        }
        tabbingContext.release();
      }

      if (!this.model.get('isViewing')) {
        tabbingContext = Drupal.tabbingManager.constrain($('.contextual-toolbar-tab, .contextual'));
        this.model.set('tabbingContext', tabbingContext);
        this.announceTabbingConstraint();
        this.announcedOnce = true;
      }
    },
    announceTabbingConstraint: function announceTabbingConstraint() {
      var strings = this.options.strings;
      Drupal.announce(Drupal.formatString(strings.tabbingConstrained, {
        '@contextualsCount': Drupal.formatPlural(Drupal.contextual.collection.length, '@count contextual link', '@count contextual links')
      }));
      Drupal.announce(strings.pressEsc);
    },
    onKeypress: function onKeypress(event) {
      if (!this.announcedOnce && event.keyCode === 9 && !this.model.get('isViewing')) {
        this.announceTabbingConstraint();

        this.announcedOnce = true;
      }

      if (event.keyCode === 27) {
        this.model.set('isViewing', true);
      }
    }
  });
})(jQuery, Drupal, Backbone, _);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Drupal, Backbone) {
  Drupal.contextualToolbar.VisualView = Backbone.View.extend({
    events: function events() {
      var touchEndToClick = function touchEndToClick(event) {
        event.preventDefault();
        event.target.click();
      };

      return {
        click: function click() {
          this.model.set('isViewing', !this.model.get('isViewing'));
        },

        touchend: touchEndToClick
      };
    },
    initialize: function initialize() {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'change:isViewing', this.persist);
    },
    render: function render() {
      this.$el.toggleClass('hidden', !this.model.get('isVisible'));

      this.$el.find('button').toggleClass('is-active', !this.model.get('isViewing'));

      return this;
    },
    persist: function persist(model, isViewing) {
      if (!isViewing) {
        localStorage.setItem('Drupal.contextualToolbar.isViewing', 'false');
      } else {
        localStorage.removeItem('Drupal.contextualToolbar.isViewing');
      }
    }
  });
})(Drupal, Backbone);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  var blockConfigureSelector = '[data-settings-tray-edit]';
  var toggleEditSelector = '[data-drupal-settingstray="toggle"]';
  var itemsToToggleSelector = '[data-off-canvas-main-canvas], #toolbar-bar, [data-drupal-settingstray="editable"] a, [data-drupal-settingstray="editable"] button';
  var contextualItemsSelector = '[data-contextual-id] a, [data-contextual-id] button';
  var quickEditItemSelector = '[data-quickedit-entity-id]';

  function preventClick(event) {
    if ($(event.target).closest('.contextual-links').length) {
      return;
    }
    event.preventDefault();
  }

  function closeToolbarTrays() {
    $(Drupal.toolbar.models.toolbarModel.get('activeTab')).trigger('click');
  }

  function disableQuickEdit() {
    $('.quickedit-toolbar button.action-cancel').trigger('click');
  }

  function closeOffCanvas() {
    $('.ui-dialog-off-canvas .ui-dialog-titlebar-close').trigger('click');
  }

  function getItemsToToggle() {
    return $(itemsToToggleSelector).not(contextualItemsSelector);
  }

  function setEditModeState(editMode) {
    if (!document.querySelector('[data-off-canvas-main-canvas]')) {
      throw new Error('data-off-canvas-main-canvas is missing from settings-tray-page-wrapper.html.twig');
    }
    editMode = !!editMode;
    var $editButton = $(toggleEditSelector);
    var $editables = void 0;

    if (editMode) {
      $editButton.text(Drupal.t('Editing'));
      closeToolbarTrays();

      $editables = $('[data-drupal-settingstray="editable"]').once('settingstray');
      if ($editables.length) {
        document.querySelector('[data-off-canvas-main-canvas]').addEventListener('click', preventClick, true);

        $editables.not(contextualItemsSelector).on('click.settingstray', function (e) {
          if ($(e.target).closest('.contextual').length || !localStorage.getItem('Drupal.contextualToolbar.isViewing')) {
            return;
          }
          $(e.currentTarget).find(blockConfigureSelector).trigger('click');
          disableQuickEdit();
        });
        $(quickEditItemSelector).not(contextualItemsSelector).on('click.settingstray', function (e) {
          if (!$(e.target).parent().hasClass('contextual') || $(e.target).parent().hasClass('quickedit')) {
            closeOffCanvas();
          }

          if ($(e.target).parent().hasClass('contextual') || $(e.target).parent().hasClass('quickedit')) {
            return;
          }
          $(e.currentTarget).find('li.quickedit a').trigger('click');
        });
      }
    } else {
        $editables = $('[data-drupal-settingstray="editable"]').removeOnce('settingstray');
        if ($editables.length) {
          document.querySelector('[data-off-canvas-main-canvas]').removeEventListener('click', preventClick, true);
          $editables.off('.settingstray');
          $(quickEditItemSelector).off('.settingstray');
        }

        $editButton.text(Drupal.t('Edit'));
        closeOffCanvas();
        disableQuickEdit();
      }
    getItemsToToggle().toggleClass('js-settings-tray-edit-mode', editMode);
    $('.edit-mode-inactive').toggleClass('visually-hidden', editMode);
  }

  function isInEditMode() {
    return $('#toolbar-bar').hasClass('js-settings-tray-edit-mode');
  }

  function toggleEditMode() {
    setEditModeState(!isInEditMode());
  }

  function prepareAjaxLinks() {
    Drupal.ajax.instances.filter(function (instance) {
      return instance && $(instance.element).attr('data-dialog-renderer') === 'off_canvas';
    }).forEach(function (instance) {
      if (!instance.options.data.hasOwnProperty('dialogOptions')) {
        instance.options.data.dialogOptions = {};
      }
      instance.options.data.dialogOptions.settingsTrayActiveEditableId = $(instance.element).parents('.settings-tray-editable').attr('id');
      instance.progress = { type: 'fullscreen' };
    });
  }

  $(document).on('drupalContextualLinkAdded', function (event, data) {
    prepareAjaxLinks();

    $('body').once('settings_tray.edit_mode_init').each(function () {
      var editMode = localStorage.getItem('Drupal.contextualToolbar.isViewing') === 'false';
      if (editMode) {
        setEditModeState(true);
      }
    });

    data.$el.find(blockConfigureSelector).on('click.settingstray', function () {
      if (!isInEditMode()) {
        $(toggleEditSelector).trigger('click').trigger('click.settings_tray');
      }

      disableQuickEdit();
    });
  });

  $(document).on('keyup.settingstray', function (e) {
    if (isInEditMode() && e.keyCode === 27) {
      Drupal.announce(Drupal.t('Exited edit mode.'));
      toggleEditMode();
    }
  });

  Drupal.behaviors.toggleEditMode = {
    attach: function attach() {
      $(toggleEditSelector).once('settingstray').on('click.settingstray', toggleEditMode);
    }
  };

  $(window).on({
    'dialog:beforecreate': function dialogBeforecreate(event, dialog, $element, settings) {
      if ($element.is('#drupal-off-canvas')) {
        $('body .settings-tray-active-editable').removeClass('settings-tray-active-editable');
        var $activeElement = $('#' + settings.settingsTrayActiveEditableId);
        if ($activeElement.length) {
          $activeElement.addClass('settings-tray-active-editable');
        }
      }
    },
    'dialog:beforeclose': function dialogBeforeclose(event, dialog, $element) {
      if ($element.is('#drupal-off-canvas')) {
        $('body .settings-tray-active-editable').removeClass('settings-tray-active-editable');
      }
    }
  });
})(jQuery, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  function mapTextContentToAjaxResponse(content) {
    if (content === '') {
      return false;
    }

    try {
      return JSON.parse(content);
    } catch (e) {
      return false;
    }
  }

  function bigPipeProcessPlaceholderReplacement(index, placeholderReplacement) {
    var placeholderId = placeholderReplacement.getAttribute('data-big-pipe-replacement-for-placeholder-with-id');
    var content = this.textContent.trim();

    if (typeof drupalSettings.bigPipePlaceholderIds[placeholderId] !== 'undefined') {
      var response = mapTextContentToAjaxResponse(content);

      if (response === false) {
        $(this).removeOnce('big-pipe');
      } else {
        var ajaxObject = Drupal.ajax({
          url: '',
          base: false,
          element: false,
          progress: false
        });

        ajaxObject.success(response, 'success');
      }
    }
  }

  var interval = drupalSettings.bigPipeInterval || 50;

  var timeoutID = void 0;

  function bigPipeProcessDocument(context) {
    if (!context.querySelector('script[data-big-pipe-event="start"]')) {
      return false;
    }

    $(context).find('script[data-big-pipe-replacement-for-placeholder-with-id]').once('big-pipe').each(bigPipeProcessPlaceholderReplacement);

    if (context.querySelector('script[data-big-pipe-event="stop"]')) {
      if (timeoutID) {
        clearTimeout(timeoutID);
      }
      return true;
    }

    return false;
  }

  function bigPipeProcess() {
    timeoutID = setTimeout(function () {
      if (!bigPipeProcessDocument(document)) {
        bigPipeProcess();
      }
    }, interval);
  }

  bigPipeProcess();

  $(window).on('load', function () {
    if (timeoutID) {
      clearTimeout(timeoutID);
    }
    bigPipeProcessDocument(document);
  });
})(jQuery, Drupal, drupalSettings);;
